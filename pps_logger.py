#!/usr/bin/python
import datetime
import pathlib
import time

import redis

import becs.r2dbe.data as r2data
import r2daemon
import vdif

rs = redis.Redis()

r2dbe_last_update = {}
while True:
    for k in rs.keys("r2dbe.*.raw._last_update"):
        k = "".join(chr(c) for c in k)
        r2dbe_name = ".".join(k.split(".")[:2])
        if r2dbe_name not in list(r2dbe_last_update.keys()):
            r2dbe_last_update[r2dbe_name] = -1
        last_update = float(rs[k])
        if last_update > r2dbe_last_update[r2dbe_name]:
            param_dict = r2data.lookup_param("pps_offset")
            binary = r2daemon.decode(rs[r2dbe_name + ".raw." + param_dict["dev"]])
            pps_offset = param_dict["decode"](binary)
            vdif_time = vdif.VDIFTime.from_datetime(datetime.datetime.fromtimestamp(last_update))
            with open(pathlib.os.sep.join([str(pathlib.Path.home()), "log", ".".join(["pps", r2dbe_name, "log"])]), "a+") as fh:
                fh.seek(0)
                if len(fh.read()) == 0:
                    fh.write("ref_epoch,sec_since_epoch,data_frame,pps_offset\n")
                fh.write("{v.epoch},{v.sec},{v.frame},{offset}\n".format(v=vdif_time, offset=pps_offset))
            r2dbe_last_update[r2dbe_name] = last_update
    time.sleep(60)
