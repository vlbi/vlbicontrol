#!/usr/bin/python
import pathlib
import signal
import subprocess
import sys
import time

import logging
logger = logging.getLogger()

import config
import vexparser

from pwr_logger import get_valid_schedules

def main(logger, loop_wait=10):
    running_logger = {}
    while True:
        # sleep a while
        time.sleep(loop_wait)
        # remove logger subprocesses, if any, that have terminated
        for md5, proc in list(running_logger.items()):
            if proc.poll() is not None:
                logger.info("logger process with PID %d returned %d, removing (md5sum=%s)", proc.pid, proc.returncode, md5)
                running_logger.pop(md5)
        # reload config on each iteration in case anything changed
        tmp_level = logger.level
        if logger.level > logging.DEBUG:
            # if higher than DEBUG level, suppress anything below warning in config
            logger.setLevel(logging.WARNING)
        cfg = config.Config("/etc/backend.conf")
        logger.setLevel(tmp_level)
        valids = get_valid_schedules(cfg)
        stationcode = cfg.instruct["stationcode"]
        if not len(valids) == 1:
            # do nothing if more than one valid schedule (pwr_logger.py will not
            # do anything if started in any case)
            logger.debug("found %d valid schedules, waiting until there is exactly 1", len(valids))
            continue
        # capture md5sum
        md5sum = vexparser.vexparser(valids[0])["md5sum"]
        logger.debug("found exactly 1 valid schedule '%s' (md5sum=%s)", valids[0], md5sum)
        # if there is a running logger and md5sum does not match, then first
        # stop the running logger
        if running_logger:
            if md5sum not in list(running_logger.keys()):
                logger.info("md5sum does not match running logger(s)")
                for md5, proc in list(running_logger.items()):
                    logger.info("sending SIGINT to %d (md5sum=%s)", proc.pid, md5)
                    proc.send_signal(signal.SIGINT)
            else:
                # do nothing, running logger already associated with this md5sum
                continue
        # start pwr_logger instance
        logger.info("starting logger process for exactly 1 valid schedule found '%s' (md5sum=%s)", valids[0], md5sum)
        script = __file__.replace(pathlib.os.path.basename(__file__), "pwr_logger.py")
        proc = subprocess.Popen(["/usr/bin/python", script, "--append-md5"])
        logging.info("logger process started with PID %d (md5sum=%s)", proc.pid, md5sum)
        # give it time to die if in the meantime number of valids have changed
        time.sleep(1)
        # if process still alive, store using md5sum
        if proc.poll() is None:
            running_logger[md5sum] = proc
        else:
            logging.warning("logger process with PID %d has stopped immediately", proc.pid)

if __name__ == "__main__":
    logger.addHandler(logging.StreamHandler(sys.stdout))
    logger.setLevel(logging.INFO)
    main(logger)
