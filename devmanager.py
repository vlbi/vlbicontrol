#!/usr/bin/python2 -u
from __future__ import print_function
# This is part of the VLBIcontrol software.  This software is released under the terms of the MIT Licence.
# Copyright (c) 2018 Radboud RadioLab.  All rights reserved
import sys
import os
import signal
import importlib
#-- custom
import config


#-- parse commandline args
prog = os.path.basename(sys.argv[0])#{{{
if len(sys.argv) != 3:
    print('usage: %s configfile devID' % prog)
    sys.exit(1)
fname, devid = sys.argv[1:]
#}}}


#-- config file
def load_config(fname, devid):#{{{
    cfg = config.Config(fname)
    if not devid in cfg.devices:
        raise Exception('device not defined: %s' % devid)
    d = cfg.devices[devid]
    if not d['connected']:
        raise config.ConfigError('no links defined on device: %s' % devid)
    return cfg
cfg = load_config(fname, devid)
devtype = cfg.devices[devid]['type']
#}}}

start_permitted = True


#-- load device type module
devmod = importlib.import_module('devices.%s' % devtype)
devmgr = devmod.Manager(devid)


#-- sig handlers
def sigabort_handler(signum, frame):#{{{
    print('signal %s caught' % signum)
    devmgr.stop()

def sigvex_handler(signum, frame):
    print('signal %s caught' % signum)
    #-- scan vexfiles

def sigconfig_handler(signum, frame):
    print('signal %s caught' % signum)
    #-- read new config file
    try:
        print('reading:', fname)
        cfg2 = load_config(fname, devid)
    except Exception as e:
        print(e)
        print('continue with current config...')
        return
    print('%s-manager apply new configuration: %s' % (devtype, devid))
    global start_permitted
    start_permitted = True
    devmgr.stop()
    #-- are there changes to the relevant section?

sighandlers = {
    signal.SIGTERM: sigabort_handler,
    signal.SIGINT:  sigabort_handler,
    signal.SIGUSR1: sigconfig_handler,
    signal.SIGUSR2: sigvex_handler}
for s,h in sighandlers.iteritems():
    signal.signal(s, h)
#}}}


#== init
print('%s-manager setup: %s' % (devtype, devid))
devmgr.setup(cfg)


#== run
print('%s-manager start: %s' % (devtype, devid))
#-- restart when a new config file has been loaded
while start_permitted:
    start_permitted = False
    devmgr.run()
    #-- if still permitted a reconfigure is requested
    if start_permitted:
        devmgr.reconfigure()


#== clean up
print('%s-manager teardown: %s' % (devtype, devid))
devmgr.teardown()

# vim: foldmethod=marker
