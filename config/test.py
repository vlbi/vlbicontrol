#!/usr/bin/python3
import glob
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-v","--verbose", action='count', help="verbose output (repeat to increase)")
args = parser.parse_args()

#-- test config parsing
import config
path = os.path.dirname(config.__file__) + '/testing'
files = sorted(glob.iglob(path + '/*'))
warn, ngood = [], 0
for f in files:
    fname = f[len(path)+1:]
    try:
        cfg = config.Config(f)
    #-- an exception is good
    except config.ConfigError as e:
        ee = e
        pass
    #-- no exception is bad
    else:
        msg = 'FAIL   : %s' % fname
        warn.append(msg)
        print(msg + ' <' + 20*'-')
        continue

    #-- read the expected error line
    with open(f, 'r') as f:
        f.readline()  #-- skip
        line = f.readline().strip()
    linenow = '# %s: %s' % (type(ee).__name__, ee)

    #-- bad: unexpected error
    if line != linenow:
        msg = 'WARN   : %s' % fname
        warn.append(msg)
        print(msg + ' <' + 20*'-')
        print('         expected: %s' % line)
        print('         got:      %s' % linenow)
        continue

    #-- correct: expected error
    print('success: %s' % fname)
    if args.verbose is not None:
        print('         %s' % ee)
    ngood += 1


print()
print('summary:')
print('   success:', ngood)
print('   fail   :', len(warn))
for msg in warn:
    print(msg)
