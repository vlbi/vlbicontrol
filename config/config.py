#!/bin/python3
# This is part of the VLBIcontrol software.  This software is released under the terms of the MIT Licence.
# Copyright (c) 2018 Radboud RadioLab.  All rights reserved
import logging
import yaml
from jsonschema import validate
import collections


def main():
    '''Parse config file and print exported variables'''
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="Input filename")
    args = parser.parse_args()

    #-- setup logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    #-- instantiate cfg class
    cfg = Config(args.filename)

    print('devices:')
    for k in sorted(cfg.devices):
        print(k, cfg.devices[k])
    print()

    print('ports:')
    for k in sorted(cfg.ports):
        print(k, cfg.ports[k])
    print()

    print('chains:')
    for k in sorted(cfg.chains):
        print(k, cfg.chains[k])
    print()

    print('config parsed successfully!')

    #-- write old-version config file
    fname = args.filename + '.old'
    print('\nwrite old-version config file:', fname)
    with open(fname, 'w') as f:
        f.write(cfg.make_old_version())

    return cfg

class ConfigError(Exception): pass

class DictDefaultList(dict):
    '''A dict that returns an empty list for undefined keys'''
    def __getitem__(self, key):
        if not key in self:
            print('key not found:', key)
            return []
        return super().__getitem__(key)

class Config():
    """VLBIcontrol configuration file class"""
#{{{
#{{{
    SCHEMA = """{
required: [instruct, report, deviceLayouts, devices, links, delim_device, delim_port, delim_link],
properties: {
    instruct: {required: [vexstore, stationname, stationcode]},
    report: {},
    deviceLayouts: {
        additionalProperties: false,
        patternProperties: {'(rx|bdc_[12]band|nifdc|r2dbe|dbbc3|mark6|terminator)': {
            type: object,
            minProperties: 1,
            properties: {
                _chainstart: {type: array, uniqueItems: true},
                _chainend: {type: array, uniqueItems: true},
                _role: {
                    type: string,
                    pattern: '^(rx|bdc|digitizer|dbbc|recorder|terminator)$'
                },
            },
            required: [_role],
            patternProperties: {'^[^_].*': {
                type: object,
                minProperties: 1,
                patternProperties: {.*: {
                    type: object
                }}
            }}
        }}
    },
    devices: {
        additionalProperties: false,
        patternProperties: {
            ^(rx|bdc|nifdc|r2dbe|dbbc3|mark6|terminator)\..*: {
                type: object,
                required: [layout]
            },
            ^(bdc|r2dbe|dbbc3|mark6)\..*: {
                required: [hostname]
            },
            ^(mark6)\..*: {
                required: [modules]
            }
        }
    },
    links: {
        type: array,
        items: {
            type: string,
            pattern: '^[a-zA-Z0-9_]+\.[a-zA-Z0-9_]+:[a-zA-Z0-9_]+  [a-zA-Z0-9_]+\.[a-zA-Z0-9_]+:[a-zA-Z0-9_]+$'
        },
        uniqueItems: true
    }
}
}"""
#}}}

    def __init__(self, fname):
        #-- read config file
        logging.info('Reading config file: %s' % fname)
        with open(fname) as f:
            config = yaml.load(f, Loader=yaml.FullLoader)

        #-- adopt objects defined in config file
        for k,v in config.items():
            setattr(self, k, v)

        #-- validate against schema
        schema = yaml.load(self.SCHEMA, Loader=yaml.FullLoader)
        validate(config, schema)

        #-- collect ports from all devices
        self.ports = self.__devices_inherit()
        #-- turn links into ordered dict with (startport, endport)
        self.links = self.__check_links()
        self.chains = self.__compile_chains()

        #-- count links per device
        for l in self.links:
            devs = [l2.split(self.delim_port)[0] for l2 in l.split(self.delim_link)]
            for d in devs:
                self.devices[d]['connected'] = True


    def __devices_inherit(self):
        '''Devices inherit inports, outports, and other properties from their layout'''
        def mkport(name, devid, intent, feed, **kwargs): #{{{
            '''Port constructor'''
            port = { #{{{
                'name': name,
                'devid': devid,
                'intent': intent,
                'feeds': None,
                'fedby': None,
                'label': kwargs.pop('label', ''),
                'connected': False,
                'terminates': False,
                }

            #-- feed depends on intent
            if intent == 'in':
                port['feeds'] = [self.delim_port.join([devid, f]) for f in feed]
            elif intent == 'out':
                port['fedby'] = self.delim_port.join([devid, feed])
            else:
                raise Exception('mkport: invalid port intent')

            #-- copy remaining properties
            for k,v in kwargs.items():
                if k in port:
                    raise ConfigError('reserved port property encountered: %s' % k)
                port[k] = v

            return port
        #}}}

        ports = {}

        #-- instantiate devices
        for d,dev in self.devices.items():
            #-- don't silently overwrite existing definitions
            #-- We don't know how to deal with explicit inputs.
            #-- in principle they could be combined with inherited
            #-- inputs as long as their names do not clash.
            for k in ['role', 'inports', 'outports', 'connected']:
                if k in dev:
                    raise ConfigError('reserved device property encountered: %s' % k)

            #-- devtype
            lay = dev['layout']
            #-- verify that device layout is defined
            if not lay in self.deviceLayouts:
                raise ConfigError('device layout not defined: %s' % lay)
            dev.update({"layout": lay})

            #-- process inputs and outputs
            ins, outs = [], []
            for k,v in self.deviceLayouts[lay].items():
                #-- adopt non-input properties
                #-- chainstart and chainend are dropped later
                if k[0] == '_':
                    dev[k[1:]] = v
                    continue

                portname = self.delim_port.join([d, k])
                ins.append(portname)
                if portname in ports:
                    raise ConfigError('port name not unique: %s' % portname)
                ports[portname] = mkport(k, d, 'in', v.keys())

                #-- outputs
                for kk,vv in v.items():
                    portname = self.delim_port.join([d, kk])
                    outs.append(portname)
                    if portname in ports:
                        raise ConfigError('port name not unique: %s' % portname)
                    ports[portname] = mkport(kk, d, 'out', k, **vv)

            #-- port terminate property
            for k in dev.pop('chainstart', []):
                portname = self.delim_port.join([d, k])
                p = ports[portname]
                if p['intent'] != 'in':
                    raise ConfigError('chainstart port is not inport: %s' % k)
                p['terminates'] = True
            for k in dev.pop('chainend', []):
                portname = self.delim_port.join([d, k])
                p = ports[portname]
                if p['intent'] != 'out':
                    raise ConfigError('chainend port is not outport: %s' % k)
                p['terminates'] = True

            dev.update({"inports": ins})
            dev.update({"outports": outs})

            #-- link counter
            dev["connected"] = False  #-- initialize here, set after links are verified

        return ports
#}}}


    def __check_links(self):
        '''Verify linked ports are defined and used only once'''
        links = collections.OrderedDict()#{{{
        for l in self.links:
            po, pi = l.split(self.delim_link)
            links[l] = (po, pi)
            for p in links[l]:
                if not p in self.ports:
                    raise ConfigError('linkport unknown: %s' % p)
                if self.ports[p]['connected']:
                    raise ConfigError('linkport not unique: %s' % p)
                self.ports[p]['connected'] = True
            #-- link outport to inport
            self.ports[po]['feeds'] = pi
            self.ports[pi]['fedby'] = po
        return links
#}}}


    def __compile_chains(self):
        '''compile chains out of links'''
        chains = {}#{{{
        ports = self.ports  #-- shortcut
        #-- each chain ends with a chainend and can be traced back from there
        for l,link in self.links.items():
            pi = link[1]
            pos = ports[pi]['feeds']
            #-- we need an unused chainend port
            for po in pos:
                if ports[po]['connected']: continue
                if ports[po]['terminates']: break
            #-- no unused chainend port found on this link
            else:
                continue

            #-- mark this chainend port as used
            ports[po]['connected'] = True

            #-- chainend found
            chain = [l]
            label = [ports[po]['label']]

            #-- find links up the chain
            po = link[0]
            label.append(ports[po]['label'])
            pi = ports[po]['fedby']
            while not ports[pi]['terminates']:
                po = ports[pi]['fedby']
                if po not in ports:
                    raise ConfigError('chain has no valid start, chainstart termination missing?: %s' % chain)
                label.append(ports[po]['label'])
                l = self.delim_link.join([po, pi])
                assert l in self.links
                chain = [l] + chain
                #-- next inport
                pi = ports[po]['fedby']
            #-- mark this chainstart port as used
            else:
                ports[pi]['connected'] = True

            #-- reverse label
            label = '_'.join([l for l in label[::-1] if l != ''])

            if label in chains:
                raise ConfigError('chain label not unique: %s' % label)
            chains[label] = chain

        #-- are all links part of a chain?
        links = set([l for c in chains for l in chains[c]])
        #-- nonexisting links used
        badlinks = links - set(self.links)
        if badlinks != set():
            raise self.Exception('undefined links got chained: %s' % list(badlinks))
        #-- unused links
        missinglinks = set(self.links) - links
        if missinglinks != set():
            raise ConfigError('links not part of a chain, chainend termination missing?: %s' % list(missinglinks))

        #-- are all outputs of connected inputs connected?
        for p,port in ports.items():
            if port['intent'] != 'in': continue
            if not port['connected']: continue
            for po in port['feeds']:
                if not ports[po]['connected']:
                    raise ConfigError('links leave active output port dangling: %s' % po)

        return chains
#}}}


    def make_old_version(self):
        '''generate old version of the config file for backwards compatibility'''
        backends = ['RX-LO_BDC-LO', 'RX-LO_BDC-HI', 'RX-HI_BDC-LO', 'RX-HI_BDC-HI']#{{{

        chains = {}
        for cname, chain in self.chains.items():
            #-- verify that this is a r2dbe chain
            for link in chain:
                po, pi = link.split(self.delim_link)
                dev, port = pi.split(self.delim_port)
                if dev.split(self.delim_device)[0] == 'r2dbe': break
            else:
                print('WARNING: chain {:} does not contain r2dbe: incompatible with old_version config file!'.format(cname))
                continue

            #-- collect chain properties
            c = chains[cname] = {}
            for link in chain:
                for port in link.split(self.delim_link):
                    dev, p = port.split(self.delim_port)
                    layout = self.devices[dev]['layout']
                    if layout == 'rx':
                        c['pol'] = self.ports[port]['pol']
                        c['rx_sb'] = self.ports[port]['sideband']
                    if (layout == 'bdc_2band'  or  layout == 'bdc_1band')  and  self.ports[port]['intent'] == 'out':
                        c['bdc'] = self.devices[dev]['hostname']
                        c['bdc_sb'] = self.ports[port]['band']
                    if layout == 'r2dbe':
                        c['r2dbe'] = self.devices[dev]['hostname']
                        c['r2dbe_inport'] = p
                    if layout == 'mark6':
                        c['mark6'] = self.devices[dev]['hostname']
                        c['iface'] = p
                        outp = self.ports[port]['feeds'][0].split(self.delim_port)[1]
                        c['mods'] = outp[-2:]

        #-- old config sections cover two chains each
        sections = {}
        for c in chains.values():
            secname = 'RX-%s_BDC-%s' % (c['rx_sb'][:2].upper(), c['bdc_sb'][:2].upper())
            sec = sections.pop(secname, {})

            ifnr = int(c['r2dbe_inport'][-1])
            for k in 'bdc r2dbe mark6'.split():
                sec[k] = c[k]
            for k in 'pol rx_sb bdc_sb iface mods'.split():
                kk = 'if%d_%s' % (ifnr, k)
                sec[kk] = c[k]

            sections[secname] = sec

        seckeys = 'bdc r2dbe mark6 if0_pol if0_rx_sb if0_bdc_sb if0_iface if0_mods if1_pol if1_rx_sb if1_bdc_sb if1_iface if1_mods'

        s = '[global]\n'
        s += 'station={:}\n'.format(self.instruct['stationcode'])
        s += 'backends=' + ','.join(sorted(sections.keys())[::-1]) + '\n'

        for secname in sorted(sections.keys())[::-1]:
            s += '\n[%s]\n' % secname
            sec = sections[secname]
            for k in seckeys.split():
                s += '%s=%s\n' % (k, sec[k])

        return s
#}}}

    def list_chains_with_devices(self):
        devs = {}#{{{
        for c,chain in self.chains.items():
            x = {}
            for link in chain:
                for p in link.split(self.delim_link):
                    d = p.split(self.delim_port)[0]
                    x[d] = None
            devs[c] = list(x)
        devs['all'] = list({v: None for c in devs.values() for v in c})
        return devs
#}}}

    #-- each outport is uniquely linked to an inport, so inports can be derived from this result
    def list_devids_with_outports(self, devtype):
        '''For a given device type, return a dict of output ports reachable through each possible dev selector.
        A dev selector can be a device name, device hostnames, or chain name.
        Example:
            input: bdc
            output keys: all, lsb, usb, bdc1, bdc2, lcp_lsb_lo, rcp_lsb_lo, lcp_lsb_hi, rcp_lsb_hi, lcp_usb_lo, rcp_usb_lo, lcp_usb_hi, rcp_usb_hi
        '''
        assert isinstance(devtype, str)#{{{
        devsels = DictDefaultList()
        allports = {}
        for c,chain in self.chains.items():
            #-- virtually link to connected terminators as output ports
            lastinport = chain[-1].split(self.delim_link)[1]
            terminators = self.ports[lastinport]['feeds']
            chainx = [*chain, *terminators]
            for l in chainx:
                p = l.split(self.delim_link)[0]

                d, pp = p.split(self.delim_port)
                t, devname = d.split(self.delim_device)
                if t != devtype:
                    continue
                dev = self.devices[d] #-- device name
                devsels.setdefault(devname, [])
                devsels[devname].append(p)
                if 'hostname' in dev:
                    devsels.setdefault(dev['hostname'], [])
                    devsels[dev['hostname']].append(p) #-- hostname
                allports.setdefault(p, None)
                devsels[c] = [p] #-- chain name
        devsels['all'] = list(allports)
        return devsels
#}}}
    def outport_to_inport(self, p):
        return self.ports[p]['fedby']

    def list_devids_with_inports(self, devtype):
        '''For a given device type, return a dict of input ports reachable through each possible dev selector.
        A dev selector can be a device name, device hostnames, or chain name.'''
        assert isinstance(devtype, str)#{{{
        devsels = DictDefaultList()
        allports = {}
        for k,v in self.list_devids_with_outports(devtype).items():
            devsels.setdefault(k, [])
            for p in v:
                inport = self.outport_to_inport(p)
                devsels[k].append(inport)
                allports.setdefault(inport, None)
        devsels['all'] = list(allports)
        return devsels
#}}}#}}}

if __name__ == "__main__":
    cfg = main()

# vim: foldmethod=marker
