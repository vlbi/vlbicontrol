#!/bin/python2
# This is part of the VLBIcontrol software.  This software is released under the terms of the MIT Licence.
# Copyright (c) 2018 Radboud RadioLab.  All rights reserved
import template

class Manager(template.Manager):
    '''VLBIcontrol R2DBE device manager'''

    def config_signal_paths(self, cfg):
        '''Extract signal paths that pass through this R2DBE.'''
        dev = cfg.devices[self.devid]
        paths = []
        for up, down in zip(dev['inports'], dev['outports']):
            sideband = None
            pol = None
            band = None
            recorder = None
            iface = None
            #-- go upstream to find analog properties: sideband, polarization, band
            while not (sideband and pol and band):
                up = cfg.ports[up]["fedby"]
                if not up:
                    break
                port = cfg.ports[up]
                sideband = port["sideband"] if "sideband" in port.keys() and sideband is None else sideband
                band = port["band"] if "band" in port.keys() and band is None else band
                pol = port["pol"] if "pol" in port.keys() and pol is None else pol
            #-- go downstream to find recorder, interface
            down = cfg.ports[down]["feeds"]
            if not down:
                break
            port = cfg.ports[down]
            iface = port["name"]
            recorder = cfg.devices[port["devid"]]["hostname"]
            #-- append path
            paths.append((sideband, pol, band, recorder, iface))


    '''Initialize from scratch the device to be managed.'''
    paths = self.config_signal_paths(cfg)

# vim: foldmethod=marker
