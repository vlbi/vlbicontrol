#!/bin/python2
from __future__ import print_function
# This is part of the VLBIcontrol software.  This software is released under the terms of the MIT Licence.
# Copyright (c) 2018 Radboud RadioLab.  All rights reserved
import threading

class Manager(object):
    '''VLBIcontrol device manager'''
    def __init__(self, devid):
        self.devid = devid

        self.abortStart = threading.Event()
        self.abortCompleted = threading.Event()


    def stop(self):
        '''Stop the perpetual manage loop.'''
        print('stopping manager %s' % self.devid)
        self.abortStart.set()


    def run(self):
        '''Start the perpetual manage loop.'''
        print('starting manager %s' % self.devid) #{{{
        self.abortCompleted.clear()
        self.abortStart.clear()
        while not self.abortStart.is_set():
            self.abortStart.wait(1)
        self.abortCompleted.set()
    #}}}


    def teardown(self):
        '''Uninitialize the device to be managed.'''
        pass


    def setup(self, cfg):
        '''Initialize from scratch the device to be managed.'''
        self._set_once()
        self._set_anytime()


    def reconfigure(self):
        '''Update the configuration of the device to be managed.
        This will typically be run between stop() and start() calls.'''
        self._set_anytime()


    def _set_anytime(self):
        '''Configure things that can be changed anytime, runtime parameters.'''
        pass


    def _set_once(self):
        '''Configure things that can only be set once, startup parameters.'''
        pass

# vim: foldmethod=marker
