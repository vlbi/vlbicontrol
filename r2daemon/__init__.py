import sys
if sys.version_info[0] < 3:
    from r2daemon import decode
else:
    from .r2daemon import decode
