from __future__ import print_function

import os
import base64
import random
import struct
import sys
import time

import redis

if sys.version_info[0] < 3:
    from daemon import Daemon
    from r2mmap import MemoryMappedR2DBE, R2DBE_MEMORY_FILE
else:
    from .daemon import Daemon
    from .r2mmap import MemoryMappedR2DBE, R2DBE_MEMORY_FILE

def decode(data):

    return base64.b64decode(data)

class R2DBEDaemon(Daemon, MemoryMappedR2DBE):

    PERIOD = 1.0

    EXPORT_LIST = [
      "r2dbe_data_mux_0_sel",
      "r2dbe_data_mux_1_sel",
      "r2dbe_delay_0_samples",
      "r2dbe_delay_1_samples",
      "r2dbe_monitor0_counts",
      "r2dbe_monitor0_power",
      "r2dbe_monitor1_counts",
      "r2dbe_monitor1_power",
      "r2dbe_onepps_alive",
      "r2dbe_onepps_gps_pps_cnt",
      "r2dbe_onepps_gps_pps_per",
      "r2dbe_onepps_msr_pps_cnt",
      "r2dbe_onepps_msr_pps_per",
      "r2dbe_onepps_offset",
      "r2dbe_onepps_since_epoch",
      "r2dbe_quantize_0_thresh",
      "r2dbe_quantize_1_thresh",
      "r2dbe_snap_2bit_0_data_bram",
      "r2dbe_snap_2bit_1_data_bram",
      "r2dbe_snap_8bit_0_data_bram",
      "r2dbe_snap_8bit_1_data_bram",
      "r2dbe_tengbe_0_core",
      "r2dbe_tengbe_0_dest_ip",
      "r2dbe_tengbe_0_dest_port",
      "r2dbe_tengbe_1_core",
      "r2dbe_tengbe_1_dest_ip",
      "r2dbe_tengbe_1_dest_port",
      "r2dbe_vdif_0_hdr_w0_sec_ref_ep",
      "r2dbe_vdif_0_hdr_w1_ref_ep",
      "r2dbe_vdif_0_hdr_w3_station_id",
      "r2dbe_vdif_0_hdr_w3_thread_id",
      "r2dbe_vdif_0_hdr_w4",
      "r2dbe_vdif_1_hdr_w0_sec_ref_ep",
      "r2dbe_vdif_1_hdr_w1_ref_ep",
      "r2dbe_vdif_1_hdr_w3_station_id",
      "r2dbe_vdif_1_hdr_w3_thread_id",
      "r2dbe_vdif_1_hdr_w4",
    ]

    def __init__(self, name, redishost="localhost", redisport=6379, memfile=R2DBE_MEMORY_FILE):
        self._name = name
        self._redis = redis.Redis(host=redishost, port=redisport)
        pidfile = os.getcwd() + "/r2daemon.pid"
        Daemon.__init__(self, pidfile)
        MemoryMappedR2DBE.__init__(self, memfile=memfile)

    def run(self, verbose=False):
        snaps = ["r2dbe_snap_%dbit_%d_data" % (b,c) for b in [8, 2] for c in [0, 1]]
        print("run: entering loop")
        n_iter = 0
        while True:
            t0 = time.time()
            alive = self.read_int("r2dbe_onepps_alive")
            # only process data if alive
            if alive:
                try:
                    # first update memory file with snapshot data and refresh cache
                    #  1) claim snapshot
                    #  2) arm and wait
                    #  3) refresh cache
                    #  4) yield snapshot
                    lock_name, lock_value = self._claim_snapshot()
                    snap_result = self.snapshots_arm_and_wait(snaps, man_trig=True)
                    if not snap_result:
                        print("snapshot timeout at %.3f" % t0)
                        continue
                    self.refresh()
                    self._yield_snapshot(lock_name, lock_value)
                    #   .. then read data from cache
                    btable = self.get_bytes_table(use_cache=True, keys=self.EXPORT_LIST)
                    # post results
                    self._post_monitor_parameters(btable)
                    # notify subscribers
                    self._notify_update()
                except redis.exceptions.ConnectionError as ce:
                    print("ConnectionError encountered: %s" % str(ce))
            # compute waiting time and sleep
            t1 = time.time()
            wait = self.PERIOD - (t1 - t0) - 0.01
            if wait > 0.0:
                time.sleep(wait)
            # wait until alive ticks over to next second
            while alive and self.read_uint("r2dbe_onepps_alive") == alive:
                time.sleep(0.001)
            n_iter += 1
            if args.verbose or (n_iter % 1000 == 0):
                print("run: n_iter = %d")
        print("run: exited loop")

    def _safe_snapshots_arm_and_wait(self):
        snaps = ["r2dbe_snap_%dbit_%d_data" % (b,c) for b in [8, 2] for c in [0, 1]]
        lock_name, lock_value = self._claim_snapshot()
        result = self.snapshots_arm_and_wait(snaps)
        self._yield_snapshot(lock_name, lock_value)

        return result

    def _claim_snapshot(self):
        lock_name = "%s.lock.snap" % self._name
        lock_value = "%8x" % random.randint(0,2**32-1)
        # write unique value that expires after few seconds
        # call returns True only if key does not exist
        while not self._redis.set(lock_name, lock_value, ex=10, nx=True):
            time.sleep(0.05)

        # caller will need lock name / value to unlock afterwards
        return lock_name, lock_value

    def _yield_snapshot(self, lock_name, lock_value):
        # only delete lock if it matches value
        if self._redis.get(lock_name) == lock_value:
            self._redis.delete(lock_name)

    def _post_monitor_parameters(self, table):
        mapping = dict()
        for k,v in table.items():
            key = "%s.raw.%s" % (self._name, k)
            mapping[key] = self._encode(v)
        # add timestamp
        mapping["%s.raw._last_update" % self._name] = self._last_update

        self._redis.mset(mapping)

    def _notify_update(self):
        chan = "%s.notice" % self._name
        self._redis.publish(chan,"new data at %.3f" % time.time())

    def _encode(self, value):

        return base64.b64encode(value)

def _int_handler(signum, frame):
    sys.exit(0)

if __name__ == "__main__":
    import argparse
    import sys
    import signal
    import socket

    parser = argparse.ArgumentParser()
    parser.add_argument("--r2dbe", type=str, default=socket.gethostname(), help="R2DBE host")
    parser.add_argument("--redis", type=str, default="localhost", help="redis host")
    parser.add_argument("--memfile", type=str, default=R2DBE_MEMORY_FILE, help="device memory file")
    parser.add_argument("--nodaemon", action="store_true", help="run in foreground (non-daemon mode)")
    parser.add_argument("--stop", action="store_true", help="stop running daemon")
    parser.add_argument("--verbose", action="store_true", help="print verbose messages")
    args = parser.parse_args()

    r2d = R2DBEDaemon(args.r2dbe, redishost=args.redis, memfile=args.memfile)

    if args.stop:
        print("stop")
        r2d.stop()
        sys.exit(0)

    if args.nodaemon:
        print("running in non-daemon mode")
        signal.signal(signal.SIGINT, _int_handler)
        r2d.run(verbose=args.verbose)

    print("start")
    r2d.start()
