from __future__ import print_function

import functools
import time
import signal
import sys

import redis

from r2daemon import R2DBEDaemon

def report(r, message):
    r2dbe = message["channel"].split(".")[0]
    print("%s: %s" % (r2dbe, message["data"]))
    for k in r.keys("%s.raw.*" % r2dbe):
        s = R2DBEDaemon.decode(r.get(k))
        print("%s = %s" % (k, s[:32]))

def terminate(t_ps, signum, frame):
    print("exiting")
    t_ps.stop()
    time.sleep(0.1)
    sys.exit(0)

if __name__ == "__main__":

    print("subscribe to channel")
    r = redis.Redis()
    if len(sys.argv) > 1 and sys.argv[1] == "--clear":
        print("clearing register")
        for k in r.keys("r2dbe*.raw.*"):
            r.delete(k)
    p = r.pubsub()
    p.psubscribe(**{"r2dbe*.notice": functools.partial(report, r)})
    t_pubsub = p.run_in_thread()

    print("register interrupt")
    signal.signal(signal.SIGINT, functools.partial(terminate, t_pubsub))

    print("running")
    while True:
        time.sleep(1)
