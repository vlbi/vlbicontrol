#!/usr/bin/env python2
from __future__ import print_function

import unittest

from r2mmap import (
  MemoryMappedR2DBE,
  R2DBE_TABLE,
  R2DBE_USER_SPACE_TABLE,
  R2DBE_USER_SIZE,
  R2DBE_USER_OFFSET,
)

class R2MMConstantsTests(unittest.TestCase):

    def test_r2dbe_user_size(self):
        # check if user size is what it is
        raise unittest.SkipTest("not implemented yet")

    def test_r2dbe_user_offset(self):
        # check if user offset is what it is
        raise unittest.SkipTest("not implemented yet")

    def test_r2dbe_user_table(self):
        # check if
        # 1) all keys in user table are in global table
        # 2) relative positions are correct
        # 3) sizes of each entry match
        raise unittest.SkipTest("not implemented yet")

class R2MMReadTests(unittest.TestCase):

    def setUpClass(self):
        # create random byte table, write to file
        pass

    def test_get_bytes_by_name_uncached(self):
        # read each entry and compare to reference table
        raise unittest.SkipTest("not implemented yet")

    def test_get_bytes_table_uncached(self):
        # read entire table and compare to reference
        raise unittest.SkipTest("not implemented yet")

    def test_caching(self):
        # possibly multiple tests, but steps like
        # 1) refresh
        # 2) write change to file
        # 3) read with use_cache=True --> should give old value
        # 4) read with use_cache=False --> should give new value
        raise unittest.SkipTest("not implemented yet")

    def tearDownClass(self):
        # remove dummy file
        pass
