import mmap
import struct
import time

FLAG_READ = 1
FLAG_WRITE = 2

_table_entry = lambda base, end, size, repeat, flags: dict([
  ("base", base),
  ("end", end),
  ("size", size),
  ("repeat",repeat),
  ("flags",flags),
])

R2DBE_TABLE = {
  "sys_board_id": _table_entry(0x00000000, 0x00000004, 4, 0, FLAG_READ),
  "sys_rev": _table_entry(0x00000004, 0x00000008, 4, 0, FLAG_READ),
  "sys_rev_rcs": _table_entry(0x00000008, 0x0000000c, 4, 0, FLAG_READ),
  "sys_scratchpad": _table_entry(0x0000000c, 0x00000010, 4, 0, FLAG_READ),
  "sys_clkcounter": _table_entry(0x00000010, 0x00020000, 4, 0, FLAG_READ),
  "adc5g_controller": _table_entry(0x00020000, 0x00060000, 4096, 0, FLAG_READ),
  "sfp_gpio_data_in": _table_entry(0x00060000, 0x00060004, 4, 0, FLAG_READ),
  "sfp_gpio_data_out": _table_entry(0x00060004, 0x00060008, 4, 0, FLAG_READ),
  "sfp_gpio_data_oe": _table_entry(0x00060008, 0x0006000c, 4, 0, FLAG_READ),
  "sfp_gpio_data_ded": _table_entry(0x0006000c, 0x00060010, 4, 0, FLAG_READ),
  "sfp_mdio_sel": _table_entry(0x00060010, 0x00060014, 4, 0, FLAG_READ),
  "sfp_op_issue": _table_entry(0x00060014, 0x00060018, 4, 0, FLAG_READ),
  "sfp_op_type": _table_entry(0x00060018, 0x0006001c, 4, 0, FLAG_READ),
  "sfp_op_addr": _table_entry(0x0006001c, 0x00060020, 4, 0, FLAG_READ),
  "sfp_op_data": _table_entry(0x00060020, 0x00060024, 4, 0, FLAG_READ),
  "sfp_op_result": _table_entry(0x00060024, 0x00060028, 4, 0, FLAG_READ),
  "sfp_op_dbg": _table_entry(0x00060028, 0x0006002c, 4, 0, FLAG_READ),
  "sfp_op_dbg1": _table_entry(0x0006002c, 0x00060030, 4, 0, FLAG_READ),
  "r2dbe_data_mux_0_sel": _table_entry(0x01000000, 0x01000100, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_data_mux_1_sel": _table_entry(0x01000100, 0x01000200, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_delay_0_samples": _table_entry(0x01000200, 0x01000300, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_delay_1_samples": _table_entry(0x01000300, 0x01020000, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_monitor0_counts": _table_entry(0x01020000, 0x01040000, 131072, 0, FLAG_READ),
  "r2dbe_monitor0_power": _table_entry(0x01040000, 0x01060000, 131072, 0, FLAG_READ),
  "r2dbe_monitor1_counts": _table_entry(0x01060000, 0x01080000, 131072, 0, FLAG_READ),
  "r2dbe_monitor1_power": _table_entry(0x01080000, 0x010a0000, 131072, 0, FLAG_READ),
  "r2dbe_noise_gen_0_arm": _table_entry(0x010a0000, 0x010a0100, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_noise_gen_0_seed": _table_entry(0x010a0100, 0x010a0200, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_noise_gen_1_arm": _table_entry(0x010a0200, 0x010a0300, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_noise_gen_1_seed": _table_entry(0x010a0300, 0x010a0400, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_onepps_alive": _table_entry(0x010a0400, 0x010a0500, 4, 0, FLAG_READ),
  "r2dbe_onepps_ctrl": _table_entry(0x010a0500, 0x010a0600, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_onepps_gps_pps_cnt": _table_entry(0x010a0600, 0x010a0700, 4, 0, FLAG_READ),
  "r2dbe_onepps_gps_pps_per": _table_entry(0x010a0700, 0x010a0800, 4, 0, FLAG_READ),
  "r2dbe_onepps_msr_pps_cnt": _table_entry(0x010a0800, 0x010a0900, 4, 0, FLAG_READ),
  "r2dbe_onepps_msr_pps_per": _table_entry(0x010a0900, 0x010a0a00, 4, 0, FLAG_READ),
  "r2dbe_onepps_offset": _table_entry(0x010a0a00, 0x010a0b00, 4, 0, FLAG_READ),
  "r2dbe_onepps_since_epoch": _table_entry(0x010a0b00, 0x010a0c00, 4, 0, FLAG_READ),
  "r2dbe_quantize_0_thresh": _table_entry(0x010a0c00, 0x010a0d00, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_quantize_1_thresh": _table_entry(0x010a0d00, 0x010b0000, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_snap_2bit_0_data_bram": _table_entry(0x010b0000, 0x010c0000, 65536, 0, FLAG_READ),
  "r2dbe_snap_2bit_0_data_ctrl": _table_entry(0x010c0000, 0x010c0100, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_snap_2bit_0_data_status": _table_entry(0x010c0100, 0x010d0000, 4, 0, FLAG_READ),
  "r2dbe_snap_2bit_1_data_bram": _table_entry(0x010d0000, 0x010e0000, 65536, 0, FLAG_READ),
  "r2dbe_snap_2bit_1_data_ctrl": _table_entry(0x010e0000, 0x010e0100, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_snap_2bit_1_data_status": _table_entry(0x010e0100, 0x01100000, 4, 0, FLAG_READ),
  "r2dbe_snap_8bit_0_data_bram": _table_entry(0x01100000, 0x01140000, 262144, 0, FLAG_READ),
  "r2dbe_snap_8bit_0_data_ctrl": _table_entry(0x01140000, 0x01140100, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_snap_8bit_0_data_status": _table_entry(0x01140100, 0x01180000, 4, 0, FLAG_READ),
  "r2dbe_snap_8bit_1_data_bram": _table_entry(0x01180000, 0x011c0000, 262144, 0, FLAG_READ),
  "r2dbe_snap_8bit_1_data_ctrl": _table_entry(0x011c0000, 0x011c0100, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_snap_8bit_1_data_status": _table_entry(0x011c0100, 0x011c4000, 4, 0, FLAG_READ),
  "r2dbe_tengbe_0_core": _table_entry(0x011c4000, 0x011c8000, 16384, 0, FLAG_READ),
  "r2dbe_tengbe_0_dest_ip": _table_entry(0x011c8000, 0x011c8100, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_tengbe_0_dest_port": _table_entry(0x011c8100, 0x011c8200, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_tengbe_0_rst": _table_entry(0x011c8200, 0x011cc000, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_tengbe_1_core": _table_entry(0x011cc000, 0x011d0000, 16384, 0, FLAG_READ),
  "r2dbe_tengbe_1_dest_ip": _table_entry(0x011d0000, 0x011d0100, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_tengbe_1_dest_port": _table_entry(0x011d0100, 0x011d0200, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_tengbe_1_rst": _table_entry(0x011d0200, 0x011d0300, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_buffer_data_rst": _table_entry(0x011d0300, 0x011d0400, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_enable": _table_entry(0x011d0400, 0x011d0500, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_hdr_w0_reset": _table_entry(0x011d0500, 0x011d0600, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_hdr_w0_sec_ref_ep": _table_entry(0x011d0600, 0x011d0700, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_hdr_w1_ref_ep": _table_entry(0x011d0700, 0x011d0800, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_hdr_w3_station_id": _table_entry(0x011d0800, 0x011d0900, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_hdr_w3_thread_id": _table_entry(0x011d0900, 0x011d0a00, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_hdr_w4": _table_entry(0x011d0a00, 0x011d0b00, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_little_end": _table_entry(0x011d0b00, 0x011d0c00, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_reorder_2b_samps": _table_entry(0x011d0c00, 0x011d0d00, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_0_test_sel": _table_entry(0x011d0d00, 0x011d0e00, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_buffer_data_rst": _table_entry(0x011d0e00, 0x011d0f00, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_enable": _table_entry(0x011d0f00, 0x011d1000, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_hdr_w0_reset": _table_entry(0x011d1000, 0x011d1100, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_hdr_w0_sec_ref_ep": _table_entry(0x011d1100, 0x011d1200, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_hdr_w1_ref_ep": _table_entry(0x011d1200, 0x011d1300, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_hdr_w3_station_id": _table_entry(0x011d1300, 0x011d1400, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_hdr_w3_thread_id": _table_entry(0x011d1400, 0x011d1500, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_hdr_w4": _table_entry(0x011d1500, 0x011d1600, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_little_end": _table_entry(0x011d1600, 0x011d1700, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_reorder_2b_samps": _table_entry(0x011d1700, 0x01200000, 4, 64, FLAG_READ | FLAG_WRITE),
  "r2dbe_vdif_1_test_sel": _table_entry(0x01200000, 0x01200100, 4, 64, FLAG_READ | FLAG_WRITE),
  "rcs_app": _table_entry(0x01200100, 0x01200200, 4, 0, FLAG_READ),
  "rcs_lib": _table_entry(0x01200200, 0x01200300, 4, 0, FLAG_READ),
  "rcs_user": _table_entry(0x01200300, 0x01200400, 4, 0, FLAG_READ),
}

# define user-space block
R2DBE_USER_SPACE_TABLE = dict([(k, R2DBE_TABLE[k]) for k in R2DBE_TABLE.keys() if k.find("r2dbe") == 0])
R2DBE_USER_OFFSET = min([v["base"] for v in R2DBE_USER_SPACE_TABLE.values()])
R2DBE_USER_SIZE = max([v["end"] for v in R2DBE_USER_SPACE_TABLE.values()]) - R2DBE_USER_OFFSET
for k,v in R2DBE_USER_SPACE_TABLE.items():
    R2DBE_USER_SPACE_TABLE[k] = _table_entry(
      v["base"] - R2DBE_USER_OFFSET,
      v["end"] - R2DBE_USER_OFFSET,
      v["size"],
      v["repeat"],
      v["flags"],
    )

R2DBE_MEMORY_FILE = "/dev/roach/mem"

class MemoryMappedR2DBEError(Exception):
    pass

class MemoryMappedR2DBE(object):

    def __init__(self, offset=R2DBE_USER_OFFSET, size=R2DBE_USER_SIZE,
      memfile=R2DBE_MEMORY_FILE, mapping=R2DBE_USER_SPACE_TABLE):
        self._file = open(memfile, "ab+")
        self._mmap = mmap.mmap(self._file.fileno(), size, mmap.MAP_SHARED, mmap.PROT_READ | mmap.PROT_WRITE, offset=offset)
        self._offset = offset
        self._table = mapping
        self.refresh()

    def close(self):
        self._mmap.close()
        self._file.close()

    def refresh(self):
        self._cache = self._mmap[:]
        self._last_update = time.time()

    def get_bytes_table(self, use_cache=False, keys=None):
        if not keys:
            keys = self._table.keys()
        table = dict([(k, "") for k in keys])
        for k in keys:
            table[k] = self.get_bytes_by_name(k, use_cache=use_cache)

        return table

    def get_bytes_by_name(self, name, use_cache=False):
        if name not in self._table.keys():
            raise MemoryMappedR2DBEError("Device '%s' not defined in mapping" % name)

        if not self._table[name]["flags"] & FLAG_READ:
            raise MemoryMappedR2DBEError("Device '%s' is not read-enabled" % name)

        start = self._table[name]["base"]
        stop = start + self._table[name]["size"]
        if use_cache:

            return self._cache[start:stop]

        return self._mmap[start:stop]

    def set_bytes_by_name(self, name, value):
        if name not in self._table.keys():
            raise MemoryMappedR2DBEError("Device '%s' not defined in mapping" % name)

        if not self._table[name]["flags"] & FLAG_WRITE:
            raise MemoryMappedR2DBEError("Device '%s' is not write-enabled" % name)

        if len(value) != self._table[name]["size"]:
            raise MemoryMappedR2DBEError("Device '%s' size is %d but bytes to write are %d" % (name, self._table[name]["size"], len(value)))

        start = self._table[name]["base"]
        stop = self._table[name]["end"]
        self._mmap[start:stop] = value * self._table[name]["repeat"]

    def read_int(self, name):
        return struct.unpack(">i", self.get_bytes_by_name(name))[0]

    def read_uint(self, name):
        return struct.unpack(">I", self.get_bytes_by_name(name))[0]

    def write_int(self, name, value):
        self.set_bytes_by_name(name, struct.pack(">i", value))

    def _snapshot_arm(self, name, man_trig=False):
        self.write_int(name + "_ctrl", (0 + (man_trig<<1)))
        self.write_int(name + "_ctrl", (1 + (man_trig<<1)))

    def snapshot_arm_and_wait(self, name, man_trig=False, wait_period=0.001, timeout=5.0):
        self._snapshot_arm(name, man_trig=man_trig)
        tstart = time.time()
        time.sleep(wait_period)
        while True:
            status = self.read_uint(name + "_status")
            if status >> 31 == 0:
                break
            time.sleep(0.001)
            if time.time() - tstart > timeout:

                return False

        return True

    def snapshots_arm_and_wait(self, names, man_trig=False, wait_period=0.001, timeout=5.0):
        # make auxiliary list of names, code below removes entries
        aux_names = list(names)
        for name in aux_names:
            self._snapshot_arm(name, man_trig=man_trig)
        tstart = time.time()
        time.sleep(wait_period)
        result = dict([(name, "") for name in aux_names])
        while len(aux_names) > 0:
            for name in aux_names:
                status = self.read_uint(name + "_status")
                if status >> 31 == 1:
                    continue
                aux_names.remove(name)
            time.sleep(0.001)
            if time.time() - tstart > timeout:
                return False

        return True
