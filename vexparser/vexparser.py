#!/usr/bin/python
from __future__ import print_function
'''
A Vexfile consists of three types of declarations:
1. named Sections
2. named Paragraphs
3. named Values

A Vexfile is line-based; a complete line can be a commented out by
a leading '*' character.

One line can contain multiple declarations; each declaration ends
with a ';' character.

Declarations can span multiple lines, i.e. a number of
unterminated lines followed by a terminated line.

A Section declaration starts with a '$' symbol.

A paragraph declaration start with identifier 'def' or 'scan' and
ends with identifier 'enddef' or 'endscan'.

Section and Paragraph names are unique.  Value names may be
repeated.

A Vexfile is loaded into nested objects in which Sections and
Paragraphs are objects themselves.  Each nesting level in the
object (Vexfile, Section, or Paragraph) can contain named Values
(key-value pairs).  The value in a key-value pair is of type
'string' (in single-value cases) or of type 'array' (in
multi-value cases).

Vexfixarrays compares data types of identically named Values
across sister objects and creates single-element arrays where
arrays are expected.
'''

import binascii
import hashlib
import time, calendar

def vexparser(filename):
    '''Parses vexfile into a python dict'''
    hash_ = ""
    stack = []
    obj = {'filename': filename}

    with open(filename, 'r') as f:
        lines = f.readlines()

    i = 0
    warn = lambda s: print('parse error: %s(%d): %s' % (filename, i, s))

    prev, pardef = '', ''

    for i in range(len(lines)):
        if len(lines[i]) == 0: continue  # empty
        if lines[i][0] == '*': continue  # comment
        #-- line continuation
        fullline = prev + lines[i].strip()
        prev = ''

        sublines = fullline.split(';')
        for j, line in enumerate(sublines):
            line = line.strip()
            if len(line) == 0: continue  # empty
            if line[0] == '*': continue  # comment
            #-- continued line, otherwise the last subline would have len 0
            if j == len(sublines) - 1:
                prev = line
                break
            hash_ += line
            #-- section declaration
            if line[0] == '$':
                name = line
                if len(stack) == 0: stack.append(obj)
                while len(stack) > 1:
                    warn('sec: stacklevel %d, should be 1' % len(stack))
                    stack.pop()
                stack[0][name] = {}
                obj = stack[0][name]
                #-- paragraph definition identifier
                pardef = 'def'
                if name == '$SCHED': pardef = 'scan'
                continue
            #-- paragraph declaration
            if line[0 : len(pardef)+1] == pardef + ' ':
                name = line[len(pardef) + 1 :]
                if len(stack) == 1:
                    stack.append(obj)
                else:
                    warn('par: stacklevel %d, should be 1' % len(stack))
                obj[name] = {}
                obj = obj[name]
                continue
            if line == 'end%s' % pardef:
                if len(stack) == 2:
                    obj = stack.pop()
                else:
                    warn('end%s: stacklevel %d, should be 2' % (pardef, len(stack)))
                continue
            #-- parameter declaration
            kv = list(map(str.strip, line.split('=')))
            if len(kv) != 2:
                warn('invalid parameter declaration: %s' % line)
                continue
            k, v = kv
            if not k in obj:
                obj[k] = v
            else:
                #if type(obj[k]) is not list: print('create list:', k)
                if type(obj[k]) is not list: obj[k] = [obj[k]]
                obj[k].append(v)
    #-- last paragraph was incomplete
    if len(stack) > 1:
        print('last paragraph unterminated, drop it:', filename, name)
        del stack[-1][name]
    #-- incomplete input may leave the stack empty
    if len(stack) > 0: obj = stack[0]
    #-- checksum
    obj['checksum'] = binascii.crc32(bytes(ord(c) for c in hash_)) % (1 << 32)  # convert to 32bit unsigned int
    obj['md5sum'] = hashlib.md5(bytes(ord(c) for c in ''.join(lines))).hexdigest()

    return obj


def vexfixlists(o, ro={}):
    '''Fixes inconsistent data types across sister objects by
    creating single-element lists where lists are expected.'''
    if isinstance(o, list): items = enumerate(o)
    else: items = o.items()
    sib = {}
    for k,v in list(items):
        if isinstance(v, list)  or  isinstance(v, dict):
            vexfixlists(v, sib)
            #if k in ro  and  not isinstance(ro[k], list):
            if len(sib) == 0: sib = v
            continue
        if not k in ro: continue
        if isinstance(ro[k], list):
            print('fix list:', k, v)
            o[k] = list(v)


def vexexpand(vex, o=None):
    '''Replaces references with the contents of the referenced objects.'''
    if o is None: o = vex
    for k,v in list(o.items()):
        if type(v) == dict: vexexpand(vex, v)
        if type(v) != str: continue
        if k[:4] != 'ref ': continue
        ref = k[4:]
        if ref not in vex: continue
        if v not in vex[ref]: continue
        if type(vex[ref][v]) is not dict: continue

        #-- replace reference with the contents of the object it refers to
        del o[k]
        o.update(vex[ref][v])


def vex2time(vextime):
    '''Express vextime in seconds since 1970-01-01 UTC (unix time)'''
    if type(vextime) != str: return None
    return calendar.timegm(time.strptime(vextime, '%Yy%jd%Hh%Mm%Ss'))

if __name__ == '__main__':
    import sys
    for fname in sys.argv[1:]:
        vex = vexparser(fname)
        vexfixlists(vex)
        vexexpand(vex)
        print(vex['filename'], vex['checksum'], vex['md5sum'])

        #print(vex['$GLOBAL'])
        for k,v in vex['$SCHED'].items():
            #print(k, type(v['station']))
            pass
        #vextime = vex['$GLOBAL']['exper_nominal_start']
        #t = vex2time(vextime)
        #print(vextime, t, time.gmtime(t))

# vim: tw=66 fo-=t
