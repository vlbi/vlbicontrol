#!/usr/bin/python3
# This is part of the VLBIcontrol software.  This software is released under the terms of the MIT Licence.
# Copyright (c) 2018 Radboud RadioLab.  All rights reserved

# Generate a graph from the devices and links defined in the config file.

#-- std modules
import os
import argparse
#-- 3rd-party modules
import pydot
#-- custom
import config

#-- setup parser
parser = argparse.ArgumentParser()
parser.add_argument('fname', help='Name of input file')
parser.add_argument('format', default='pdf', help='File type to generage')
#-- parse arguments
args = parser.parse_args()

#-- read config file
cfg = config.Config(args.fname)

#-- use colors to distinguish different device types
color = {
    'rx': '#eebbff',
    'bdc': '#aaffcc',
    'dbbc': '#ffffcc',
    'digitizer': '#ffddbb',
    'recorder': '#bbeeff',
    'terminator': '#dddddd'}

graph = pydot.Dot(graph_type='digraph')

#-- translate device name to something graphviz understands
translate = lambda x: x.replace('.', '_')

#-- define nodes for each device
for d,dev in cfg.devices.items():
    #-- skip unused device
    if not dev['connected']: continue

    #-- a device is represented by table with rows and cells
    #-- input and output rows
    rowin, rowout = [], []
    for pi in sorted(dev['inports']):
        n = cfg.ports[pi]['name']
        outs = cfg.ports[pi]['feeds']
        nout = len(outs)
        rowin.append('<td colspan="%d" port="%s">%s</td>' % (nout, n, n))
        for po in sorted(outs):
            n = cfg.ports[po]['name']
            rowout.append('<td port="%s">%s</td>' % (n, n))

    #-- device row
    nout = len(dev['outports'])
    d_display = d + ('<br />' + dev['hostname'] if 'hostname' in dev.keys() else '')
    rowdev = '<td bgcolor="%s" colspan="%d">%s</td>' % (color[dev['role']], nout, d_display)

    #-- compile table
    table = '''<table BORDER="0" CELLBORDER="1" CELLSPACING="0">
    <tr>%s</tr> <tr>%s</tr> <tr>%s</tr>
</table>''' % (''.join(rowin), rowdev, ''.join(rowout))

    devname = translate(d)
    n = pydot.Node(devname, shape='plain', label='<%s>'%table)
    graph.add_node(n)

#-- define edges for each link
links = [translate(l) for l in cfg.links]
for l in links:
    ll, lr = l.split()
    edge = pydot.Edge(ll, lr + ':n')
    #edge = pydot.Edge(ll + ':s', lr)
    graph.add_edge(edge)

#-- add chain labels
for c,chain in cfg.chains.items():
    n = pydot.Node(c, shape='box', color='gray', style='rounded')
    graph.add_node(n)

    l = chain[-1]
    p = cfg.links[l][-1]
    term = cfg.ports[p]['feeds'][0]

    ll, lr = translate(term), c
    edge = pydot.Edge(ll, lr, arrowhead='none', color='gray', len=3)
    graph.add_edge(edge)

fname = os.path.basename(args.fname)
fnout = '/tmp/' + fname + '.' + args.format
print('saving:', fnout)
graph.write(fnout, format=args.format)
