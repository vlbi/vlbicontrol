#!/bin/bash
# extract station code from backend config file
beconf=/etc/backend.conf
sc_line=`grep stationcode $beconf`
sc_parts=(${sc_line//:/})
sc=${sc_parts[1]}

# create timestamp
datestamp=`date +%Y-%m-%d`
year=`date +%Y`

# output directory
outdir=Mk6_logs_${sc}_${datestamp}
mkdir $outdir

# copy log files here
for ii in {1..4} ; do
	host=`ssh oper@recorder${ii} hostname`
	if [ $? != 0 ] ; then
		continue
	fi
	mkdir $outdir/$host
	# c/d-plane logs
	scp oper@recorder${ii}:/var/log/mark6/[cd]plane-daemon.log* $outdir/$host
	# M6 logs
	scp oper@recorder${ii}:/var/log/mark6/M6-${year}*log* $outdir/$host
	# M6_CC logs
	scp oper@recorder${ii}:/home/oper/M6_CC-${sc}-*.log $outdir/$host
done

# create tarball
tar -cf ${outdir}.tar ${outdir}
gzip ${outdir}.tar

# delete outdir
rm -r ${outdir}
