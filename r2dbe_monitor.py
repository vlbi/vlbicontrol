#!/usr/bin/python
import collections
import datetime
import multiprocessing
import numpy as np
import sys
import threading
import time
import traceback

import redis

import config
import becs.common
import becs.config
import becs.r2dbe as device
import becs.r2dbe.data as r2data
import becs.ui as ui
import r2daemon.r2daemon as r2daem
import vdif

# make instance generator function
get_r2dbe_inst = lambda name, cfg: becs.config.get_r2dbe_inst(name, cfg, device.R2DBE)

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# matplotlib logger is too busy
mpl_logger = logging.getLogger("matplotlib")
mpl_logger.setLevel(logging.WARNING)

_stop_lock = threading.Semaphore()
_stop = False

def handle_close(evt):
    global _stop_lock
    global _stop
    # Acquire lock and set stop condition
    with _stop_lock:
        time.sleep(0.2)
        logger.debug("handle_close: _stop <-- True")
        _stop = True

def test_restart_r2deamon(args):
    if not args.r2dbe.test_r2daemon_running():
        logger.info("%s (hostname %s) r2daemon not running, starting r2daemon ...", args.device, args.r2dbe.host)
        if not args.r2dbe.start_r2daemon():
            logger.info("%s (hostname %s) ... r2daemon start failed, aborting", args.device, args.r2dbe.host)
            return False
        # give deamon chance to start up and populate keys
        time.sleep(3)
        logger.info("%s (hostname %s) ... r2deamon started", args.device, args.r2dbe.host)
    return True

def get_r2dbe_data(args):
    result = {}
    def redis_key(k):
        return bytes(ord(c) for c in (args.device + ".raw." + k))
    def get_value(name):
        param_dict = r2data.lookup_param(name)
        binary = r2daem.decode(args.redis[redis_key(param_dict["dev"])])
        return param_dict["decode"](binary)
    # numerical data
    for ch in [0, 1]:
        # 2-bit and 8-bit spectra & 2-bit counts
        for b in [2, 8]:
            x = range(2048)
            unpack = getattr(r2data, "unpack_%dbit_data" % b)
            samples = np.array(unpack(r2daem.decode(args.redis[redis_key("r2dbe_snap_%dbit_%d_data_bram" % (b, ch))])))
            # centre 2-bit data around zero for spectra
            offset = -samples.mean() if b == 2 else 0.0
            y = np.fft.fft(samples.reshape((-1, len(x)*2)) + offset, axis=-1)
            y = abs((y * y.conj()).mean(axis=0))[:len(x)]
            result["%dbit_spectrum_%d" % (b, ch)] = x, 10*np.log10(y/max(y))
            if b == 2:
                x = range(-2,2)
                #~ logger.debug("y = [%s]", ", ".join("%.2f" % yy for yy in [100.0*len(samples[samples == i])/len(samples) for i in x]))
                result["2bit_counts_%d" % ch] = x, [100.0*len(samples[samples == i])/len(samples) for i in x]
        # 8-bit counts
        counts_dict = r2data.unpack_8bit_counts(r2daem.decode(args.redis[redis_key("r2dbe_monitor%d_counts" % ch)]))
        # sum over 4 ADC cores
        counts = np.array(counts_dict["counts"]).sum(axis=1)
        # check for -1 entries which mean incomplete count
        result["8bit_counts_%d" % ch] = counts_dict["states"], 100.0*counts[-1,:]/sum(counts[-1,:]) if not any(counts[-1,:] == -1) else 100.0*counts[-2,:]/sum(counts[-2,:])
    # text data
    result["text_panel"] = {}
    #   .. time
    sec = get_value("vdif_seconds")
    ep = get_value("ref_epoch_0")
    #   .. store _last_update in result so main_loop can compare to previous update
    result["_last_update"] = float(args.redis[redis_key("_last_update")])
    #   .. now (use _last_update, the timestamp of data read from device)
    last_update = datetime.datetime.fromtimestamp(result["_last_update"]).strftime("%Y-%m-%d %H:%M:%S")
    utcnow = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    timestamp = vdif.VDIFTime(ep, sec).to_datetime().strftime("%Y-%m-%d %H:%M:%S")
    uptime = get_value("uptime")
    upday = uptime // (24*60*60)
    uphour = (uptime - (24*60*60)*upday) // (60*60)
    upmin = (uptime - (24*60*60)*upday - (60*60)*uphour) // 60
    upsec = uptime - (24*60*60)*upday - (60*60)*uphour - 60*upmin
    #   .. PPS
    pps_cnt = get_value("pps_offset")
    result["text_panel"].update({
      "name": args.device,
      "host": args.r2dbe.host,
      "pps_clk": "%d" % pps_cnt,
      "pps_ns": "%.2f" % (pps_cnt / 256e6 * 1e9),
      "now": utcnow,
      "last_update": last_update,
      "time": timestamp,
      "uptime": "%3dd %02dh %02dm %02ds" % (upday, uphour, upmin, upsec),
    })
    #   .. per-chanel
    for ch in [0, 1]:
        result["text_panel"].update({
          "ch%d" % ch: "%d" % ch,
          "pol%d" % ch: get_value("pol_%d" % ch),
          "bdc%d" % ch: get_value("bdc_ch_%d" % ch),
          "rx%d" % ch: get_value("rx_sb_%d" % ch),
          "station%d" % ch: get_value("station_%d" % ch),
          "th%d" % ch: "%d" % get_value("2bit_th_%d" % ch),
        })
    return result

def main_loop(args, period=3.0):
    import matplotlib
    matplotlib.use("TkAgg")
    import matplotlib.pyplot as plt
    plt.ion()

    # initialise figure
    fig = plt.figure(args.device, figsize=(12,6))
    fig.canvas.mpl_connect('close_event', handle_close)
    # outer-margin horizontal
    _omh = 0.05
    # outer-margin vertical
    _omv = 0.05
    # inner-margin horizontal
    _imh = 0.05
    # inner-margin vertical
    _imv = 0.05
    # width, height
    _wdt, _hgt = (1 - 2*_omh - 3*_imh)/4.0, (1 - 2*_omv - 1*_imv)/2.0
    _rect = [_omh, _omh, _wdt, _hgt]
    ax = {
      "8bit_counts_0": plt.subplot2grid((2, 5), (0, 0)),
      "8bit_counts_1": plt.subplot2grid((2, 5), (0, 1)),
      "8bit_spectrum_0": plt.subplot2grid((2, 5), (0, 2)),
      "8bit_spectrum_1": None, # will be set to same as 8bit_spectrum_0
      "2bit_counts_0": plt.subplot2grid((2, 5), (1, 0)),
      "2bit_counts_1": plt.subplot2grid((2, 5), (1, 1)),
      "2bit_spectrum_0": plt.subplot2grid((2, 5), (1, 2)),
      "2bit_spectrum_1": None, # will be set to same as 2bit_spectrum_0
      "text_panel": plt.subplot2grid((2, 5), (0, 3), colspan=2, rowspan=2),
    }
    ax["8bit_spectrum_1"] = ax["8bit_spectrum_0"]
    ax["2bit_spectrum_1"] = ax["2bit_spectrum_0"]
    hplot = {}
    htext = {}
    text_templates = {}
    colormap = [[0.3, 0.3, 1.0], [0.15, 0.7, 0.15]]
    for k, v in ax.items():
        if "counts" in k:
            color = colormap[int(k[-1])]
            v.grid("on")
            v.set_title(k.replace("counts_","counts:if"))
            v.set_xlabel("States")
            v.set_ylabel("Fraction [%]")
            if "8bit" in k:
                v.set_xlim((-130,129))
                v.set_ylim((0, 5))
                v.set_xticks(list(np.arange(-128,128,128)) + [127])
                x_gauss = np.arange(-128, 127, 0.1)
                y_gauss = np.exp(-(x_gauss**2)/32**2/2.0) / np.sqrt(2.0*np.pi*32**2)
                v.plot(x_gauss, 100*y_gauss, color=[0.5,0.5,0.5])
                hplot[k], = v.step(range(-128, 128), [0] * 256, color=color, fillstyle='full')
            elif "2bit" in k:
                v.set_xlim((-2.6,1.6))
                v.set_ylim((0, 50))
                v.set_xticks(range(-2,2))
                hplot[k] = v.bar(range(-2,2), [0,]*4, color=color)
                htext[k] = [v.text(i, 10, "%.1f%%" % 0, horizontalalignment="center", fontsize='small') for i in range(-2,2)]
        elif "spectrum" in k:
            color = colormap[int(k[-1])]
            v.grid("on")
            if "8bit" in k:
                v.set_title("8bit_spec")
            elif "2bit" in k:
                v.set_title("2bit_spec")
            v.set_xlabel("Frequency [MHz]")
            v.set_ylabel("Normalised PSD [dB]")
            v.set_xlim((-32,2047+32))
            v.set_xticks(list(np.arange(0,2048,1024)) + [2047])
            v.set_ylim((-40, 5))
            hplot[k], = v.plot([], [], label="if%s" % k[-1], color=color)
        elif k == "text_panel":
            v.set_xticks([])
            v.set_yticks([])
            lines = [
              "{name}   host: {host}",
              "-"*30,
              "    Current time:  {now}",
              "  Last update at:  {last_update}",
              "      R2DBE time:  {time}",
              "Uptime: {uptime}"
              "",
              "",
              "PPS offset (ext. vs int.):",
              "-"*30,
              "{pps_clk} cycles @ FPGA clock rate",
              "{pps_ns} ns",
              "",
            ]
            for ch in [0, 1]:
                lines += [
                  "IF{ch%d}" % ch,
                  "-"*5,
                  "Pol={pol%d}" % ch,
                  "Rx={rx%d}" % ch,
                  "BDC={bdc%d}" % ch,
                  "Station={station%d}" % ch,
                  "2-bit threshold={th%d}" % ch,
                  "",
                ]
            text_templates[k] = "\n".join(lines)
            v.set_xlim((0, 1))
            v.set_ylim((0, 1))
            htext[k] = v.text(0.05,0.95, text_templates[k], family="monospace", verticalalignment="top")

    r = redis.Redis()
    setattr(args, "redis", r)

    n_iter = 0
    prev_update = -1
    while True:
        n_iter += 1
        t1 = time.time()

        # exit if stopped
        is_stopped = False
        with _stop_lock:
            if _stop:
                logger.debug("main_loop: _stop == True")
                is_stopped = True
        if is_stopped:
            logger.debug("main_loop: break")
            break

        # fetch data
        batch = get_r2dbe_data(args)

        # in case there is no new data ..
        if not args.test_only and not batch["_last_update"] > prev_update:
            #   .. check if r2daemon still running, and restart if necessary
            if not test_restart_r2deamon(args):
                return False
            logger.debug("main_loop: iter %d, no new updates, sleep and continue (previous = %.3f, latest = %.3f", n_iter, prev_update, batch["_last_update"])
            time.sleep(period)
            continue
        prev_update = batch["_last_update"]

        # update plots
        for k in list(hplot.keys()):
            if k not in batch.keys():
                logger.debug("main_loop: missing '%s' in batch, skipping", k)
                continue
            x, y = batch[k]
            if "counts" in k and "2bit" in k:
                for ch, yy in zip(hplot[k].get_children(), y):
                    ch.set_height(yy)
                if "2bit" in k:
                    for ht, yy in zip(htext[k], y):
                        ht.set_text("%.1f%%" % yy)
            elif "spectrum" in k or ("counts" in k and "8bit" in k):
                hplot[k].set_data(x, y)

            # update axis limits based on data
            if "spectrum" in k:
                ax[k].legend(loc='lower left')
                min_y = min(y)
                if min_y < ax[k].get_ylim()[0] - 10 or min_y > ax[k].get_ylim()[0] + 20:
                    if not np.isnan(min_y) and not np.isinf(min_y):
                        ax[k].set_ylim([20*(min_y//20), 5])

        # update text data
        if "text_panel" in batch.keys():
            text_values = batch["text_panel"]
            htext["text_panel"].set_text(text_templates["text_panel"].format(**text_values))

        # update canvas
        fig.tight_layout()
        fig.canvas.draw()
        fig.canvas.flush_events()
        time.sleep(0.1)

        # round off loop
        t2 = time.time()
        logger.debug("main_loop: iter %d in %.3f seconds" % (n_iter, t2-t1))
        wait = period - (t2 - t1)
        if wait > 0:
            logger.debug("main_loop: sleeping for %.3f seconds" % wait)
            time.sleep(wait)

    return True

def _parallel_main(devname_cfg_args):
    try:
        devname, cfg, args = devname_cfg_args
        setattr(args, "device", devname)
        # testing hack
        if args.test_only:
            name_host_map = {"r2dbe.lsb_lo": "r2dbe1", "r2dbe.lsb_hi": "r2dbe2"}
            setattr(args, "r2dbe", collections.namedtuple("R2DBE", ("host",))(host=name_host_map[devname]))
            return main_loop(args)
        # get device instance and do basic testing
        r2dbe = get_r2dbe_inst(devname, cfg)
        setattr(args, "r2dbe", r2dbe)
        if not r2dbe:
            logger.info("%s invalid device specification in config file, aborting", devname)
            return False
        if not r2dbe.test_device_reachable():
            logger.info("%s (hostname %s) device is not reachable, aborting", devname, r2dbe.host)
            return False
        if not r2dbe.test_login_root():
            logger.info("%s (hostname %s) remote login to device failed, aborting", devname, r2dbe.host)
            return False
        # start the main loop
        return main_loop(args)
    except Exception:
        exc_type, exc_value, exc_tb = sys.exc_info()
        out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
        # handle _tkinter.TclError raised on event_close
        if "can't invoke \"update\" command: application has been destroyed" in out:
            return True
        # other cases error
        logger.info(out)
        return False

def main(args):
    cfg = config.Config(args.conffile)
    if not args.devices:
        alldevs = list(cfg.devices.keys())
        all_r2devs = [dev for dev in alldevs if cfg.devices[dev]['layout'] == 'r2dbe']
        setattr(args, "devices", all_r2devs)
    devname_cfg_args = [(d, cfg, args) for d in args.devices]
    p = multiprocessing.Pool(len(devname_cfg_args))
    return all(p.map(_parallel_main, devname_cfg_args))

if __name__ == "__main__":
    import argparse

    arg_parser = argparse.ArgumentParser(description="display live updates of r2dbe",
      epilog="Call with multiple r2dbe arguments will open up a different window for each device. " + \
      "Starts r2daemon on the specified r2dbe devices if not already running.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", default="/etc/backend.conf",
      help="backend config file")
    arg_parser.add_argument("--test-only", action="store_true", default=False, help=argparse.SUPPRESS)
    arg_parser.add_argument("devices", metavar="r2dbe", nargs="*",
      help="name of device as specified in config-file")
    args = arg_parser.parse_args()

    ui.init_ui(log_to_file=False)

    try:
        if main(args):
            exit(ui.RC_OKAY)
        exit(ui.RC_FAIL)
    except Exception:
        exc_type, exc_value, exc_tb = sys.exc_info()
        out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
        sys.stdout.write(out)
        sys.stderr.write(out)
        exit(ui.RC_ERROR)
