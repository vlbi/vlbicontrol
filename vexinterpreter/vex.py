import collections
import glob
import os

from datetime import datetime, timedelta

# Datetime format
DATETIME_FORMAT = "%Yy%jd%Hh%Mm%Ss"

# Section names
SECTION_ANTENNA = '$ANTENNA'
SECTION_BBC = '$BBC'
SECTION_DAS = '$DAS'
SECTION_EXPER = '$EXPER'
SECTION_FREQ = '$FREQ'
SECTION_GLOBAL = '$GLOBAL'
SECTION_HEAD_POS = '$HEAD_POS'
SECTION_IF = '$IF'
SECTION_MODE = '$MODE'
SECTION_PASS_ORDER = '$PASS_ORDER'
SECTION_PHASE_CAL_DETECT = '$PHASE_CAL_DETECT'
SECTION_PROCEDURES = '$PROCEDURES'
SECTION_ROLL = '$ROLL'
SECTION_SCHED = '$SCHED'
SECTION_SITE = '$SITE'
SECTION_SOURCE = '$SOURCE'
SECTION_STATION = '$STATION'
SECTION_TRACKS = '$TRACKS'

# Experiment fields
EXPER_DESC = "exper_description"
EXPER_NAME = "exper_name"
EXPER_START = "exper_nominal_start"
EXPER_STOP = "exper_nominal_stop"

# Schedule entry fields
SCAN_MODE = "mode"
SCAN_SOURCE = "source"
SCAN_START = "start"
SCAN_STATIONS = "station"
SCAN_STATIONS_SEP = ":"

class VexExperiment(object):

    def __init__(self, exper_dict):
        self._description = exper_dict[EXPER_DESC]
        self._name = exper_dict[EXPER_NAME]
        self._start = datetime.strptime(exper_dict[EXPER_START], DATETIME_FORMAT)
        self._stop  = datetime.strptime(exper_dict[EXPER_STOP], DATETIME_FORMAT)

    def __str__(self):
        return self.name

    @property
    def description(self):
        return self._description

    @property
    def name(self):
        return self._name

    @property
    def start(self):
        return self._start

    @property
    def stop(self):
        return self._stop

class VexScan(object):

    def __init__(self, station, mode, source, start, duration):
        self._station = station
        self._mode = mode
        self._source = source
        self._start = start
        self._duration = duration
        self._stop = start + timedelta(seconds=duration)

    def __str__(self):
        return "{b}".format(b=self.start.strftime("%jd-%Hh%Mm%Ss"))

    def __lt__(self, other):
        return self.start < other.start

    def __gt__(self, other):
        return self.start > other.start

    def __eq__(self, other):
        return self.start == other.start

    @property
    def duration(self):
        return self._duration

    @property
    def mode(self):
        return self._mode

    @property
    def source(self):
        return self._source

    @property
    def start(self):
        return self._start

    @property
    def station(self):
        return self._station

    @property
    def stop(self):
        return self._stop

class VexSchedule(object):

    def __init__(self, sched_dict):
        self._sched = sched_dict
        self._scans = []

    def set_station(self, code):
        # Initialize scan list to populate
        scans = []

        # Go through all scans
        for k, scan_dict in list(self._sched.items()):
            mode = scan_dict[SCAN_MODE]
            source = scan_dict[SCAN_SOURCE]
            start = datetime.strptime(scan_dict[SCAN_START], DATETIME_FORMAT)
            # For each scan, get station list
            stations_list = scan_dict[SCAN_STATIONS]
            if not isinstance(stations_list, list): stations_list = [stations_list]
            for entry in stations_list:
                station, offset, duration = self._parse_station_line(entry)
                # Skip entries not for this station
                if not station == code:
                    continue
                # We have an entry for this station, append scan
                scans.append(VexScan(station, mode, source, start, duration))
                # All other station entries will not be for this station
                break

        # With scan list populated, sort
        self._scans = sorted(scans)

    @classmethod
    def _parse_station_line(cls, line):
        elements = [e.strip() for e in line.split(SCAN_STATIONS_SEP)]
        station = elements[0]
        start = int(elements[1].split()[0])
        duration = int(elements[2].split()[0])

        return station, start, duration

    @property
    def scans(self):
        return self._scans

class Vex(object):

    NUM_MD5_SHOW = 7

    def __init__(self, vex_dict):
        # Store original dictionary
        self._vex_dict = vex_dict

        # Add some internal attributes
        self._source = vex_dict["filename"]
        self._checksum = vex_dict["checksum"]
        self._md5sum = vex_dict["md5sum"]

        # Add experiment, assume there is only one
        self._experiment = VexExperiment(list(vex_dict[SECTION_EXPER].items())[0][1])

        # Add schedule
        self._schedule = VexSchedule(vex_dict[SECTION_SCHED])

    def __cmp__(self, other):
        # Compare on start time
        ours = self.start
        theirs = other.start

        if ours < theirs:
            return -1
        if ours == theirs:
            return 0
        return 1

    def __str__(self):
        return str(self.experiment)

    def __repr__(self):
        return self.md5sum[:self.NUM_MD5_SHOW]

    def to_string(self):
        # write VEX_rev first
        sout = "VEX_rev = %s;\n" % self._vex_dict["VEX_rev"]
        # then all $-sections
        for sname, sec in list(self._vex_dict.items()):
            if not sname.startswith("$"):
                continue
            sout += sname + ";\n"
            # paragraph definition special for $SCHED
            par = "scan" if sname == SECTION_SCHED else "def"
            # ... and line-definitions also special for $SCHED:
            #     In other sections, <name>" = "<value>, but in $SCHED it seems
            #     customary to use <name>"="<value>. Note that if the latter is
            #     used in the $EXPER section, vex2xml.py breaks.
            assign = "=" if sname == SECTION_SCHED else " = "
            # each section is a dictionary
            for k, v in list(sec.items()):
                # internal to section can be paragraph or line definition
                #   .. dictionary for paragraph
                if isinstance(v, dict):
                    sout += "%s %s;\n" % (par, k)
                    for kk, vv in list(v.items()):
                        # each entry can be str or list, make list of all
                        if isinstance(vv, str):
                            vv = [vv]
                        # put each assignment on separate line
                        for vvv in vv:
                            sout += "     %s%s%s;\n" % (kk, assign, vvv)
                    sout += "end%s;\n" % par
                #   .. str or list for line
                else:
                    if isinstance(v, str):
                        v = [v]
                    for vv in v:
                        sout += "     %s%s%s;\n" % (k, assign, vv)
        return sout

    def set_experiment_name(self, name):
        # update $EXPER
        for old, exp in list(self._vex_dict[SECTION_EXPER].items()):
            self._vex_dict[SECTION_EXPER].pop(old)
            exp[EXPER_NAME] = name
            self._vex_dict[SECTION_EXPER][name] = exp
        # update reference in $GLOBAL
        try:
            self._vex_dict[SECTION_GLOBAL]["ref %s" % SECTION_EXPER] = name
        except KeyError:
            pass

    def replace_station(self, old, new):
        # Replace one two-leter station code with another

        # for all paragraph definitions in $MODE ...
        for par in list(self._vex_dict[SECTION_MODE].values()):
            if not isinstance(par, dict):
                continue
            # ... multiple line-definitions has ...:<station>:<station>:...
            for k, v in list(par.items()):
                par[k] = v.replace(":%s:" % old, ":%s:" % new)

        # the $STATION section has a 'def <station>;'
        station = self._vex_dict[SECTION_STATION]
        station[new] = station.pop(old)
        # ... and it references a $SITE definition
        siteref = station[new]["ref $SITE"]

        # the $SITE section has a site_ID
        site = self._vex_dict[SECTION_SITE][siteref]
        site["site_ID"] = new

        # the $SCHED has a "station=<station>: ..." line
        sched = self._vex_dict[SECTION_SCHED]
        for name, scan in list(sched.items()):
            for i, st in enumerate(scan["station"]):
                if st.startswith("%s:" % old):
                    scan["station"][i] = st.replace("%s:" % old, "%s:" % new)
                    # only one "station=..." per station
                    break

    def _update_vex_dict_exper_start_stop(self):
        new_start = datetime(2525,1,1)
        new_stop = datetime(1066,1,1)
        for name, scan in list(self._vex_dict[SECTION_SCHED].items()):
            scan_start = datetime.strptime(scan["start"], DATETIME_FORMAT)
            if scan_start < new_start:
                new_start = scan_start
            _, _, duration = VexSchedule._parse_station_line(scan["station"][0])
            scan_stop = scan_start + timedelta(seconds=duration)
            if scan_stop  > new_stop:
                new_stop = scan_stop
        for name, exp in list(self._vex_dict[SECTION_EXPER].items()):
            exp[EXPER_START] = new_start.strftime(DATETIME_FORMAT)
            exp[EXPER_STOP] = new_stop.strftime(DATETIME_FORMAT)

    def add_scan(self, start, duration, name=None, mode=None, source=None, stations=None):
        # create a scan
        scan = collections.OrderedDict()
        scan["start"] = start.strftime(DATETIME_FORMAT)
        if not mode:
            mode = list(self._vex_dict[SECTION_MODE].keys())[0]
        scan["mode"] = mode
        if not source:
            source = list(self._vex_dict[SECTION_SOURCE].keys())[0]
        scan["source"] = source
        if not stations:
            stations = list(self._vex_dict[SECTION_STATION].keys())
        scan["station"] = []
        for st in stations:
            scan["station"].append("%s:    0 sec:  %s sec:    0.000 GB:   :       : 1" % (st, duration))
        if not name:
            n = len(list(self._vex_dict[SECTION_SCHED].keys())) + 1
            while True:
                name = "No%04d" % n
                if name not in list(self._vex_dict[SECTION_SCHED].keys()):
                    break
                # increase number if collision with existing scan
                n += 1
        # add it at the end
        self._vex_dict[SECTION_SCHED][name] = scan
        # update schedule
        self._schedule = VexSchedule(self._vex_dict[SECTION_SCHED])
        # update start/stop in _vex_dict experiment
        self._update_vex_dict_exper_start_stop()
        # update experiment
        self._experiment = VexExperiment(list(self._vex_dict[SECTION_EXPER].items())[0][1])

    def del_scan(self, name, rename_all=False):
        self._vex_dict[SECTION_SCHED].pop(name)
        if not rename_all:
            return
        new_sched = collections.OrderedDict()
        n = 1
        for k, v in list(self._vex_dict[SECTION_SCHED].items()):
            new_sched["No%04d" % n] = v
            n += 1
        self._vex_dict[SECTION_SCHED] = new_sched
        # update schedule
        self._schedule = VexSchedule(self._vex_dict[SECTION_SCHED])
        # update start/stop in _vex_dict experiment
        self._update_vex_dict_exper_start_stop()
        # update experiment
        self._experiment = VexExperiment(list(self._vex_dict[SECTION_EXPER].items())[0][1])

    def clr_sched(self):
        for name in list(self._vex_dict[SECTION_SCHED].keys()):
            self._vex_dict[SECTION_SCHED].pop(name)
        # update schedule
        self._schedule = VexSchedule(self._vex_dict[SECTION_SCHED])
        # update start/stop in _vex_dict experiment
        self._update_vex_dict_exper_start_stop()
        # update experiment
        self._experiment = VexExperiment(list(self._vex_dict[SECTION_EXPER].items())[0][1])

    @property
    def basename(self):
        return self._source.split(os.sep)[-1].split(os.extsep)[0]

    @property
    def filename(self):
        return self._source

    @property
    def checksum(self):
        return self._checksum

    @property
    def experiment(self):
        return self._experiment

    @property
    def md5sum(self):
        return self._md5sum

    @property
    def schedule(self):
        return self._schedule

    # handle to various definitions
    @property
    def antennas(self):
        return list(self._vex_dict["$ANTENNA"].keys())

    @property
    def modes(self):
        return list(self._vex_dict["$MODE"].keys())

    @property
    def sites(self):
        return list(self._vex_dict["$SITE"].keys())

    @property
    def sources(self):
        return list(self._vex_dict["$SOURCE"].keys())

    @property
    def stations(self):
        return list(self._vex_dict["$STATION"].keys())

    # forward .name, .description, .start and .stop from VexExperiment
    @property
    def description(self):
        return self.experiment.description

    @property
    def name(self):
        return self.experiment.name

    @property
    def start(self):
        return self.experiment.start

    @property
    def stop(self):
        return self.experiment.stop
