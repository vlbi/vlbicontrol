#!/bin/bash
if [[ $# -eq 0 || ${1:0:1} == '-' || $2 =~ [a-zA-Z] ]]; then
	echo "Clone a vex but let it start NOW [+ offset]."
	echo "usage: $0 input.vex [offset_seconds]"
	exit 1
fi

fname=$1  #-- input filename
toff=${2-0}  #-- start time offset from NOW

[ -s $fname ] || { echo "Input file not found: $fname"; exit 1; }
basename=$(basename $fname)
tmpfile=/tmp/${basename%.vex}_timeshift.vex
rm -rf $tmpfile || { echo "temporary file exists: $tmpfile"; exit 1; }

#-- start time is toff seconds from now
tstart=$(date +%s -d "NOW + $toff seconds")
echo "exper_nominal_start: $(date -d@$tstart --rfc-3339=seconds)"

originStart=999999999999999
originStop=0

while IFS=$'\n' read line; do
	if [[ ! "$line" =~ ([0-9]{4})y([0-9]{3})d([0-9]{2})h([0-9]{2})m([0-9]{2})s ]]; then
		echo "${line}"
		continue
	fi

	#-- vexdate 2 time
	match=${BASH_REMATCH[0]}
	words=()
	for ((i=1; i<${#BASH_REMATCH[@]}; i++)) do
		words+=( $((10#${BASH_REMATCH[$i]})) )
	done
	printf -v vexdate 'Jan 1 %d - 1 day + %d days + %d hours + %d minutes + %d seconds' "${words[@]}"
	#>&2 echo "vexdate: $vexdate"
	time=$(date -u -d "$vexdate" +%s)
	if [ "$originStart" -gt "$time" ]; then
		originStart=$time
	fi
	if [ "$time" -gt "$originStop" ]; then
		originStop=$time
	fi

	#-- original start time
	if [[ "$line" =~ exper_nominal_start ]]; then
		tdelta=$((tstart - time))
	fi

	#-- shifted time
	vexdatenew=$(date -u -d@$((time + tdelta)) +%Yy%jd%Hh%Mm%Ss)

	echo "${line/$match/$vexdatenew}"
done < $fname > $tmpfile || { echo "parse error"; exit 1; }

#-- output filename
fnameout=${fname%.vex}_timeshifted_${tdelta}.vex
mv $tmpfile $fnameout || { echo "file creation error: $fnameout"; exit 1; }

echo "OriginStart: $originStart"
echo "OriginStop:  $originStop"
echo "Start:       $tstart"
echo "saved: $fnameout"
