import re
import socket
import struct
import time

from .message import KLMessage

import logging
logger = logging.getLogger(__name__)

class KLClientError(RuntimeError):
    pass

class KLClient(object):

    _informs = []

    def __init__(self, host, port=7147):
        assert isinstance(host, str), "host should be str, either hostname or IP address in dot-decimal notation"
        assert isinstance(port, int) and not isinstance(port, bool) and (port >= 0 and port < 2**16), "port number should be an unsigned 16-bit integer"

        self.host = host
        self.port = port
        self._connected = False

    @property
    def connected(self):
        return self._connected

    @property
    def informs(self):
        return self._informs

    def _connect(self, timeout=5):
        assert isinstance(timeout, (float, int)) and not isinstance(timeout, bool) and timeout >= 0.0, "timeout should be a non-negative number"

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(timeout)
        self.sock.connect((self.host, self.port))
        self._connected = True

    def _send_recv(self, msgout):
        assert isinstance(msgout, KLMessage), "msgout should be a KLMessage instance"

        if not self.connected:
            self._connect()
        try:
            self.sock.sendall(bytes(msgout))
        except (BrokenPipeError, ConnectionResetError) as e:
            logger.error("Error '%s' occurred; Reconnecting...", str(e))
            self._connect()
            self.sock.sendall(bytes(msgout))
        while True:
            # Continue reading until last character is a newline
            raw = b""
            while True:
                raw += self.sock.recv(1048576)
                if raw[-1] == ord(b"\n"):
                    logger.debug("Received %d bytes" % len(raw))
                    break
            # Parse lines
            lines = raw.splitlines()
            logger.debug("Received %d lines" % len(lines))
            self._informs = []
            for line in lines:
                logger.debug("Create message from line %r" % line)
                msgin = KLMessage.parse(line)
                # Log info messages
                if msgin.mtype == KLMessage.INFORM:
                    logger.info("%r" % msgin)
                    self._informs.append(msgin)
                    continue
                # Check if reply to request
                if msgin.mtype == KLMessage.REPLY:
                    if msgin.name != msgout.name:
                        logger.error("Requested '%s' but got reply '%s'" % (msgout.name, msgin.name))
                        continue

                    return msgin

    def blindwrite(self, name, data, offset=0):
        assert isinstance(name, str), "name should be str"
        assert isinstance(data, bytes), "data should bytes"
        assert isinstance(offset, int) and not isinstance(offset, bool)  and (offset >= 0 and offset < 2**32), "offset should be an unsigned 32-bit integer"

        msgout = KLMessage.request("write", name, offset, data)
        msgin = self._send_recv(msgout)
        if not msgin.reply_okay():
            logger.error("blindwrite to register '%s' at offset %d with data %r failed" % (name, offset, data))
            raise KLClientError("blindwrite to register '%s' at offset %d with data %r failed" % (name, offset, data))

    def est_brd_clk(self):
        f, _ = self.est_brd_clk_with_int(wait=2)

        return f/1e6

    def est_brd_clk_with_int(self, wait=5):
        """Estimate board clock frequency.

        Returns an estimate of the clock frequency in hertz, as well as a tuple
        with estimates for the lower and upper bounds of the frequency."""
        t1a = time.time()
        c1 = self.read_uint("sys_clkcounter")
        t1b = time.time()
        t1 = (t1a+t1b)/2
        # divide wait into 10s increments
        wait_step = min(10,wait)
        n_wrap = 0
        c_pre = c1
        while wait >= 0:
            time.sleep(min(wait,wait_step))
            wait -= wait_step
            t2a = time.time()
            c_post = self.read_uint("sys_clkcounter")
            t2b = time.time()
            # clock counter wraps after 2**32 cycles, catch this by checking if
            # post-wait counter value is lower than pre-wait counter value
            if c_post < c_pre:
                n_wrap += 1
            c_pre = c_post
        c2 = c_post + n_wrap*(2**32)
        t2 = (t2a+t2b)/2
        fmax = (c2-c1)/(t2a-t1b)
        fmin = (c2-c1)/(t2b-t1a)
        fest = (c2-c1)/(t2-t1)

        return fest, (fmin,fmax)

    def fpgastatus(self):
        msgout = KLMessage.request("fpgastatus")
        msgin = self._send_recv(msgout)
        pattern = re.compile("fpga programmed, mapped with (.+\.bof) and meta ready")
        if msgin.reply_okay():
            for inf in self.informs:
                if len(inf.args) == 4:
                    match = pattern.search(inf.args[3])
                    if match:

                        return match.group(1)

    def _list(self, listname):
        assert listname in ["bof", "dev"], "listname should be one of ['bof', 'dev']"

        cmd = "list" + listname
        msgout = KLMessage.request(cmd)
        msgin = self._send_recv(msgout)
        if not msgin.reply_okay():
            logger.error(cmd + "failed")
            raise KLClientError(cmd + "failed")
        result = []
        for m in self.informs:
            if m.name == cmd:
                result.append(m.args[0])

        return result

    def listbof(self):
        return self._list("bof")

    def listdev(self):
        return self._list("dev")

    def progdev(self, name):
        assert isinstance(name, str), "name should be str"

        msgout = KLMessage.request("progdev", name)
        msgin = self._send_recv(msgout)
        if not msgin.reply_okay():
            logger.error("progdev with binary file '%s' failed" % (name))
            raise KLClientError("progdev with binary file '%s' failed" % (name))

    def read(self, name, size, offset=0):
        assert isinstance(name, str), "name should be str"
        assert isinstance(size, int) and not isinstance(size, bool) and (size >= 0 and size < 2**32), "size should be an unsigned 32-bit integer"
        assert isinstance(offset, int) and not isinstance(offset, bool) and (offset >=0 and offset < 2**32), "offset should be non-negative integer"

        msgout = KLMessage.request("read", name, offset, size)
        msgin = self._send_recv(msgout)
        if not msgin.reply_okay():
            logger.error("read from register '%s' at offset %d of %d bytes failed" % (name, offset, size))
            raise KLClientError("read from register '%s' at offset %d of %d bytes failed" % (name, offset, size))

        return bytes(ord(c) for c in msgin.args[1])

    def read_int(self, name):
        assert isinstance(name, str), "name should be str"

        data = self.read(name, 4)

        return struct.unpack('>i', data)[0]

    def read_uint(self, name):
        assert isinstance(name, str), "name should be str"

        data = self.read(name, 4)

        return struct.unpack('>I', data)[0]

    def _snapshot_arm(self, name, man_trig=False):
        assert isinstance(name, str), "name should be str"
        assert isinstance(man_trig, bool), "man_trig should be bool"

        self.write_int(name + "_ctrl", (0 + (man_trig<<1)))
        self.write_int(name + "_ctrl", (1 + (man_trig<<1)))

    def snapshot_arm_and_wait(self, name, man_trig=False, wait_period=0.01, timeout=3):
        assert isinstance(name, str), "name should be str"
        assert isinstance(man_trig, bool), "man_trig should be bool"
        assert isinstance(wait_period, (float, int)) and not isinstance(wait_period, bool) and wait_period >= 0.0, "wait_period should be a non-negative number"
        assert isinstance(timeout, (float, int)) and not isinstance(timeout, bool) and timeout >= 0.0, "timeout should be a non-negative number"

        self._snapshot_arm(name, man_trig=man_trig)
        tstart = time.time()
        time.sleep(wait_period)
        while True:
            status = self.read_uint(name + "_status")
            if status >> 31 == 0:
                break
            time.sleep(wait_period)
            if time.time() - tstart > timeout:

                return False

        return True

    def snapshot_get(self, name, man_trig=False, wait_period=0.01, timeout=3):
        assert isinstance(name, str), "name should be str"
        assert isinstance(man_trig, bool), "man_trig should be bool"
        assert isinstance(wait_period, (float, int)) and not isinstance(wait_period, bool) and wait_period >= 0.0, "wait_period should be a non-negative number"
        assert isinstance(timeout, (float, int)) and not isinstance(timeout, bool) and timeout >= 0.0, "timeout should be a non-negative number"

        self._snapshot_arm(name, man_trig=man_trig)
        start_time = time.time()
        time.sleep(wait_period)
        while True:
            status = self.read_uint(name + "_status")
            if status >> 31 == 0:
                break
            if time.time() - start_time > timeout:
                raise KLClientError("snapshot timeout '%s'" % name)
            time.sleep(wait_period)
        size = status & 0x7fffffff
        result = {
          "length": size,
          "data": self.read(name + "_bram", size),
          "offset": 0,
        }

        return result

    def snapshots_arm_and_wait(self, names, man_trig=False, wait_period=0.001, timeout=5.0):
        assert isinstance(names, list) and all([isinstance(name, str) for name in names]), "names should be list of str"
        assert isinstance(man_trig, bool), "man_trig should be bool"
        assert isinstance(wait_period, (float, int)) and not isinstance(wait_period, bool) and wait_period >= 0.0, "wait_period should be a non-negative number"
        assert isinstance(timeout, (float, int)) and not isinstance(timeout, bool) and timeout >= 0.0, "timeout should be a non-negative number"

        # make auxiliary list of names, code below removes entries
        aux_names = list(names)
        for name in aux_names:
            self._snapshot_arm(name, man_trig=man_trig)
        tstart = time.time()
        time.sleep(wait_period)
        result = dict([(name, "") for name in aux_names])
        while len(aux_names) > 0:
            for name in aux_names:
                status = self.read_uint(name + "_status")
                if status >> 31 == 1:
                    continue
                aux_names.remove(name)
            time.sleep(0.001)
            if time.time() - tstart > timeout:
                return False

        return True

    def snapshots_get(self, names, man_trig=False, wait_period=0.01, timeout=3):
        assert isinstance(names, list) and all([isinstance(name, str) for name in names]), "names should be list of str"
        assert isinstance(man_trig, bool), "man_trig should be bool"
        assert isinstance(wait_period, (float, int)) and not isinstance(wait_period, bool) and wait_period >= 0.0, "wait_period should be a non-negative number"
        assert isinstance(timeout, (float, int)) and not isinstance(timeout, bool) and timeout >= 0.0, "timeout should be a non-negative number"

        # make auxiliary list of names, code below removes entries
        aux_names = list(names)
        for name in aux_names:
            self._snapshot_arm(name, man_trig=man_trig)
        start_time = time.time()
        time.sleep(wait_period)
        result = dict([(name, "") for name in aux_names])
        while len(aux_names) > 0:
            for name in aux_names:
                status = self.read_uint(name + "_status")
                if status >> 31 == 1:
                    continue
                aux_names.remove(name)
                size = status & 0x7fffffff
                result[name] = {
                  "length": size,
                  "data": self.read(name + "_bram", size),
                  "offset": 0,
                }
            if time.time() - start_time > timeout:
                raise KLClientError("snapshots timed out ['%s']" % "', '".join(aux_names))
            time.sleep(wait_period)

        return result

    def wait_connected(self, timeout=5):
        assert isinstance(timeout, (float, int)) and not isinstance(timeout, bool) and timeout >= 0.0, "timeout should be a non-negative number"

        try:
            self._connect(timeout=timeout)
            return True

        except socket.timeout as timeout:
            logger.error("Connecting to %s:%d timed out" % (self.host, self.port))
        except Exception as exc:
            logger.error("Connecting to %s:%d raised '%s'" % (self.host, self.port, exc))

        return False

    def write(self, name, data, offset=0):
        assert isinstance(name, str), "name should be str"
        assert isinstance(data, bytes), "data should be bytes"
        assert isinstance(offset, int) and not isinstance(offset, bool) and (offset >= 0 and offset < 2**32), "offset should be an unsigned 32-bit integer"

        self.blindwrite(name, data, offset=offset)
        read_data = self.read(name, len(data), offset=offset)
        if read_data != data:
            logger.error("Data read from register '%s' does not match data written (offset %d)" % (name, offset))
            raise KLClientError("Data read from register '%s' does not match data written (offset %d)" % (name, offset))

    def write_int(self, name, value):
        assert isinstance(name, str), "name should be str"
        assert isinstance(value, int) and not isinstance(value, bool) and (value >= -2**31 and value < 2**31), "value should be a 32-bit integer"

        data = struct.pack('>i', value)
        self.write(name, data)

    def write_uint(self, name, value):
        assert isinstance(name, str), "name should be str"
        assert isinstance(value, int) and not isinstance(value, bool) and (value >= 0 and value < 2**32), "value should be a 32-bit integer"

        data = struct.pack('>I', value)
        self.write(name, data)

    def _interp_rcs_word(self, word):
        assert isinstance(word, int) and not isinstance(word, bool) and (word >= 0 and word < 2**32), "word should be an unsigned 32-bit integer"

        rv = dict()
        if (word>>31) & 0x01:
            rv["rcs_type"] = "timestamp"
            rv["last_modified"] = word & 0x7fffffff
            rv["dirty"] = False
        else:
            if (word>>30) & 0x01:
                rv["rcs_type"] = "svn"
            else:
                rv["rcs_type"] = "git"
            rv["dirty"] = (word>>28) & 0x01 == 0x01
            rv["rev"] = word & 0x0fffffff

        return rv

    def get_rcs(self, name="rcs"):
        assert isinstance(name, str), "name should be str"

        rv = dict()
        app = self._interp_rcs_word(self.read_uint(name + "_app"))
        for k,v in list(app.items()):
            rv["app_" + k] = v
        lib = self._interp_rcs_word(self.read_uint(name + "_lib"))
        for k,v in list(lib.items()):
            rv["lib_" + k] = v
        rv["user"] = self.read_uint(name + "_user")

        return rv

    def get_10gbe_core_details(self, name):
        assert isinstance(name, str), "name should be str"

        ctrl_data = self.read(name, 36)
        subnet_data = self.read(name, 4, offset=0x38)
        arp_data = self.read(name, 2048, offset=0x3000)
        mac, _, gateway, ip, _, _, _, sw_rst, fab_en, port = struct.unpack(">QIIIIIIBBH", ctrl_data)
        arp = struct.unpack(">256Q", arp_data)
        result = {
          "arp": arp,
          "fabric_en": fab_en & 0x01,
          "fabric_port": port,
          "my_ip": ip,
          "mymac": mac,
        }

        return result

    def config_10gbe_core(self, name, mac, ip, port, arp_table, gateway=1, subnet_mask=0xffffff00):
        assert isinstance(name, str), "name should be str"
        assert isinstance(mac, int) and not isinstance(mac, bool) and (mac >= 0 and mac < 2**48), "mac should be an unsigned 48-bit integer"
        assert isinstance(ip, int) and not isinstance(ip, bool) and (ip >= 0 and ip < 2**32), "ip should be an unsigned 32-bit integer"
        assert isinstance(port, int) and not isinstance(port, bool) and (port >= 0 and port < 2**16), "port should be an unsigned 16-bit integer"
        assert isinstance(arp_table, list) \
          and len(arp_table) == 256 \
          and all([isinstance(a, int) and not isinstance(a, bool) for a in arp_table]) \
          and all([(a >= 0 and a < 2**48) for a in arp_table]), "arp_table should be list of 48-bit unsigned integers"
        assert isinstance(gateway, int) and not isinstance(gateway, bool) and (gateway >= 0 and gateway < 2**32), "gateway should be an unsigned 32-bit integer"
        assert isinstance(subnet_mask, int) and not isinstance(subnet_mask, bool) and (subnet_mask >= 0 and subnet_mask < 2**32), "subnet_mask should be an unsigned 32-bit integer"

        ctrl_data = struct.pack(">QIIIIIIBBH", mac, 0, gateway, ip, 0, 0, 0, 0, 1, port)
        subnet_data = struct.pack(">I", subnet_mask)
        arp_data = struct.pack(">256Q", *arp_table)
        self.blindwrite(name, ctrl_data, offset=0)
        self.blindwrite(name, subnet_data, offset=0x38)
        self.write(name, arp_data, offset=0x3000)
