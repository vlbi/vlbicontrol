import unittest

from katcplight import KLClient, KLClientError

import struct
import time

class KLClientAssertTests(unittest.TestCase):

    NOT_A_STR = [None, True, 1, 1.0]
    NOT_A_BOOL = [None, "True", 1, 1.0]
    NOT_A_INT = [None, True, "1", 1.0]
    NOT_A_NUMBER = [None, True, "1"]

    NEGATIVE = [-1,]

    RANGE_NOT_B = [-1, 2**8]
    RANGE_NOT_b = [-2**7-1, 2**7]

    RANGE_NOT_H = [-1, 2**16]
    RANGE_NOT_h = [-2**15-1, 2**15]

    RANGE_NOT_I = [-1, 2**32]
    RANGE_NOT_i = [-2**31-1, 2**31]

    RANGE_NOT_48B = [-1, 2**48]

    def test___init__host(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^host"):
                KLClient(x)

    def test___init__port(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_H:
            with self.assertRaisesRegex(AssertionError, "^port"):
                KLClient("server", port=x)

class KLClientInstanceAssertTests(KLClientAssertTests):

    @classmethod
    def setUpClass(self):
        self.klc_inst = KLClient("server")

    def test__connect_timeout(self):
        for x in self.NOT_A_NUMBER + self.NEGATIVE:
            with self.assertRaisesRegex(AssertionError, "^timeout"):
                self.klc_inst._connect(timeout=x)

    def test__send_recv_msgout(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^msgout"):
                self.klc_inst._send_recv(x)

    def test_blindwrite_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.blindwrite(x, "data")

    def test_blindwrite_data(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^data"):
                self.klc_inst.blindwrite("name", x)

    def test_blindwrite_offset(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_I:
            with self.assertRaisesRegex(AssertionError, "^offset"):
                self.klc_inst.blindwrite("name", "data", offset=x)

    def test_progdev_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.progdev(x)

    def test_read_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.read(x, 1)

    def test_read_size(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_I:
            with self.assertRaisesRegex(AssertionError, "^size"):
                self.klc_inst.read("name", x)

    def test_read_offset(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_I:
            with self.assertRaisesRegex(AssertionError, "^offset"):
                self.klc_inst.read("name", 1, offset=x)

    def test_read_int_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.read_int(x)

    def test_read_uint_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.read_uint(x)

    def test__snapshot_arm_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst._snapshot_arm(x)

    def test__snapshot_arm_man_trig(self):
        for x in self.NOT_A_BOOL:
            with self.assertRaisesRegex(AssertionError, "^man_trig"):
                self.klc_inst._snapshot_arm("name", man_trig=x)

    def test_snapshot_get_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.snapshot_get(x)

    def test_snapshots_get_names(self):
        not_a_list = [None, True, 1, 1.0, "1", tuple([c for c in "abc"])]
        not_str_entries = [[s,]*3 for s in self.NOT_A_STR]
        for x in not_a_list + not_str_entries:
            with self.assertRaisesRegex(AssertionError, "^names"):
                self.klc_inst.snapshots_get(x)

    def test_snapshot_get_man_trig(self):
        for x in self.NOT_A_BOOL:
            with self.assertRaisesRegex(AssertionError, "^man_trig"):
                self.klc_inst.snapshot_get("name", man_trig=x)

    def test_snapshot_get_wait_period(self):
        for x in self.NOT_A_NUMBER + self.NEGATIVE:
            with self.assertRaisesRegex(AssertionError, "^wait_period"):
                self.klc_inst.snapshot_get("name", wait_period=x)

    def test_wait_connected_timeout(self):
        for x in self.NOT_A_NUMBER + self.NEGATIVE:
            with self.assertRaisesRegex(AssertionError, "^timeout"):
                self.klc_inst.wait_connected(timeout=x)

    def test_write_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.write(x, "data")

    def test_write_data_not_str(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^data"):
                self.klc_inst.write("name", x)

    def test_write_offset(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_I:
            with self.assertRaisesRegex(AssertionError, "^offset"):
                self.klc_inst.write("name", "data", offset=x)

    def test_write_int_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.write_int(x, 1)

    def test_write_int_value(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_i:
            with self.assertRaisesRegex(AssertionError, "^value"):
                self.klc_inst.write_int("name", x)

    def test__interp_rcs_word_word(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_I:
            with self.assertRaisesRegex(AssertionError, "^word"):
                self.klc_inst._interp_rcs_word(x)

    def test_get_rcs_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.get_rcs(x)

    def test_get_10gbe_core_details_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.get_10gbe_core_details(x)

    def test_config_10gbe_core_name(self):
        for x in self.NOT_A_STR:
            with self.assertRaisesRegex(AssertionError, "^name"):
                self.klc_inst.config_10gbe_core(x, 1, 1, 1, [1,]*256)

    def test_config_10gbe_core_mac(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_48B:
            with self.assertRaisesRegex(AssertionError, "^mac"):
                self.klc_inst.config_10gbe_core("name", x, 1, 1, [1,]*256)

    def test_config_10gbe_core_ip(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_I:
            with self.assertRaisesRegex(AssertionError, "^ip"):
                self.klc_inst.config_10gbe_core("name", 1, x, 1, [1,]*256)

    def test_config_10gbe_core_port(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_H:
            with self.assertRaisesRegex(AssertionError, "^port"):
                self.klc_inst.config_10gbe_core("name", 1, 1, x, [1,]*256)

    def test_config_10gbe_core_arp_table(self):
        not_list = [None, True, 1, 1.0, "1", tuple([1,]*256)]
        not_256_long = [(1,)*255, (1,)*257]
        not_int_entries = [[1,]*127 + [x,]*1 + [1,]*128 for x in self.NOT_A_INT]
        not_in_range_entries = [[1,]*127 + [x,]*1 + [1,]*128 for x in self.RANGE_NOT_48B]
        for x in not_list + not_256_long + not_int_entries + not_in_range_entries:
            with self.assertRaisesRegex(AssertionError, "^arp_table"):
                self.klc_inst.config_10gbe_core("name", 1, 1, 1, x)

    def test_config_10gbe_core_gateway(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_I:
            with self.assertRaisesRegex(AssertionError, "^gateway"):
                self.klc_inst.config_10gbe_core("name", 1, 1, 1, [1,]*256, gateway=x)

    def test_config_10gbe_core_subnet_mask(self):
        for x in self.NOT_A_INT + self.RANGE_NOT_I:
            with self.assertRaisesRegex(AssertionError, "^subnet_mask"):
                self.klc_inst.config_10gbe_core("name", 1, 1, 1, [1,]*256, subnet_mask=x)

class KLClientCorrComparisonTests(unittest.TestCase):
    HOST = "r2dbe1"
    BOFFILE = "r2dbe_rev2_v1.1.bof"
    REG = "r2dbe_vdif_0_hdr_w4"
    CH = 0

    @classmethod
    def setUpClass(cls):
        try:
            import corr
            cls.ref_client = corr.katcp_wrapper.FpgaClient(cls.HOST)
        except ImportError:
            raise unittest.SkipTest("Module 'corr' not found, skipping %s" % cls.__name__)

        try:
            import mandc.r2dbe as r2dbe
            import mandc.r2dbe.defines as r2dbe_defines
            cls.r2dbe = r2dbe.R2dbe(cls.HOST)
            cls.r2dbe_input_source = r2dbe_defines.R2DBE_INPUT_DATA_SOURCE_ADC
        except ImportError:
            raise unittest.SkipTest("Module 'mandc...' not found, skipping %s" % cls.__name__)

        cls.klc_inst = KLClient(cls.HOST)
        if not cls.ref_client.wait_connected(timeout=3) or not cls.klc_inst.wait_connected(timeout=3):
            raise unittest.SkipTest("Failed to connect to %s, skipping %s" % (cls.HOST, cls.__name__))

        # program reference binary file and do initial setup
        cls.ref_client.progdev(cls.BOFFILE)
        cls.r2dbe.adc_interface_cal(cls.CH)
        cls.r2dbe.arm_one_pps()
        cls.r2dbe.set_input_data_source(cls.CH,cls.r2dbe_input_source)

        cls.longMessage = True

    def test_blindwrite(self):
        # by nature blindwrite cannot be verified through normal writeto and readback from device
        raise unittest.SkipTest("test not implemented, blindwrite by definition does not care")

    def test_progdev(self):
        # would have to force this test to be done last, or in a different suite
        raise unittest.SkipTest("test not implemented yet")

    def test_read_single_register(self):
        for hi in [0x0000, 0xfa42, 0xffff]:
            for lo in [0x0000, 0x1248, 0xffff]:
                v = (hi<<16) + lo
                b = struct.pack(">I", v)
                self.ref_client.write(self.REG, b)
                r_ref = self.ref_client.read(self.REG, 4)
                r_uut = self.klc_inst.read(self.REG, 4)
                self.assertEqual(r_ref, r_uut)

    def test_read_decode(self):
        # make sure to test reading all byte values
        for v in range(260):
            b = struct.pack(">I", v)
            self.ref_client.write(self.REG, b)
            r_ref = self.ref_client.read(self.REG, 4)
            r_uut = self.klc_inst.read(self.REG, 4)
            self.assertEqual(r_ref, r_uut)

    def test_read_large_buffer(self):
        # ensure stable snapshot buffer, and make repeated reads with reference and UuT
        reg_ctl = "r2dbe_snap_8bit_%d_data_ctrl" % self.CH
        reg_dat = "r2dbe_snap_8bit_%d_data_bram" % self.CH
        self.ref_client.write_int(reg_ctl, 0x0)
        time.sleep(0.05)
        for nbyte, noff in [(4, 1004), (64, 124), (256, 92), (8224, 1024), (262144, 0)]:
            r_ref = self.ref_client.read(reg_dat, nbyte, offset=noff)
            r_uut = self.klc_inst.read(reg_dat, nbyte, offset=noff)
            self.assertEqual(len(r_ref), len(r_uut))
            self.assertEqual(r_ref, r_uut)

    def test_read_int(self):
        for v in [-2**31, -2**31+123456, 0, 654321, 2**31-1]:
                self.ref_client.write_int(self.REG, v)
                r_ref = self.ref_client.read_int(self.REG)
                r_uut = self.klc_inst.read_int(self.REG)
                self.assertEqual(r_ref, r_uut)

    def test_read_uint(self):
        for v in [0, 654321, 2**31-1, 2**32-1]:
                self.ref_client.write(self.REG, struct.pack(">I", v))
                r_ref = self.ref_client.read_uint(self.REG)
                r_uut = self.klc_inst.read_uint(self.REG)
                self.assertEqual(r_ref, r_uut)

    def test_snapshot_get(self):
        snap = "r2dbe_snap_8bit_%d_data" % self.CH
        s_ref = self.ref_client.snapshot_get(snap, man_trig=True)
        s_uut = self.klc_inst.snapshot_get(snap, man_trig=True)
        # return should be dictionary
        self.assertIsInstance(s_uut, type(s_ref))
        for k in list(s_ref.keys()):
            # return should have all the standard keys
            self.assertIn(k, list(s_uut.keys()), msg="uut missing key %s" % k)
            # values should match exactly except for data
            if k == "data":
                continue
            self.assertEqual(s_ref[k], s_uut[k])
        # test approximate equal data values at the end
        v_ref = struct.unpack(">%db" % s_ref["length"], s_ref["data"])
        m_ref = 1.0*sum(v_ref)/len(v_ref)
        v_uut = struct.unpack(">%db" % s_uut["length"], s_uut["data"])
        m_uut = 1.0*sum(v_uut)/len(v_uut)
        self.assertAlmostEqual(m_ref, m_uut)

    def test_snapshots_get(self):
        raise unittest.SkipTest("test not implemented yet")

    def test_wait_connected(self):
        raise unittest.SkipTest("test not implemented yet")

    def test_write_single_register(self):
        for hi in [0x0000, 0xfa42, 0xffff]:
            for lo in [0x0000, 0x1248, 0xffff]:
                v = (hi<<16) + lo
                b = struct.pack(">I", v)
                # first use reference client to write different value
                rst = struct.pack(">I", 1) if v == 0 else struct.pack(">I",0)
                self.ref_client.write(self.REG, rst)
                self.klc_inst.write(self.REG, b)
                r_ref = self.ref_client.read(self.REG, 4)
                self.assertEqual(r_ref, b)

    def test_write_encode(self):
        # make sure to test writing all byte values
        for v in range(260):
            b = struct.pack(">I", v)
            # first use reference client to write different value
            rst = struct.pack(">I", 1) if v == 0 else struct.pack(">I",0)
            self.ref_client.write(self.REG, rst)
            self.klc_inst.write(self.REG, b)
            r_ref = self.ref_client.read(self.REG, 4)
            self.assertEqual(r_ref, b)

    def test_write_int(self):
        for v in [-2**31, -2**31+123456, 0, 654321, 2**31-1]:
            # first use reference client to write different value
            rst = 1 if v == 0 else 0
            self.ref_client.write_int(self.REG, rst)
            self.klc_inst.write_int(self.REG, v)
            r_ref = self.ref_client.read_int(self.REG)
            self.assertEqual(r_ref, v)

    def test_get_rcs(self):
        rcs_ref = self.ref_client.get_rcs()
        rcs_uut = self.klc_inst.get_rcs()
        # return should be dictionary
        self.assertIsInstance(rcs_uut, type(rcs_ref))
        # available keys depend on specific cases
        # for the following keys, the UuT should mimic the reference
        # note that the reference has a bug (lib_dirty is clobbered by app_dirty==False)
        for pre in ["app_", "lib_"]:
            for suf in ["dirty", "rcs_type", "rev"]:
                k = pre + suf
                if k in list(rcs_ref.keys()):
                    # if the key is in the reference, it should be in the UuT
                    self.assertIn(k, list(rcs_uut.keys()))
                    # check matching values, except for dirty (due to reference bug)
                    if suf == "dirty":
                        continue
                    self.assertEqual(rcs_uut[k], rcs_ref[k])
        # also check user key
        if "user" in list(rcs_ref.keys()):
            self.assertIn("user", list(rcs_uut.keys()))
            self.assertEqual(rcs_uut["user"], rcs_ref["user"])

    def test_get_10gbe_core_details(self):
        core = "r2dbe_tengbe_%d_core" % self.CH
        mac = 0xabcdef123456
        ip = 0x98700789
        port = 12345
        arp_table = [0xaabbccddee00 + n for n in range(256)]
        gateway = 0xeeeeeeee
        # reset and config using reference
        self.ref_client.config_10gbe_core(core, 0, 0, 0, [0,]*256)
        self.ref_client.config_10gbe_core(core, mac, ip, port, arp_table, gateway=gateway)
        # read using reference
        c_ref = self.ref_client.get_10gbe_core_details(core)
        # read using UuT
        c_uut = self.klc_inst.get_10gbe_core_details(core)
        # all keys in the UuT return should match the reference
        # note that UuT return does not implement all keys in reference
        for k in list(c_uut.keys()):
            if k in list(c_ref.keys()):
                # check individual ARP entries
                if k == "arp":
                    for entry_uut, entry_ref in zip(c_uut[k], c_ref[k]):
                        self.assertEqual(entry_uut, entry_ref)
                    continue
                # check other parameters directly
                self.assertEqual(c_uut[k], c_ref[k])

    def test_config_10gbe_core(self):
        core = "r2dbe_tengbe_%d_core" % self.CH
        mac = 0xabcdef123456
        ip = 0x98700789
        port = 12345
        arp_table = [0xaabbccddee00 + n for n in range(256)]
        gateway = 0xeeeeeeee
        # reset using reference
        self.ref_client.config_10gbe_core(core, 0, 0, 0, [0,]*256)
        # config values using UuT
        self.klc_inst.config_10gbe_core(core, mac, ip, port, arp_table, gateway=gateway)
        # get config dictionary using reference
        c_uut = self.ref_client.get_10gbe_core_details(core)
        # reset and config using reference
        self.ref_client.config_10gbe_core(core, 0, 0, 0, [0,]*256)
        self.ref_client.config_10gbe_core(core, mac, ip, port, arp_table, gateway=gateway)
        c_ref = self.ref_client.get_10gbe_core_details(core)
        for k in list(c_ref.keys()):
            self.assertEqual(c_uut[k], c_ref[k])

if __name__ == "__main__":

    suite_assert = unittest.TestLoader().loadTestsFromTestCase(KLClientAssertTests)
    suite_instance_assert = unittest.TestLoader().loadTestsFromTestCase(KLClientInstanceAssertTests)
    suite_corr_comparison = unittest.TestLoader().loadTestsFromTestCase(KLClientCorrComparisonTests)

    all_tests = unittest.TestSuite([
      suite_assert,
      suite_instance_assert,
      suite_corr_comparison,
    ])

    unittest.TextTestRunner(verbosity=2).run(all_tests)
