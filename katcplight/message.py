import logging
logger = logging.getLogger(__name__)

class KLMessageError(ValueError):
    pass

class KLMessage(object):

    ESCAPE_CHAR = "\\"

    # Mapping from unescaped to escaped
    ENCODE_MAP = [
      ("\\", "\\\\"),
      (" ", "\\_"),
      ("\0", "\\0"),
      ("\n", "\\n"),
      ("\r", "\\r"),
      ("\x1b", "\\e"),
      ("\t", "\\t"),
    ]

    # Mapping from escaped to unescaped
    DECODE_MAP = [
      ("\\\\", "\\1"), # dummy replacement to avoid artificial escaped (\\1 is not a valid escaped)
      ("\\_", " "),
      ("\\0", "\0"),
      ("\\n", "\n"),
      ("\\r", "\r"),
      ("\\e", "\x1b"),
      ("\\t", "\t"),
      ("\\@", ""),
      ("\\1", "\\"), # replace the dummy
    ]

    # Message types
    REQUEST = "REQUEST"
    REPLY = "REPLY"
    INFORM = "INFORM"
    MESSAGE_TYPE_SYMBOLS = {
      REQUEST: "?",
      REPLY: "!",
      INFORM: "#"
    }
    MESSAGE_TYPE_NAMES = dict([(v,k) for k,v in list(MESSAGE_TYPE_SYMBOLS.items())])

    # Reply status
    OKAY = "ok"
    FAIL = "fail"
    INVALID = "invalid"
    REPLY_STATUS_VALUES = [OKAY, FAIL, INVALID]

    def __init__(self, mtype, name, *args):
        self.mtype = mtype
        self.name = name
        self.args = []
        for a in args:
            # bytes are converted to string using chr()
            if isinstance(a, bytes):
                self.args.append("".join(chr(b) for b in a))
                continue
            # everything else converts to string using str()
            # bool should first be converted to int
            if isinstance(a, bool):
                a = int(a)
            self.args.append(str(a))

    def __str__(self):

        return self.MESSAGE_TYPE_SYMBOLS[self.mtype] + self.name + " " + " ".join([self.escape(str(a)) for a in self.args]) + "\n"

    def __bytes__(self):

        return bytes(ord(c) for c in str(self))

    def __repr__(self):

        return self.mtype + " : " + self.name + "(" + ",".join(["%r" % a for a in self.args]) + ")"

    def reply_okay(self):

        return self.mtype == self.REPLY and len(self.args ) > 0 and self.args[0] == self.OKAY

    @classmethod
    def request(cls, name, *args):

        return cls(cls.REQUEST, name, *args)

    @classmethod
    def reply(cls, name, *args):
        if args[0] not in cls.REPLY_STATUS_VALUES:
            logging.error("Unkown reply status '%s'" % args[0])
            raise KLMessageError("Unkown reply status '%s'" % args[0])

        return cls(cls.REPLY, name, *args)

    @classmethod
    def inform(cls, name, *args):

        return cls(cls.INFORM, name, *args)

    @classmethod
    def parse(cls, line):
        # convert to string-line
        if isinstance(line, bytes):
            line = "".join(chr(b) for b in line)
        try:
            mtype = cls.MESSAGE_TYPE_NAMES[line[0]]
        except KeyError:
            logging.error("Unknown message type symbol '%s'" % msymb)
            raise KLMessageError("Unknown message type symbol '%s'" % msymb)
        elements = line.split(" ")
        name = elements[0][1:]
        args = [cls.descape(e) for e in elements[1:]]
        msg = cls(mtype, name, *args)
        logger.debug("Parsed message %r" % msg)

        return msg

    @classmethod
    def escape(cls, s):
        escaped = s
        for k, v in cls.ENCODE_MAP:
            escaped = escaped.replace(k, v)
        #~ escaped = cls._RE_ESCAPE.sub(cls._escape_match, s)
        #~ logger.debug("Escaped %r to %r" % (s, escaped))

        return escaped

    @classmethod
    def descape(cls, s):
        descaped = s
        for k, v in cls.DECODE_MAP:
            descaped = descaped.replace(k, v)
        #~ descaped = cls._RE_DESCAPE.sub(cls._descape_match, s)
        #~ logger.debug("Descaped %r to %r" % (s, descaped))

        return descaped
