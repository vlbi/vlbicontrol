import collections
import re

COMMENT = "#"
SECTION_ID = "!"
ASSIGN = "="
EXPAND = "$"
VALID_VARNAME = "[a-zA-Z_][\\w]*"

_variables = {}

_varget_pattern_fmt = "\\$(%s)"
_varget_pattern = re.compile(_varget_pattern_fmt % VALID_VARNAME)
def _expand_var(s):
    s_exp = s
    while True:
        match = _varget_pattern.search(s_exp)
        if not match:
            break
        key = match.group(1)
        try: value = _variables[key]
        except KeyError: raise ActionParserError(0, "reference to undefined variable '%s'" % key)
        s_exp = re.sub(_varget_pattern_fmt % key, value, s_exp)
    return s_exp

_varset_pattern = re.compile("^(%s)[\\s]*%s(.*)$" % (VALID_VARNAME, ASSIGN))
def _assign_var(s):
    match = _varset_pattern.match(s)
    if not match:
        return
    _variables.update([match.groups()])

class ActionParserError(ValueError):

    def __init__(self, lineno, *args, **kwargs):
        self._lineno = lineno
        super(ActionParserError, self).__init__(*args, **kwargs)

    def __str__(self):
        return "line %d: " % (self._lineno + 1) + super(ActionParserError, self).__str__()

    def offset(self, lines):
        self._lineno += lines

    @property
    def lineno(self):
        return self._lineno

class TimeSpec(object):

    REF_SCAN_START = "scanStart"
    REF_SCAN_END = "scanEnd"
    _ALL_REF = [REF_SCAN_START, REF_SCAN_END]

    def __init__(self, s):
        offset, ref = self._parse_timespec(s)
        self._offset = offset
        self._ref = ref

    def __str__(self):
        return "%s %+ds" % (self.ref, self.offset)

    def __lt__(self, other):
        if self.ref == self.REF_SCAN_START and other.ref == other.REF_SCAN_END: return True
        if self.ref == other.ref and self.offset < other.offset: return True
        return False

    def __gt__(self, other):
        if self.ref == self.REF_SCAN_END and other.ref == other.REF_SCAN_START: return True
        if self.ref == other.ref and self.offset > other.offset: return True
        return False

    def __eq__(self, other):
        if self.ref == other.ref and self.offset == other.offset: return True
        return False

    def __hash__(self):
        return hash((self._offset, self._ref))

    @property
    def offset(self):
        return self._offset

    @property
    def ref(self):
        return self._ref

    _time_multipliers = {
      "h": 3600,
      "m": 60,
      "s": 1
    }
    _time_pattern = re.compile("^[\\s]*([\\d]+)(%s)[\\s]*$" % "|".join(list(_time_multipliers.keys())))
    def _parse_time(self, s):
        m = self._time_pattern.match(s)
        if not m:
            raise ActionParserError(0, "invalid TIME '%s'" % s)
        count, mult = m.group(1,2)
        return int(count) * self._time_multipliers[mult]

    _timespec_sign = {
      "before": -1,
      "after": 1,
    }
    _timespec_end = ":"
    _timespec_pattern = re.compile("[\\s]*([\S]+)[\\s]+(%s)[\\s]+(%s)[\\s]*%s[\\s]*$" % (
      "|".join(list(_timespec_sign.keys())),
      "|".join(_ALL_REF),
      _timespec_end,
    ))
    def _parse_timespec(self, s):
        m = self._timespec_pattern.match(s)
        if not m:
            raise ActionParserError(0, "invalid TIMESPEC '%s'" % s)
        t, s, ref = m.group(1, 2, 3)
        offset = self._parse_time(t) * self._timespec_sign[s]
        return (offset, ref)

class CmdSpec(object):

    def __init__(self, s):
        cmd = self._parse_cmdspec(s)
        self._cmd = cmd

    def __str__(self):
        return self._cmd

    @property
    def cmd(self):
        return self._cmd

    _cmdspec_end = ";"
    _cmdspec_pattern = re.compile("^([^%s]+)%s[\\s]*$" % (_cmdspec_end, _cmdspec_end))
    #_cmdspec_pattern = "^((?:[\\S]+|[\\s]+)+)%s[\\s]*$" % _cmdspec_end
    #_cmdspec_elem_pattern = re.compile("[^\\s;]+")
    def _parse_cmdspec(self, s):
        m = self._cmdspec_pattern.match(s)
        if not m:
            raise ActionParserError(0, "invalid CMDSPEC '%s'" % s)
        return m.group(1)

class ActSpec(object):

    ID = "A" + SECTION_ID

    def __init__(self, s):
        lines = s.splitlines()
        timespec = None
        cmdspec_list = []
        try:
            for n, line in enumerate(lines):
                # skip empty and comment lines
                if not line.strip() or line.strip().startswith(COMMENT):
                    continue

                # process variables
                #   expansions first ...
                line = _expand_var(line)
                #   ... then assignments
                _assign_var(line)

                # first content line is TIMESPEC
                if not timespec:
                    if not line.startswith(self.ID):
                        raise ActionParserError(n, "ACTSPEC should start with '%s'" % self.ID)
                    timespec = TimeSpec(re.sub("^%s[\\s]*" % self.ID, "", line))
                    continue
                # remainder should be CMDSPEC
                cmdspec_list.append(CmdSpec(line))
        except ActionParserError as ape:
            ape.offset(n)
            raise ape
        if not cmdspec_list:
            raise ActionParserError(0, "no CMDSPEC found in ACTSPEC")
        self._timespec = timespec
        self._commands = cmdspec_list

    def __str__(self):
        return "%s:\n  %s" % (str(self._timespec), "\n  ".join(str(cs) for cs in self._commands))

    # compare on basis of .timespec
    def __lt__(self, other):
        if isinstance(other, ActSpec): return self.timespec.__lt__(other.timespec)
        elif isinstance(other, TimeSpec): return self.timespec.__lt__(other)
        else: raise TypeError("incompatible other %s for %s.__lt__" % (str(type(other)), self.__class__.__name__))

    def __gt__(self, other):
        if isinstance(other, ActSpec): return self.timespec.__gt__(other.timespec)
        elif isinstance(other, TimeSpec): return self.timespec.__gt__(other)
        else: raise TypeError("incompatible other %s for %s.__gt__" % (str(type(other)), self.__class__.__name__))

    def __eq__(self, other):
        if isinstance(other, ActSpec): return self.timespec.__eq__(other.timespec)
        elif isinstance(other, TimeSpec): return self.timespec.__eq__(other)
        else: raise TypeError("incompatible other %s for %s.__eq__" % (str(type(other)), self.__class__.__name__))

    @property
    def timespec(self):
        return self._timespec

    @property
    def commands(self):
        return self._commands

_actspec_pattern = re.compile("^%s.+?(?=\n.%s|$)+" % (ActSpec.ID, SECTION_ID), re.S)
def parse(fname):
    with open(fname, "r") as fh:
        s = fh.read()
    # initialise variables
    _variables = {}
    lines = s.splitlines()
    actspec_list = []
    skip_lines = 0
    for n, line in enumerate(lines):
        try:
            # skip lines processed as separate section
            if skip_lines:
                skip_lines -= 1
                continue

            # skip empty and comment lines
            if not line.strip() or line.strip().startswith(COMMENT):
                    continue

            # process variables
            #   expansions first ...
            lines[n] = _expand_var(lines[n])
            #   ... then assignments
            _assign_var(lines[n])

            # try to match ACTSPEC
            match = _actspec_pattern.match("\n".join(lines[n:]))
            if match:
                part = match.group()
                skip_lines = len(part.splitlines()) - 1
                # parse ACTSPEC
                actspec = ActSpec(match.group())
                # check if duplicate TIMESPEC
                if actspec.timespec in [a.timespec for a in actspec_list]:
                    raise ActionParserError(0, "multiple ACTSPEC with same TIMESPEC '%s'" % actspec.timespec)
                # add to list of ACTSPEC
                actspec_list.append(actspec)

            # try match OTHER?

        except ActionParserError as ape:
            ape.offset(n)
            raise ape

    # file should include at least one ACTSPEC
    if not actspec_list:
        raise ActionParserError(len(lines), "no ACTSPEC found in '%s'" % fname)

    # sort ACTSPECs
    actspec_list = sorted(actspec_list)

    return actspec_list
