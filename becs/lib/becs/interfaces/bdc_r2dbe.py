import datetime
import re
import struct
import time

from .. import config

from .generic import Interface, InterfaceError

import logging
logger = logging.getLogger(__name__)

class InterfaceBDCR2DBEError(InterfaceError):
    pass

class InterfaceBDCR2DBE(Interface):

    def testgroup_pretest(self, test):
        result = []
        if test == self.test_routing:
            if not self.rx_dev.test_tcpborphserver_running():
                return False
            if not self.rx_dev.test_firmware_running_version():
                return False
        return all(result)

    def test_routing(self, delta_attn=1.0, wait=0.02, tol=0.1):
        """Test if cabling between BDC and R2DBE units match configuration by
        adjusting BDC attenuation by +/-1.0dB and checking if change is
        reflected in R2DBE 8-bit data. BDC attenuation is restored to original
        value.
        In case of failure:
          - check cabling corresponds to config-file specification"""
        # get attenuator and channel
        attn = config.get_bdc_attn_from_outport(self.tx_port)
        ch = config.get_r2dbe_inport_channel(self.rx_port)
        # read attenuation and power baseline values
        a0_dB = list(self.tx_dev.get_attn(attn).values())[0]
        p0 = self.rx_dev.get_8bit_power(ch)["power"][-1]
        logger.debug("%s:%s power = %d at %s:%s attenuation = %.1f", self.rx_dev, self.rx_port, p0, self.tx_dev, self.tx_port, a0_dB)
        # adjust attenuation, pause, and read updated power
        if a0_dB + delta_attn > 31.5:
            delta_attn = -delta_attn
        a1_dB = a0_dB + delta_attn
        self.tx_dev.set_attn(**{attn: a0_dB+delta_attn})
        time.sleep(wait)
        p1 = self.rx_dev.get_8bit_power(ch)["power"][-1]
        logger.debug("%s:%s power = %d at %s:%s attenuation = %.1f", self.rx_dev, self.rx_port, p1, self.tx_dev, self.tx_port, a1_dB)
        # reset attenuation
        self.tx_dev.set_attn(**{attn: a0_dB})
        try:
            r_p1_p0 = 1.0 * p1 / p0
        except ZeroDivisionError as zde:
            logger.debug("calculating power-ratio raised %s: %s", zde.__class__.__name__, str(zde))
            return False
        r_a1_a0 = 10.0**(-(a1_dB - a0_dB)/10.0)
        logger.debug("power ratio is %.3f, expect %.3f +/- %.1f%%", r_p1_p0, r_a1_a0, tol*100)
        if abs(r_p1_p0 - r_a1_a0) / (r_a1_a0) > tol:
            logger.info("%s routing test failed, power level change on %s:%s does not reflect attenuation change on %s:%s", self, self.rx_dev, self.rx_port, self.tx_dev, self.tx_port)
            return False
        logger.info("%s routing test passed", self)
        return True
