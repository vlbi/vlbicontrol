import datetime
import re
import struct

import vdif

from .. import mark6
from .. import common

from .generic import Interface, InterfaceError

import logging
logger = logging.getLogger(__name__)

class InterfaceR2DBEMark6Error(InterfaceError):
    pass

class InterfaceR2DBEMark6(Interface):

    def testgroup_pretest(self, test):
        result = []
        if test == self.test_routing:
            result = [
              self.rx_dev.test_login_oper(),
              self.rx_dev.test_login_root(),
              self.rx_dev.test_cplane_running(),
            ]
        elif test == self.test_timesync:
            result = [
              self.rx_dev.test_login_root(),
              self.rx_dev.test_cplane_running(),
            ]
        return all(result)

    def test_routing(self, timeout=3):
        """Test if cabling between R2DBE and Mark6 units match configuration by
        inspecting VDIF packet IP and Ethernet headers.
        In case of failure:
          - check if cabling corresponds to config-file specification
          - Mark6 and/or R2DBE reconfigure may be needed to reflect changes to config-file"""
        assert isinstance(timeout, int) and timeout > 0, "timeout should be a positive int"
        # get expected packet size
        eth_size = 0
        try:
            input_streams = self.rx_dev.cp_input_stream_query()
        except mark6.VSIError as vsie:
            logger.error(vsie)
            raise InterfaceR2DBEMark6Error("%s cplane command raised error", self)
        for inp in input_streams:
            if inp["interface_ID"] == self.rx_port:
                eth_size = inp["payload_size"] + inp["payload_offset"]
                break
        if not eth_size:
            logger.info("%s routing test failed, network interface has not matching input_stream on %s", self, self.rx_dev.host)
            return False
        logger.debug("packet size %d", eth_size)
        # capture a packet using tcpdump
        cmd = "timeout %d tcpdump -enn -i %s -c 1 length=%d" % (timeout, self.rx_port, eth_size)
        logger.debug("tcpdump command is: %s", cmd)
        rc, out, err = common.subp_remote_system_call(cmd, self.rx_dev._root, self.rx_dev.host)
        # this code returned if timeout times out
        if rc == 124:
            logger.info("%s routing test failed, tcpdump timed out, apparently no traffic of expected packet size being received on %s", self, self.rx_port)
            return False
        # any other non-zero code is an error
        if rc:
            raise InterfaceR2DBEMark6Error("%s call '%s' failed [returned %d: %s]", self, cmd, rc, err)
        # inspect tcpdump output to get routing information
        p_mac = ":".join(["[0-9a-fA-F]{2}",]*6)
        p_ip_port = "\\.".join(["[0-9]+",]*5)
        pattern_mac = "(%s)[\\s]*>[\\s]*(%s)" % (p_mac, p_mac)
        pattern_ip_port = "(%s)[\\s]*>[\\s]*(%s)" % (p_ip_port, p_ip_port)
        pattern = "%s.+[\\s]+%s" % (pattern_mac, pattern_ip_port)
        re_comp = re.compile(pattern)
        match = re_comp.search(out)
        if not match:
            logger.info("%s routing test failed, could not determine addresses from '%s'", self, out)
            return False
        logger.debug("MAC: %s > %s ; IP: %s > %s ;" % match.groups())
        mac_send, mac_recv, ip_port_send, ip_port_recv = [s.lower() for s in match.groups()]
        # shave of fifth decimal (port) in ip string matches
        ip_send = ".".join(ip_port_send.split(".")[:-1])
        ip_recv = ".".join(ip_port_recv.split(".")[:-1])
        # first check all but last byte in send/recv mac match
        #mac5_send = ":".join(mac_send.split(":")[:-1])
        #mac5_recv = ":".join(mac_recv.split(":")[:-1])
        #logger.debug("MACs should have matching bytes [0:4], expect %s == %s", mac5_send, mac5_recv)
        #if not mac5_send == mac5_recv:
        #   logger.info("%s routing test failed, source and destination MAC addresses in packet do not match, R2DBE may need reconfiguring", self)
        #   return False
        # then ckeck all but last byte in send/recv ip match
        ip3_send = ".".join(ip_send.split(".")[:-1])
        ip3_recv = ".".join(ip_recv.split(".")[:-1])
        logger.debug("source and destination IP addresses should have matching bytes [0:2], expect %s == %s", ip3_send, ip3_recv)
        if not ip3_send == ip3_recv:
            logger.info("%s routing test failed, source and destination IP addresses in packet do not match, R2DBE May need reconfiguring", self)
            return False
        # now check mac match destination and mark6 interface
        #mac_mark6 = common.get_iface_mac_addr(self.rx_port, self.rx_dev._oper, self.rx_dev.host)
        #logger.debug("destination MAC should match interface, mac_recv = %s, expect %s", mac_recv, mac_mark6)
        #if not mac_mark6 == mac_recv:
        #   mark6_b1, mark6_b0 = mac_mark6.split(":")[-2:]
        #   recv_b1, recv_b0 = mac_recv.split(":")[-2:]
        #   if not mark6_b0 == recv_b0:
        #       logger.debug("data appears to be routed to the wrong Mark6 (least significant bytes of destination and interface MACs are unequal, %s != %s)", mark6_b0, recv_b0)
        #   if not mark6_b1 == recv_b1:
        #       logger.debug("data appears to be routed to the wrong network port on Mark6 (second least significant bytes of destination and interface MACs are unequal, %s != %s)", mark6_b1, recv_b1)
        #   logger.info("%s routing test failed, packet destination MAC does not match interface MAC", self)
        #   return False
        # now check ip match destination and mark6 interface
        ip_mark6 = common.get_iface_ip_addr(self.rx_port, self.rx_dev._oper, self.rx_dev.host)
        logger.debug("destination IP should match interface, ip_recv = %s, expect %s", ip_recv, ip_mark6)
        if not ip_mark6 == ip_recv:
            mark6_b1, mark6_b0 = ip_mark6.split(".")[-2:]
            recv_b1, recv_b0 = ip_recv.split(".")[-2:]
            if not mark6_b0 == recv_b0:
                logger.debug("data appears to be routed to the wrong Mark6 (least significant bytes of destination and interface IPs are unequal, %s != %s)", mark6_b0, recv_b0)
            if not mark6_b1 == recv_b1:
                logger.debug("data appears to be routed to the wrong network port on Mark6 (second least significant bytes of destination and interface IPs are unequal, %s != %s)", mark6_b1, recv_b1)
            logger.info("%s routing test failed, packet addressed to does not match interface IP", self)
            return False
        # success
        logger.info("%s routing test passed", self)
        return True

    def test_timesync(self, timeout=3, max_delta_t=0.2):
        """Test time synchronisation between R2DBE and Mark6 by capturing VDIF
        packet on Mark6 using pcap and comparing the VDIF timestamp to pcap
        timestamp.
        In case of failure:
          - run post-config checks on Mark6 and R2DBE"""
        assert isinstance(timeout, int) and timeout > 0, "timeout should be a positive int"
        assert isinstance(max_delta_t, float) and max_delta_t > 0, "max_delta_t should be positive float"
        # get expected packet size
        eth_size = 0
        payload_offset = 0
        try:
            input_streams = self.rx_dev.cp_input_stream_query()
        except mark6.VSIError as vsie:
            logger.error(vsie)
            raise InterfaceR2DBEMark6Error("%s cplane command raised error")
        for inp in input_streams:
            if inp["interface_ID"] == self.rx_port:
                payload_offset = inp["payload_offset"]
                eth_size = payload_offset + inp["payload_size"]
                break
        if not eth_size:
            logger.info("%s time sync test failed, interface has not matching input_stream on", self)
            return False
        logger.debug("packet size %d", eth_size)
        # capture a packet using tcpdump to compare timestamp with VDIF time
        snaplen = payload_offset + vdif.VDIFFrameHeader.NUM_BYTES
        logger.debug("snaplen %d", snaplen)
        cmd = "timeout %d tcpdump -tt -xx -s %d -i %s -c 1 length=%d" % (timeout, snaplen, self.rx_port, eth_size)
        rc, out, err = common.subp_remote_system_call(cmd, self.rx_dev._root, self.rx_dev.host)
        # this code returned if timeout times out
        if rc == 124:
            logger.info("%s time sync test failed, tcpdump timed out, apparently no traffic of given length", self)
            return False
        # any other non-zero code is an error
        if rc:
            raise InterfaceR2DBEMark6Error("%s call '%s' failed [returned %d: %s]", self, cmd, rc, err)
        lines = out.splitlines()
        # first line is timestamp and addressing
        pattern = "^[0-9]+\\.[0-9]+"
        re_comp = re.compile(pattern)
        match = re_comp.search(lines[0])
        if not match:
            logger.info("%s time sync test failed, could not determine timestamp", self)
            return False
        pcap_time = datetime.datetime.fromtimestamp(float(match.group()))
        logger.debug("pcap_time = %s", str(pcap_time))
        # data starts on second line, build binary and construct VDIF header
        hs = []
        for line in lines[1:]:
            idx, doublets = line.split(":")
            for i in doublets.split():
                hs += [i[:2], i[2:]]
        if not len(hs) == snaplen:
            logger.info("%s time sync test failed, could not extract all dumped bytes", self)
            return False
        hb = struct.pack(">%dB" % snaplen, *[int(h,16) for h in hs])
        vdif_hdr = vdif.VDIFFrameHeader.from_bin(hb[payload_offset:])
        # read VDIF time and compare to timestamp
        vdif_time = vdif.VDIFTime.from_vdif(vdif_hdr).to_datetime()
        logger.debug("vdif_time = %s", str(vdif_time))
        delta_t = (vdif_time - pcap_time).total_seconds()
        logger.debug("abs(delta_t) = %.6f, expect < %.6f", delta_t, max_delta_t)
        if abs(delta_t) >= max_delta_t:
            logger.info("%s time sync test failed, time difference too large", self)
            return False
        # success
        logger.info("%s time sync test passed", self)
        return True
