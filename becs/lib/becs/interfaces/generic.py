import logging
logger = logging.getLogger(__name__)

class InterfaceError(object):
    pass

class Interface(object):

    def __init__(self, send, recv):
        self._send_dev, self._send_port = send
        self._recv_dev, self._recv_port = recv

    def __str__(self):
        return "%s:%s>>%s:%s" % (self.tx_dev, self.tx_port, self.rx_dev, self.rx_port)

    @property
    def rx_dev(self):
        """Return receiver device"""
        return self._recv_dev

    @property
    def rx_port(self):
        """Return receiver port"""
        return self._recv_port

    @property
    def tx_dev(self):
        """Return sender device"""
        return self._send_dev

    @property
    def tx_port(self):
        """Return sender port"""
        return self._send_port
