from .. import common

def gen_net_param(cfg, send_devport, recv_devport):
    # byte1 is determined by input port on the receiving device
    recv_dev, recv_port = recv_devport.split(cfg.delim_port)
    recv_layout = cfg.devices[recv_dev]["layout"]
    all_recv_layout_ports = sorted([k for k in list(cfg.deviceLayouts[recv_layout].keys()) if not k.startswith("_")])
    b1 = all_recv_layout_ports.index(recv_port) + 2
    # byte0 is determined by device type, device name, and send/recv:
    #   .. b7: 1 = sender, 0 = receiver
    #   .. b6..b3: device type
    #   .. b2..b0: device name
    all_layouts = sorted(cfg.deviceLayouts.keys())
    all_devices_of_layout = sorted(k for k,v in list(cfg.devices.items()) if v["layout"] == recv_layout)
    b0_recv = all_layouts.index(recv_layout)*8 + all_devices_of_layout.index(recv_dev)
    send_dev, send_port = send_devport.split(cfg.delim_port)
    send_layout = cfg.devices[send_dev]["layout"]
    all_devices_of_layout = sorted(k for k,v in list(cfg.devices.items()) if v["layout"] == send_layout)
    b0_send = all_layouts.index(send_layout)*8 + all_devices_of_layout.index(send_dev) + 128
    ip_base = (172 << 24) + (16 << 16) + (b1 << 8)
    mac_base = (0x02 << 40) + (0x45 << 32) + (0x48 << 24) + (0x54 << 16) + (b1 << 8)
    return dict({
      "send": {
        "mac": common.int2mac(mac_base + b0_send),
        "ip": common.int2ip(ip_base + b0_send),
        "port": 4000,
      },
      "recv": {
        "mac": common.int2mac(mac_base + b0_recv),
        "ip": common.int2ip(ip_base + b0_recv),
        "port": 4001,
      }
    })

def get_chain_port_names(cfg, chain):
    return [k for j in chain for k in j.split(cfg.delim_link)]

def get_chain_ports(cfg, chain):
    return [cfg.ports[k] for k in get_chain_port_names(cfg, chain)]

def get_chain_device_names(cfg, chain):
    n = len(chain)
    devnames = []
    for i in range(n):
        left, right = [elem.split(cfg.delim_port)[0] for elem in chain[i].split(cfg.delim_link)]
        if i == 0:
            devnames.append(left)
        devnames.append(right)
    return devnames

def get_all_device_names(cfg):
    devnames = []
    for chain in list(cfg.chains.values()):
        chain_devnames = get_chain_device_names(cfg, chain)
        [devnames.append(d) for d in chain_devnames if d not in devnames]
    return devnames
