from . import generic

import logging
logger = logging.getLogger(__name__)

def _get_dat_fmt_param(name, cfg):
    cfg_to_param = {
      "data_format": "data_format",
      "payload_size": "payload_size",
      "payload_offset": "payload_offset",
      "psn_offset": "psn_offset",
      "packet_rate": "packet_rate",
    }
    dev = cfg.devices[name]
    st_par = dict()
    for receiver in dev["inports"]:
        iface = receiver.split(cfg.delim_port)[-1]
        sender = cfg.ports[receiver]["fedby"]
        if not sender:
            continue
        sender_dev = cfg.devices[sender.split(cfg.delim_port)[0]]
        logger.debug("%s has layout '%s'", sender_dev, sender_dev["layout"])
        if sender_dev["layout"] in ["r2dbe", "dbbc3"]:
            ch_name = sender.split(cfg.delim_port)[-1]
            if ch_name not in list(sender_dev.keys()):
                logger.debug("%s does not supply data format parameters for %s, skipping", sender_dev, ch_name)
                continue
            ch_dict = sender_dev[ch_name]
            st_par[iface] = dict()
            for k_cfg, k_par in list(cfg_to_param.items()):
                if not k_cfg in list(ch_dict.keys()):
                    continue
                st_par[iface][k_par] = ch_dict[k_cfg]
        else:
            # unsupported device
            logger.debug("unable to generate data format parameters on interface %s >>> %s, skipping", sender, receiver)
            continue
    return st_par

def _get_net_param(name, cfg):
    dev = cfg.devices[name]
    st_par = dict()
    for receiver in dev["inports"]:
        iface = receiver.split(cfg.delim_port)[-1]
        sender = cfg.ports[receiver]["fedby"]
        if not sender:
            continue
        sender_dev = cfg.devices[sender.split(cfg.delim_port)[0]]
        logger.debug("%s has layout '%s'", sender_dev, sender_dev["layout"])
        if sender_dev["layout"] in ["r2dbe", "dbbc3"]:
            net_param = generic.gen_net_param(cfg, sender, receiver)
            st_par[iface] = {
              "mac": net_param["recv"]["mac"],
              "ip": net_param["recv"]["ip"],
              "port": net_param["recv"]["port"],
            }
        else:
            # unsupported device
            logger.debug("unable to generate network parameters on interface %s >>> %s, skipping", sender, receiver)
            continue
    return st_par

def _make_stream_label(label):
    # first escape invalid characters
    escape = {
      ":": ".",
    }
    escaped_label = str(label)
    for old, new in list(escape.items()):
        escaped_label = escaped_label.replace(old, new)
    # then limit string length to 16
    escaped_label = escaped_label[-16:]
    return escaped_label

def get_mark6_inst(name, cfg, cls):
    try:
        dev = cfg.devices[name]
    except KeyError as ke:
        logger.error("%s not found in config", name)
        return False
    # first find parameters specified at the device itself
    try:
        host = dev["hostname"]
        c_param = {
          "name": name,
          "host": host,
          "station": cfg.instruct["stationcode"],
        }
    except KeyError as ke:
        logger.error("%s device instance missing required parameter %s", name, ke)
        return False
    # define streams based on module subgroups (also set stream label)
    st_param = dict()
    for outp_partial, modlist in list(dev["modules"].items()):
        outp = name + cfg.delim_port + outp_partial
        inp = cfg.ports[outp]["fedby"]
        iface = inp.split(cfg.delim_port)[-1]
        sub_grp = "".join([mod.replace("mod", "") for mod in modlist])
        st_param[iface] = {
          "stream_label": _make_stream_label(inp),
          "sub_group": sub_grp,
        }
    # find network parameters
    st_param_update = _get_net_param(name, cfg)
    for iface, par in list(st_param.items()):
        try:
            par.update(st_param_update[iface])
        except KeyError as ke:
            logger.error("%s network parameters not found for interface %s", name, iface)
            return False
    # find data format parameters from upstream devices
    st_param_update = _get_dat_fmt_param(name, cfg)
    for iface, par in list(st_param.items()):
        try:
            par.update(st_param_update[iface])
        except KeyError as ke:
            logger.error("%s data format parameters not found for interface %s", name, iface)
            return False
    for iface, par in list(st_param.items()):
        logger.debug("%s parameters for interface %s are:\n    %s", name, iface, "\n    ".join([k + " = " + str(v) for k, v in list(par.items())]))
    c_param["streams"] = st_param
    return cls(**c_param)
