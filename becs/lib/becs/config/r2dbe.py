from . import generic

import logging
logger = logging.getLogger(__name__)

in_ports = ["if0", "if1"]
out_ports = ["ch0", "ch1"]

def get_r2dbe_inport_channel(inport):
    return in_ports.index(inport)

def get_r2dbe_channel_inport(channel):
    return in_ports[channel]

def get_r2dbe_outport_channel(outport):
    return out_ports.index(outport)

def get_r2dbe_channel_outport(channel):
    return out_ports[channel]

def _get_band_pol_sideband(name, cfg):
    cfg_to_param = {
      "band": "bdc_ch",
      "pol": "pol",
      "sideband": "rx_sb"
    }
    dev = cfg.devices[name]
    host = dev["hostname"]
    ch_par = dict()
    for inp in dev["inports"]:
        ch = get_r2dbe_inport_channel(inp.split(cfg.delim_port)[-1])
        par = dict()
        par.update({"station": cfg.instruct["stationcode"]})
        this_port = cfg.ports[inp]
        fedby_port = this_port["fedby"]
        while fedby_port:
            this_port = cfg.ports[fedby_port]
            for k, v in list(cfg_to_param.items()):
                if k in list(this_port.keys()):
                    par.update({v: this_port[k]})
            fedby_port = this_port["fedby"]
        ch_par.update({ch: par})
    return ch_par

def _get_net_param(name, cfg):
    dev = cfg.devices[name]
    ch_par = dict()
    for sender in dev["outports"]:
        ch = get_r2dbe_outport_channel(sender.split(cfg.delim_port)[-1])
        receiver = cfg.ports[sender]["feeds"]
        net_param = generic.gen_net_param(cfg, sender, receiver)
        ch_par[ch] = {
          "mac": net_param["send"]["mac"],
          "ip": net_param["send"]["ip"],
          "port": net_param["send"]["port"],
          "dest_mac": net_param["recv"]["mac"],
          "dest_ip": net_param["recv"]["ip"],
          "dest_port": net_param["recv"]["port"],
        }
    return ch_par

def get_r2dbe_inst(name, cfg, cls):
    try:
        dev = cfg.devices[name]
    except KeyError as ke:
        logger.error("%s not found in config", name)
        return False
    # first find parameters specified at the device itself
    try:
        host = dev["hostname"]
        c_param = {
          "name": name,
          "host": host,
          "boffile": dev["boffile"],
          "version": dev["version"],
        }
    except KeyError as ke:
        logger.error("%s device instance missing required parameter %s", name, ke)
        return False
    ch_par = dict()
    dev_layout = dev["layout"]
    ch_map = dict(list(zip(out_ports, list(range(len(out_ports))))))
    for k, v in list(dev.items()):
        if k not in list(ch_map.keys()):
            continue
        try:
            ch_par[ch_map[k]] = {
              "thread": v["thread"]
            }
        except KeyError as ke:
            logger.error("%s channel %s missing parameter %s", name, ke)
            return False
    # if new channel dictionary empty, fail --- need at least one channel
    if not ch_par:
        logger.error("%s requires parameters for at least one channel specified", name)
        return False
    logger.debug("%s parameters from device instance are\n    %s", name, "\n    ".join([k + " = " + str(v) for k, v in list(c_param.items())]))
    # find parameters specified elsewhere on the chain
    ch_par_update = _get_band_pol_sideband(name, cfg)
    for ch in list(ch_par.keys()):
        try:
            ch_par[ch].update(ch_par_update[ch])
        except KeyError as ke:
            logger.error("%s band/pol/sideband parameters not found for channel %d", name, ch)
            return False
    ch_par_update = _get_net_param(name, cfg)
    for ch in list(ch_par.keys()):
        try:
            ch_par[ch].update(ch_par_update[ch])
        except KeyError as ke:
            logger.error("%s net parameters not found for channel %d", name, ch)
            return False
    for ch in list(ch_par.keys()):
        logger.debug("%s parameters for channel %d are:\n    %s", name, ch, "\n    ".join([k + " = " + str(v) for k, v in list(ch_par[ch].items())]))
    c_param["channels"] = ch_par
    return cls(**c_param)
