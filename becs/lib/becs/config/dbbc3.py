from . import generic

import logging
logger = logging.getLogger(__name__)

in_ports = ["boardA", "boardB", "boardC", "boardD"]
out_ports = ["A_e0", "A_e2","B_e0", "B_e2","C_e0", "C_e2","D_e0", "D_e2"]

def get_dbbc3_inport_channel(inport):
    return in_ports.index(inport)

def get_dbbc3_channel_inport(channel):
    return in_ports[channel]

def get_dbbc3_outport_channel(outport):
    return out_ports.index(outport)

def get_dbbc3_channel_outport(channel):
    return out_ports[channel]

def  _get_band_pol_sideband(name, cfg):
    cfg_to_param = {
      "band": "bdc_ch",
      "pol": "pol",
      "sideband": "rx_sb"
    }
    dev = cfg.devices[name]
    host = dev["hostname"]
    ch_par = dict()
    for inp in dev["outports"]:
        # skip if this port does not belong to the processed  chains
        ch = get_dbbc3_outport_channel(inp.split(cfg.delim_port)[-1])
        par = dict()
#        par.update({"station": cfg.instruct["stationcode"]})
        this_port = cfg.ports[inp]
        fedby_port = this_port["fedby"]
        par.update({"band": this_port["band"]})
        par.update({"board":inp.split(":")[1].split("_")[0]})
        par.update({"out_port":inp.split(":")[1].split("_")[1]})
        par.update({"band_label":this_port["label"]})

        while fedby_port:
            this_port = cfg.ports[fedby_port]
            for k, v in list(cfg_to_param.items()):
                if k in list(this_port.keys()):
                    par.update({v: this_port[k]})
            fedby_port = this_port["fedby"]
        ch_par.update({ch: par})
    return ch_par
    
    

def _get_net_param(name, cfg):
    dev = cfg.devices[name]
    ch_par = dict()
    for sender in dev["outports"]:
        ch = get_dbbc3_outport_channel(sender.split(cfg.delim_port)[-1])
        receiver = cfg.ports[sender]["feeds"]
        net_param = generic.gen_net_param(cfg, sender, receiver)
        ch_par[ch] = {
          "mac": net_param["send"]["mac"],
          "ip": net_param["send"]["ip"],
          "port": net_param["send"]["port"],
          "dest_mac": net_param["recv"]["mac"],
          "dest_ip": net_param["recv"]["ip"],
          "dest_port": net_param["recv"]["port"],
        }
    return ch_par

def get_dbbc3_inst(name, cfg, chainID, cls):
    try:
        dev = cfg.devices[name]
    except KeyError as ke:
        logger.info("%s not found in config", name)
        return False
    # first find parameters specified at the device itself
    try:
        host = dev["hostname"]
        c_param = {
          "name": name,
          "host": host,
          "mode": dev["mode"],
          "sw_major": dev["sw_major"],
          "sw_minor": dev["sw_minor"],
          "port": dev["port"],
          "chainID": chainID        # needed specifically for the DBBC3 to identify which of the boards processes the chain
        }
    except KeyError as ke:
        logger.info("%s device instance missing required parameter %s", name, ke)
        return False

    # get the chain list corresponding to the given chainID
    chains = []
    if chainID in cfg.chains.keys():
        chains.append(cfg.chains[chainID])
    elif chainID == "all" or chainID == host:
        chains = list(cfg.chains.values())
    elif chainID is None:
        pass
    else:
        logger.info("chain %s not defined in the config file", chainID)
        return False
    c_param["chains"] = chains

    ports = []
    for chain in chains:
        ports.append(chain[-1].split()[0])
    c_param["ports"] = ports

    ch_par = dict()
    board_par = dict()
    dev_layout = dev["layout"]
    ch_map = dict(list(zip(out_ports, list(range(len(out_ports))))))
    board_map = dict(list(zip(in_ports, list(range(len(in_ports))))))

    # add board parameters
    for k, v in list(dev.items()):
        if k not in list(board_map.keys()):
            continue
        try:
            board_par[board_map[k]] = {
              "bitfile": v["bitfile"],
              "filter1": v["filter1"],
              "filter2": v["filter2"],
              "synth_freq": v["synth_freq"],
              "station": cfg.instruct["stationcode"]
            }
        except KeyError as ke:
            logger.info("%s %s missing parameter %s", name,k , ke)
            return False

    # add channel parameters
    for k, v in list(dev.items()):
        if k not in list(ch_map.keys()):
            continue
        try:
            ch_par[ch_map[k]] = {
              "thread": v["thread"]
            }
        except KeyError as ke:
            logger.info("%s channel %s missing parameter %s", name, ke)
            return False

    # if new channel dictionary empty, fail --- need at least one channel
    if not ch_par:
        logger.info("%s requires parameters for at least one channel specified", name)
        return False
    logger.debug("%s parameters from device instance are\n    %s", name, "\n    ".join([k + " = " + str(v) for k, v in list(c_param.items())]))

    # find parameters specified elsewhere on the chain
    ch_par_update = _get_band_pol_sideband(name, cfg)
    for ch in list(ch_par.keys()):
        try:
            ch_par[ch].update(ch_par_update[ch])
        except KeyError as ke:
            logger.info("%s band/pol/sideband parameters not found for channel %d", name, ch)
            return False
    ch_par_update = _get_net_param(name, cfg)
    for ch in list(ch_par.keys()):
        try:
            ch_par[ch].update(ch_par_update[ch])
        except KeyError as ke:
            logger.info("%s net parameters not found for channel %d", name, ch)
            return False
    for ch in list(ch_par.keys()):
        logger.debug("%s parameters for channel %d are:\n    %s", name, ch, "\n    ".join([k + " = " + str(v) for k, v in list(ch_par[ch].items())]))
    c_param["channels"] = ch_par
    c_param["boards"] = board_par
    return cls(**c_param)
