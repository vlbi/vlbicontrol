import logging
logger = logging.getLogger(__name__)

def _get_bdc_param(name, cfg):
    dev = cfg.devices[name]
    c_param = {}
    try:
        host = dev["hostname"]
        c_param.update({
          "name": name,
          "host": host,
          "port": dev["port"],
          "hw": dev["hardware"],
          "fw": dev["firmware"],
        })
    except KeyError as ke:
        logger.debug("%s device instance missing required parameter %s", name, ke)
        return
    logger.debug("%s BDC parameters are %s", host, c_param)
    return c_param

_map_outp_attn_bdc_1band = {
  "sba_pol0_hi": "A0U",
  "sba_pol0_lo": "A0L",
  "sba_pol1_hi": "A1U",
  "sba_pol1_lo": "A1L",
  "sbb_pol0_hi": "B0U",
  "sbb_pol0_lo": "B0L",
  "sbb_pol1_hi": "B1U",
  "sbb_pol1_lo": "B1L",
}

def get_bdc_1band_attn_from_outport(outp):
    try:
        return _map_outp_attn_bdc_1band[outp]
    except KeyError:
        logger.debug("outport '%s' has no matching attenuator", outp)
        return

def get_bdc_1band_outport_from_attn(attn):
    try:
        return [k for k, v in list(_map_outp_attn_bdc_1band.items()) if v == attn][0]
    except IndexError:
        logger.debug("attenuator '%s' has no matching outport", attn)
        return

def get_bdc_1band_inst(name, cfg, cls):
    dev = cfg.devices[name]
    c_param = _get_bdc_param(name, cfg)
    try:
        update = {
          "lo_freq": dev["lo_freq"],
        }
    except KeyError as ke:
        logger.debug("%s device instance missing required parameters %s", name, ke)
        return
    c_param.update(update)
    logger.debug("%s parameters updated with %s", name, update)
    return cls(**c_param)

_map_outp_attn_bdc_2band = {
  "pol0_hi": "P0U",
  "pol0_lo": "P0L",
  "pol1_hi": "P1U",
  "pol1_lo": "P1L",
}

def get_bdc_2band_attn_from_outport(outp):
    try:
        return _map_outp_attn_bdc_2band[outp]
    except KeyError:
        logger.debug("outport '%s' has no matching attenuator", outp)
        return

def get_bdc_2band_outport_from_attn(attn):
    try:
        return [k for k, v in list(_map_outp_attn_bdc_2band.items()) if v == attn][0]
    except IndexError:
        logger.debug("attenuator '%s' has no matching outport", attn)
        return

def get_bdc_2band_inst(name, cfg, cls):
    dev = cfg.devices[name]
    c_param = _get_bdc_param(name, cfg)
    try:
        update = {
          "lo_freq_5-9": dev["lo_freq_5to9"],
          "lo_freq_4-8": dev["lo_freq_4to8"],
        }
    except KeyError as ke:
        logger.debug("%s device instance missing required parameter %s", name, ke)
        return
    c_param.update(update)
    logger.debug("%s parameters updated with %s", name, update)
    return cls(**c_param)

def get_bdc_attn_from_outport(outp):
    if outp in list(_map_outp_attn_bdc_1band.keys()):
        return get_bdc_1band_attn_from_outport(outp)
    elif outp in list(_map_outp_attn_bdc_2band.keys()):
        return get_bdc_2band_attn_from_outport(outp)
    else:
        logger.debug("outport '%s' has no matching attenuator", outp)
        return

def get_bdc_outport_from_attn(attn):
    if attn in list(_map_outp_attn_bdc_1band.values()):
        return get_bdc_1band_outport_from_attn(attn)
    elif attn in list(_map_outp_attn_bdc_2band.keys()):
        return get_bdc_2band_outport_from_attn(attn)
    else:
        logger.debug("attenuator '%s' has no matching attenuator", attn)
        return
