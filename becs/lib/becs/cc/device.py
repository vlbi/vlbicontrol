from .. import common

import redis

import logging
logger = logging.getLogger(__name__)

class ControlComputer(object):

    _oper = "oper"
    _host = "localhost"

    def __init__(self):
        rc, out, err = common.subp_system_call("hostname")
        if rc == 0:
            self._host = out.strip()

    @property
    def configuration(self):
        return ""

    @property
    def host(self):
        return self._host

    def is_configured(self):
        return True

    def configure(self):
        return True

    def testgroup_preconfig(self):
        return all([
          self.test_ntp(),
          self.test_redis_server(),
          self.test_nfsmount(),
        ])

    def testgroup_postconfig(self):
        return True

    def test_ntp(self, max_offset=100.0):
        """Test if NTP is properly sync'ed
        In case of failure:
          - check /etc/ntp.conf on CC
          - start NTP server on CC, 'systemctl start ntpd'
          - check and debug list of peers on CC, 'ntpq -p'"""
        return common.ntp_test(max_offset=max_offset, cmd_is_ntp_running="systemctl status ntpd")

    def test_redis_server(self, hostname="192.168.0.1", port=6379):
        """Test if redis server is accepting connections on VLBI backend network
        In case of failure:
          - check /etc/redis.conf has the line 'bind 192.168.0.1 127.0.0.1'
          - restart the redis server, as root 'systemctl restart redis'"""
        result = True
        try:
            logger.debug("try connecting to redis server on %s:%d", hostname, port)
            redis.Redis(hostname, port).keys()
        except redis.exceptions.ConnectionError as ce:
            logger.debug("ConnectionError encountered: %s", str(ce))
            result = False
        logger.info("%s redis server accepting connections test " + ("passed" if result else "failed"), self._host)
        return result

    def test_nfsmount(self):
        """Test if R2DBE filesystem is being exported by the control computer
        In case of failure:
          - check /etc/exports has the line '/srv/roach2_boot 192.168.0.0/24(rw,subtree_check,no_root_squash,insecure)'
          - restart nfs server, as root 'systemctl restart nfs-server'"""
        rc, out, err = common.subp_system_call("showmount -e")
        if not rc == 0:
            logger.debug("showmount -e returned %d: %s", rc, err)
            return False
        expect = ("/srv/roach2_boot", "192.168.0.0/24")
        logger.debug("expect '%s' in export list", " ".join(expect))
        result = False
        for i, line in enumerate(out.splitlines()):
            if i == 0:
                # skip header line in output
                continue
            logger.debug("export-list item is '%s'", line)
            if all(e in line for e in expect):
                result = True
        logger.info("%s nfs export test " + ("passed" if result else "failed"), self._host)
        return result
