import socket
import struct

### utility functions
mac2int = lambda x: int("".join(x.split(":")), 16)
int2mac = lambda x: ":".join([("%012x" % x)[2*i:2*i+2] for i in range(6)])
ip2int = lambda x: struct.unpack(">I", socket.inet_aton(x))[0]
int2ip = lambda x: socket.inet_ntoa(struct.pack(">I", x))
