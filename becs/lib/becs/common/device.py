import subprocess
import string

from .. import ui

import logging
logger = logging.getLogger(__name__)

###System and remote calls
def subp_system_call(cmd):
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    returncode = proc.wait()
    stdout, stderr = proc.communicate()
    return returncode, stdout.decode(), stderr.decode()

def subp_remote_system_call(cmd, user, host, retries=3):
    # try multiple times if ssh itself returned error (return is 255)
    while retries > 0:
        rc, out, err = subp_system_call("ssh -oBatchMode=yes %s@%s %s" % (user, host, cmd))
        # any other return means ssh was successful, return code originated at remote process
        if not rc == 255:
            break
        retries -= 1
    if rc == 255:
        raise RuntimeError("ssh returned %d, '%s'" % (rc, err.strip()))
    return rc, out, err

###Miscelaneous utilities
def netcat_echo(cmd, host, port):
    rc, out, err = subp_system_call("echo '%s' | ncat %s %d" % (cmd, host, port))
    if not rc == 0:
        raise RuntimeError("ncat returned %d, '%s'" % (rc, err.strip()))
    return out.strip()

def get_iface_ip_addr(iface, user, host):
    rc, out, err = subp_remote_system_call("ip addr show %s | egrep -o 'inet [0-9\.]+' | egrep -o '[0-9\.]+'" % iface, user, host)
    if not rc == 0:
        raise RuntimeError("ip returned %d, '%s'" % (rc, err.strip()))
    return out.strip()

def get_iface_mac_addr(iface, user, host):
    rc, out, err = subp_remote_system_call("ip link show %s | egrep -o 'ether[[:space:]]+[0-9a-fA-F:]+[[:space:]]' | egrep -o '[[:space:]]+[0-9a-fA-F:]+[[:space:]]'" % iface, user, host)
    if not rc == 0:
        raise RuntimeError("ip returned %d, '%s'" % (rc, err.strip()))
    return out.strip().lower()

def get_system_time(user_host=None, fmt="%Y%m%d-%H:%M:%S.%Nz%Z"):
    assert user_host is None or \
      (isinstance(user_host, (tuple, list)) and len(user_host) == 2 and \
      all(isinstance(a, str) for a in user_host)), "user_host should be tuple or list of str containing exactly 2 elements"
    if user_host:
        user, host = user_host
        call = lambda cmd: subp_remote_system_call(cmd, user, host)
    else:
        host = "localhost"
        call = subp_system_call
    rc, out, err = call("date +%s" % fmt)
    if not rc == 0:
        raise RuntimeError("date returned %d, '%s'" % (rc, err.strip()))
    return out.strip()

def remote_copy_file(src, dst, user, host):
    rc, out, err = subp_system_call("scp -oBatchMode=yes %s %s@%s:%s" % (src, user, host, dst))
    if not rc == 0:
        raise RuntimeError("scp returned %d, '%s'" % (rc, err.strip()))
    return rc, out, err

###Generic tests
def ping_test(host):
    rc, out, err = subp_system_call("ping -c 1 %s" % host)
    if rc == 0:
        logger.info("%s ping test passed", host)
        logger.debug("\n".join(out.strip().splitlines()[:2]))
    elif rc == 1:
        logger.info("%s ping test failed", host)
        logger.debug("\n".join(out.strip().splitlines()[:2]))
    else:
        logger.info("an error occurred during call to ping_test")
        logger.debug(err.strip() + out.strip())
    return rc == 0

def remote_login_test(host, user):
    rc, out, err = subp_system_call("ssh -oBatchMode=yes %s@%s exit" % (user, host))
    if rc == 255:
        logger.info("%s login as %s test failed", host, user)
        return False
    logger.info("%s login as %s test passed", host, user)
    return True

def remote_process_running(procname, user, host, *args, invert_logic=False):
    rc, out, err = subp_remote_system_call("pgrep -f %s" % procname, user, host)
    if rc == 0:
        logger.debug("%s PID is %s", procname, out.strip())
    success = 1 if invert_logic else 0
    failure = 0 if invert_logic else 1
    qualifier = "not " if invert_logic else ""
    if rc == success:
        logger.info("%s %s %srunning test passed", host, procname, qualifier)
    elif rc == failure:
        logger.info("%s %s %srunning test failed", host, procname, qualifier)
    else:
        logger.info("an error occurred during call to remote_process_runnign")
        logger.debug(err + out)
    return rc == success

def ntp_test(user_host=None, max_offset=100.0, sys_peer=None, cmd_is_ntp_running=None):
    assert (user_host is None) or (isinstance(user_host, (tuple, list)) and len(user_host) == 2 and \
      all(isinstance(a, str) for a in user_host)), "user_host should be tuple or list of str containing exactly 2 elements"
    assert isinstance(max_offset, (float, int)) and max_offset > 0.0, "max_offset should be positive number"
    assert sys_peer is None or (isinstance(sys_peer, str) and \
      len(sys_peer.split(".")) == 4 and \
      all(c in string.digits for c in "".join(sys_peer.split("."))) and \
      all(int(n) < 256 and int(n) >= 0 for n in sys_peer.split("."))), "sys_peer should be an IP string in dot-decimal notation"
    NTP_PEER_UNREACHABLE, NTP_PEER_FALSETICKER, NTP_PEER_POOR_CANDIDATE, NTP_PEER_OUTLIER, NTP_PEER_SURVIVOR, NTP_PEER_DISTANT_SURVIVOR, NTP_PEER_SYSTEM_PEER, NTP_PEER_SYSTEM_PEER_PPS = list(range(8))
    NTP_PEER_LOOKUP = {
      " ": NTP_PEER_UNREACHABLE,
      "x": NTP_PEER_FALSETICKER,
      ".": NTP_PEER_POOR_CANDIDATE,
      "-": NTP_PEER_OUTLIER,
      "+": NTP_PEER_SURVIVOR,
      "#": NTP_PEER_DISTANT_SURVIVOR,
      "*": NTP_PEER_SYSTEM_PEER,
      "o": NTP_PEER_SYSTEM_PEER_PPS,
    }
    if user_host:
        user, host = user_host
        call = lambda cmd: subp_remote_system_call(cmd, user, host)
    else:
        host = "localhost"
        call = subp_system_call
    # first check if NTP is running
    if cmd_is_ntp_running:
        rc, out, err = call(cmd_is_ntp_running)
        if not rc == 0:
            logger.debug("\n".join(["%s returned %d" % (cmd_is_ntp_running, rc), err, out]))
            logger.info("%s NTP test failed, service not running", host)
            return False
    else:
        cmd = "systemctl status ntpd.service"
        rc, out, err = call(cmd)
        if not rc == 0:
            cmd2 = "service ntp status"
            rc2, out2, err2 = call(cmd2)
            if not rc2 == 0:
                logger.debug("\n".join(["%s returned %d" % (cmd, rc), err, out]))
                logger.info("%s NTP test failed, service not running", host)
                logger.debug("\n".join(["%s returned %d" % (cmd2, rc2), err2, out2]))
                logger.info("%s NTP test failed, service not running", host)
                return False
    rc, out, err = call("ntpq -pn")
    if not rc == 0:
        logger.debug("\n".join(["ntqp returned %d" % rc, err, out]))
        logger.info("%s NTP test failed, error occurred during ntpq call", host)
        return False
    lines = out.splitlines()
    # also if lines come up empty, then fail
    if not lines:
        logger.debug("\n".join(["ntpq returned %d" % rc, err, out]))
        logger.info("%s NTP test failed, could not extract peers from output", host)
        return False
    ntpq = dict()
    keys = lines[0].split()
    for line in lines[2:]:
        values = line.split()
        entry = dict(list(zip(keys, values)))
        if entry["remote"][0] in string.digits:
            remote = entry["remote"]
            peer_status = NTP_PEER_UNREACHABLE
        else:
            remote = entry["remote"][1:]
            peer_status = NTP_PEER_LOOKUP[entry["remote"][0]]
        ntpq[remote] = dict(list(zip(keys[1:], values[1:])))
        ntpq[remote].update({"peer status": peer_status})
    # if sys_peer given, check that it is listed as the system peer
    if sys_peer:
        if sys_peer not in list(ntpq.keys()):
            logger.debug("'%s' not listed as NTP peer of %s", sys_peer, host)
            logger.info("%s NTP test failed, referenced peer not listed", host)
            return False
        peer_status = ntpq[sys_peer]["peer status"]
        logger.debug("peer '%s' has status %d, expect %d", sys_peer, peer_status, NTP_PEER_SYSTEM_PEER)
        if not ntpq[sys_peer]["peer status"] == NTP_PEER_SYSTEM_PEER:
            logger.info("%s NTP test failed, referenced peer is not system peer", host)
            return False
    else:
        # look for the system peer
        for peer, entry in list(ntpq.items()):
            if entry["peer status"] == NTP_PEER_SYSTEM_PEER:
                sys_peer = peer
                logger.debug("found system peer '%s'", sys_peer)
                break
        if not sys_peer:
            logger.info("%s NTP test failed, no system peer found", host)
            return False
    # check system peer offset
    offset = abs(float(ntpq[sys_peer]["offset"]))
    logger.debug("peer '%s' abs(offset) is %.3f ms, expect < %.3f ms", sys_peer, offset, max_offset)
    if offset > max_offset:
        logger.info("%s NTP test failed, abs(offset) too large", host)
        return False
    logger.info("%s NTP test passed", host)
    return True

def system_time_utc_test(user_host=None):
    assert user_host is None or \
      (isinstance(user_host, (tuple, list)) and len(user_host) == 2 and \
      all(isinstance(a, str) for a in user_host)), "user_host should be tuple or list of str containing exactly 2 elements"
    if user_host:
        user, host = user_host
        call = lambda cmd: subp_remote_system_call(cmd, user, host)
    else:
        host = "localhost"
        call = subp_system_call
    rc, out, err = call("date +%Z")
    if not rc == 0:
        logger.debug("\n".join(["date returned %d" % rc, err, out]))
        logger.info("%s system time UTC test failed, error occurred during date call", host)
        return False
    utc = "UTC"
    zone = out.strip()
    logger.debug("system zone is '%s', expect '%s'", zone, utc)
    result = utc == zone
    logger.info("%s system time UTC test " + ("passed" if result else "failed"), host)
    return result
