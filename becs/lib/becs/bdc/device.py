import random
import re
import time

import redis

from .. import common
from .. import ui

import logging
logger = logging.getLogger(__name__)

class BDCError(RuntimeError):
    # General error, not supposed to happen ever
    # Numbers: 1, 2, 3, 4, 6, 7, 11
    pass

class BDCRemoteDisabledError(RuntimeError):
    # Number 8
    pass

class BDCTemporaryLockoutError(RuntimeError):
    # Number 9
    pass

class BDCParameterLockedError(RuntimeError):
    # Number 10
    pass

class BDCParameterOutOfRangeError(ValueError):
    # Number 5
    pass

class BDC(object):

### exposed
    ATTNS = []
    ATTN_RANGE = [0, 31.5]
    BANDS = []
    PARAMS = ["name", "host", "port", "hw", "fw"]

### properties
    @property
    def host(self):
        return self._host

    @property
    def name(self):
        return self._name

    @property
    def serial(self):
        return self.get_id()["serial number"]

    @property
    def configuration(self):
        idn = self.get_id()
        s_idn = self._host + " :  " + "  ".join(idn[k] for k in ["vendor","device type","serial number", "revision"])
        lock = self.get_lock_status()
        s_band_lock = " "*len(self._host) + " : " + "%s" + "  LO: " + "   ".join("%s = %s" % (k,v) for k,v in list(lock.items()))
        try:
            band = "Band: " + self.get_band() + " "
        except AttributeError:
            band = ""
        s_band_lock %= band
        attn = self.get_attn()
        # sort attenuators by subband before pol
        attn_k = sorted(list(attn.keys()), key=lambda x: x[0]+x[2]+x[1])
        # put four attenuators on a single line
        s_attn = " "*len(self._host) + " : Attn: "
        s_attn += ("\n" + " "*len(s_attn)).join("   ".join("%s = %4.1fdB" % (k, attn[k]) for k in attn_k[n*4:n*4+4]) for n in range(len(attn_k)//4))
        ctrl = self.get_ctrl_status()
        conf = self.get_conf()
        s_ctrl_conf = " "*len(self._host) + " : Ctrl: " + ctrl + "   Conf: " + conf
        return "\n".join((s_idn, s_band_lock, s_attn, s_ctrl_conf))

### conglomerate procedures
    def is_configured(self):
        conf = self.get_conf()
        logger.debug("%s conf is %s, self._CONF is %s", self._host, conf, self._CONF)
        return all([conf == self._CONF])

    def configure(self):
        return self.conf_conf()

    def testgroup_preconfig(self):
        if not self.test_device_reachable():
            return False
        result = all([
          self.test_firmware_version(),
          self.test_hardware_version(),
          self.test_remote_control(),
        ])
        return result

### configure steps
    def conf_conf(self):
        result = self.set_conf()
        logger.info("%s setting %s-band configuration " + ("succeeded" if result else "failed"), self._host, self._CONF)
        return result

### tests
    def test_device_reachable(self):
        """Test if device is reachable on the network
        In case of failure:
          - check if BDC is powered on
          - check network cable
          - check network configuration on front panel interface"""
        return common.ping_test(self._host)

    def test_firmware_version(self):
        """Test if firmware version matches configuration
        In case of failure:
          - flash device with correct version of firmware
          - contact support"""
        fw = self.get_firmware_version()
        logger.debug("%s firmware version is '%s', expect '%s'", self._host, fw, self._fw)
        result = fw == self._fw
        logger.info("%s firmware version test " + ("passed" if result else "failed"), self._host)
        return result

    def test_hardware_version(self):
        """Test if hardware version matches configuration
        In case of failure:
          - contact support"""
        hw = self.get_hardware_version()
        logger.debug("%s hardware version is '%s', expect '%s'", self._host, hw, self._hw)
        result = hw == self._hw
        logger.info("%s hardware version test " + ("passed" if result else "failed"), self._host)
        return result

    def test_remote_control(self):
        """Test if remote control is enabled
        In case of failure:
          - set BDC control status to either 'both' or 'remote'
            (possible from front panel interface or command-line)"""
        refs = ["BOTH", "REMOTE"]
        status = self.get_ctrl_status()
        logger.debug("%s ctrl status is '%s', expect one of ['%s']", self._host, status, "', '".join(refs))
        result = status in refs
        logger.info("%s remote ctrl test " + ("passed" if result else "failed"), self._host)
        return result

### hidden
    _err_pattern = re.compile("ERR[\s]+([0-9]+):")
    _rev_hw_pattern = re.compile("HWv([0-9]+\.[0-9a-z]+)")
    _rev_fw_pattern = re.compile("FWv([0-9]+\.[0-9a-z]+)")

    _CONF = ""

    def __init__(self, redishost="localhost", redisport=6379, **kwargs):
        assert all(p in list(kwargs.keys()) for p in self.PARAMS), "kwargs has to include the following keys for %s:\n  - %s" % (self.__class__.__name__, "\n  - ".join(self.PARAMS))
        for k, v in list(kwargs.items()):
            setattr(self, "_" + k, v)
        self._redis = redis.Redis(host=redishost, port=redisport)

    def __str__(self):
        return self._host

    def _send_recv(self, cmd):
        assert isinstance(cmd, str), "cmd should be a str"
        reply = common.netcat_echo(cmd, self._host, self._port).strip()
        # check for errors
        err_match = self._err_pattern.search(reply)
        if err_match:
            logger.error("BDC %s replied with error '%s'", self._host, reply)
            errno = int(err_match.group(1))
            if errno == 8:
                raise BDCRemoteDisabledError("remote control disabled, if local control not in use, do set_control_remote first (sent '%s' to %s and got reply '%s')" % (cmd, self._host, reply))
            if errno == 9:
                raise BDCTemporaryLockoutError("temporary lockout, if local control not in use, do override_temporary_remote_lockout first (sent '%s' to %s and got reply '%s')" % (cmd, self._host, reply))
            if errno == 10:
                raise BDCParameterLockedError("parameter locked, if safe to do so, do config_safety_unlock first (sent '%s' to %s and got reply '%s')" % (cmd, self._host, reply))
            raise BDCError("sent '%s' to %s and got reply '%s'" % (cmd, self._host, reply))
        return reply

    def _claim_set(self, param, release_after_ms=1000):
        lock_name = "%s.lock.%s" % (self._name, param)
        lock_value = "%8x" % random.randint(0,2**32-1)
        # write unique value that expires after few seconds
        # call returns True only if key does not exist
        _reported_lock = False
        while not self._redis.set(lock_name, lock_value, px=release_after_ms, nx=True):
            expires_in_ms = self._redis.pttl(lock_name)
            wait = max(0.05, expires_in_ms/1000.0)
            if not _reported_lock:
                logger.debug("%s lock %s=%s exists for parameter %s, expires in %dms", self._host, lock_name, lock_value, param, expires_in_ms)
                _reported_lock = True
            time.sleep(wait)

### get-set methods
    def get_id(self):
        reply = self._send_recv("*idn?")
        vendor, device, serial, revision = reply.split(",")
        result = {
          "vendor": vendor,
          "device type": device,
          "serial number": serial,
          "revision": revision,
        }
        hw_match = self._rev_hw_pattern.search(revision)
        if hw_match:
            result.update({"hardware version": hw_match.group(1)})
        fw_match = self._rev_fw_pattern.search(revision)
        if fw_match:
            result.update({"firmware version": fw_match.group(1)})
        return result

    def get_hardware_version(self):
        result = self.get_id()
        try:
            return result["hardware version"]
        except KeyError:
            raise BDCError("could not determine hardware version for BDC %s, revision is '%s'", self._host, result["revision"])

    def get_firmware_version(self):
        result = self.get_id()
        try:
            return result["firmware version"]
        except KeyError:
            raise BDCError("could not determine firmware version for BDC %s, revision is '%s'", self._host, result["revision"])

    def get_conf(self):
        return self._send_recv("conf?")

    def set_conf(self):
        # base class does not implement anything, set self._CONF in children classes
        if not self._CONF:
            return True
        # first check if already dual
        _conf = self.get_conf()
        if _conf == self._CONF:
            logger.debug("%s is already set to conf '%s', should be '%s'", self._host, _conf, self._CONF)
            return True
        # if not, then set to dual
        # .., but claim setting conf first
        self._claim_set("conf")
        self.config_safety_unlock()
        self._send_recv("conf %s" % self._CONF)
        self.config_safety_lock()
        # finally test to make sure it worked
        _conf = self.get_conf()
        logger.debug("%s set conf '%s', read back '%s'", self._host, self._CONF, _conf)
        return _conf == self._CONF

    def get_ctrl_status(self):
        return self._send_recv("ctrl?").strip()

    def set_ctrl_remote(self):
        self._send_recv("ctrl remote")
        return self.get_ctrl_status() == "REMOTE"

    def set_ctrl_both(self):
        self._send_recv("ctrl both")
        return self.get_ctrl_status() == "BOTH"

    def override_tmp_remote_lockout(self):
        self._send_recv("ctrl clear")
        return not self.get_ctrl_status() == "LOCAL_TMP"

    def config_safety_lock(self):
        self._send_recv("cnbl off")

    def config_safety_unlock(self):
        self._send_recv("cnbl on")

    def get_attn(self, *attns):
        assert all(a in self.ATTNS for a in attns), "attns should each be one of ['%s']" % "', '".join(self.ATTNS)
        send_recv_attn = lambda a: float(self._send_recv("attn? %s" % a))
        if not attns:
            attns = self.ATTNS
        vals = list(map(send_recv_attn, attns))
        return dict(list(zip(attns, vals)))

    def set_attn(self, **attns_vals):
        assert all(a in self.ATTNS for a in list(attns_vals.keys())), "attns_vals keys should each be one of ['%s']" % "', '".join(self.ATTNS)
        assert all(isinstance(v, (float, int)) for v in list(attns_vals.values())) and \
          all(v >= self.ATTN_RANGE[0] for v in list(attns_vals.values())) and \
          all(v <= self.ATTN_RANGE[1] for v in list(attns_vals.values())), "attns_vals values should each be a number in range [%.1f, %.1f]" % (self.ATTN_RANGE[0], self.ATTN_RANGE[1])
        attns = list(attns_vals.keys())
        # make sure value fractional part is exacly 0.0 or 0.5
        vals = [round(attns_vals[k] * 2.0) / 2.0 for k in list(attns_vals.keys())]
        send_recv_attn = lambda a, v: self._send_recv("attn %s %.1f" % (a, v))
        # claim setting attn first
        self._claim_set("attn")
        list(map(send_recv_attn, attns, vals))
        attns_vals_read = self.get_attn(*list(attns_vals.keys()))
        return all(attns_vals[k] == attns_vals_read[k] for k in list(attns_vals.keys()))

    def get_lock_status(self, *bands):
        assert all(b in self.BANDS for b in bands), "bands should each be one of ['%s']" % "', '".join(self.BANDS)
        send_recv_lock = lambda b: self._send_recv("lock? %s" % b)
        if not bands:
            bands = self.BANDS
        result = list(map(send_recv_lock, bands))
        if len(bands) == 1:
            return result[0]
        return dict(list(zip(bands, result)))

    def get_lo_freq(self, *bands):
        assert all(b in self.BANDS for b in bands), "bands should each be one of ['%s']" % "', '".join(self.BANDS)
        send_recv_freq = lambda b: int(self._send_recv("freq? %s" % b))
        if not bands:
            bands = self.BANDS
        result = list(map(send_recv_freq, bands))
        if len(bands) == 1:
            return result[0]
        return dict(list(zip(bands, result)))

class SingleBandBDC(BDC):

### exposed
    BANDS = ["A", "B"]
    ATTNS = ["A0L", "A0U", "A1L", "A1U", "B0L", "B0U", "B1L", "B1U"]
    PARAMS = BDC.PARAMS + ["lo_freq"]

### hidden
    _CONF = "SINGLE"

### macro procedures
    def testgroup_postconfig(self):
        result = all([
          self.test_correct_lo_freq(),
          self.test_lock_status(),
        ])
        return result

### tests
    def test_correct_lo_freq(self):
        """Test if LO frequency matches the configuration"""
        freqs = self.get_lo_freq()
        result = []
        for label, freq in list(freqs.items()):
            logger.debug("%s LO %s freq is %d MHz, expect %d MHz", self._host, label, freq, self._lo_freq)
            result.append(freq == self._lo_freq)
        result = all(result)
        logger.info("%s LO freq test " + ("passed" if result else "failed"), self._host)
        return result

    def test_lock_status(self):
        """Test the LO lock status, for single-band BDC both LOs should be locked.
        In case of failure:
          - check that BDC has single-band configuration loaded
          - check reference input
          - contact support"""
        lock_status = self.get_lock_status()
        result = []
        for label, status in list(lock_status.items()):
            logger.debug("%s LO '%s' status is '%s', expect LOCKED", self._host, label, status)
            result.append(status == "LOCKED")
        result = all(result)
        logger.info("%s lock status test " + ("passed" if result else "failed"), self._host)
        return result

class DualBandBDC(BDC):

### exposed
    BANDS = ["4-8", "5-9"]
    ATTNS = ["P0L", "P0U", "P1L", "P1U"]
    PARAMS = BDC.PARAMS + ["lo_freq_5-9", "lo_freq_4-8"]

### hidden
    _CONF = "DUAL"

### conglomerate procedures
    def testgroup_postconfig(self):
        result = all([
          self.test_correct_lo_freqs(),
          self.test_lock_status(),
        ])
        return result

### tests
    def test_correct_band(self, band_ref):
        """Test if band setting matches given reference"""
        band = self.get_band()
        logger.debug("%s band is '%s', expect '%s'", self._host, band, band_ref)
        result = band == band_ref
        logger.info("%s correct band test " + ("passed" if result else "failed"), self._host)
        return result

    # test
    def test_correct_lo_freqs(self):
        """Test if LO frequencies match the configuration"""
        result = []
        freqs = self.get_lo_freq()
        for b, f in list(freqs.items()):
            f_ref = getattr(self, "_lo_freq_" + b)
            logger.debug("%s LO freq for band %s is %d MHz, expect %d MHz", self._host, b, f, f_ref)
            result.append(f == f_ref)
        result = all(result)
        logger.info("%s LO freq test " + ("passed" if result else "failed"), self._host)
        return result

    def test_lock_status(self):
        """Test the LO lock status, for dual-band BDC switched LO should be
        locked and unswitched LO off.
        In case of failure:
          - check that BDC has dual-configuration loaded
          - check reference input
          - contact support"""
        lock_status = self.get_lock_status()
        band = self.get_band()
        result = []
        for b, status in list(lock_status.items()):
            if b == band:
                logger.debug("%s LO '%s' status is '%s', expect 'LOCKED'", self._host, b, status)
                result.append(status == "LOCKED")
                continue
            logger.debug("%s LO '%s' status is '%s', expect 'OFF'", self._host, b, status)
            result.append(status == "OFF")
        result = all(result)
        logger.info("%s lock status test " + ("passed" if result else "failed"), self._host)
        return result

### get-set methods
    def get_band(self):
        return self._send_recv("band?")

    def set_band(self, band):
        assert band in self.BANDS, "band should be one of ['%s']" % "', '".join(self.BANDS)
        # first check if already correct band
        _band = self.get_band()
        if _band == band:
            logger.debug("%s is already set to band '%s', should be '%s'", self._host, _band, band)
            return True
        # claim setting band first
        self._claim_set("band")
        # if not, set the band
        self._send_recv("band %s" % band)
        # finally test to make sure it worked
        _band = self.get_band()
        logger.debug("%s set band '%s', read back '%s'", self._host, band, _band)
        return _band  == band
