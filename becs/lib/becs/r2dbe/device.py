import datetime
import math
import pathlib
import random
import re
import string
import time

import adc5g
import redis

import katcplight
import vdif

from .. import common

from . import adc
from . import data

import logging
logger = logging.getLogger(__name__)

# set katcplight logger to high value
kl_logger = logging.getLogger("katcplight")
kl_logger.setLevel(logging.WARNING)

class R2DBE(object):

### exposed
    PARAMS = ["name", "host", "boffile", "version", "channels"]
    CH_PARAMS = [
      "bdc_ch",
      "pol",
      "rx_sb",
      "station",
      "thread",
      "mac",
      "ip",
      "port",
      "dest_mac",
      "dest_ip",
      "dest_port",
    ]

### hidden
    _root = "root"

### properties
    @property
    def host(self):
        return self._host

    @property
    def name(self):
        return self._name

    @property
    def serial(self):
        return common.get_iface_mac_addr("eth0", "root", self.host).replace(":","")

    @property
    def configuration(self):
        s_idn = self.host + " : "
        s_bof = " BOF: "
        try:
            s_bof += self._klc.fpgastatus()
        except katcplight.KLClientError:
            # something went wrong, write it and try to continue
            s_bof += "!! failed to read FPGA status"
        except TypeError:
            # fpgastatus returned None, no sense continuing
            return s_idn + "FPGA not programmed"
        s_idn += s_bof
        s_rev = "   Rev: "
        try:
            rcs = self._klc.get_rcs()["app_rev"]
            s_rev += "%x" % rcs
        except (katcplight.KLClientError, KeyError):
            s_rev += "!! failed to read firmware revision"
        s_idn += s_rev
        s_vdif_line = "%3s  %3s  %4s  %5s  %7s  %5s"
        s_vdif = " "*len(self.host) + " : VDIF: " + s_vdif_line % ("IF", "Pol", "RxSB", "BDCSB", "Station", "2-bit")
        for ch in data.CH:
            # use VDIF-enabled bit to filter unconfigured channels
            dev = "r2dbe_vdif_%d_enable" % ch
            try:
                if not self._klc.read_int(dev):
                    # channel not configured, skip
                    continue
            except katcplight.KLClientError:
                s_vdif += "\n" + " "*len(self.host) + " :     : !! could not determine if channel %d configured" % ch
                continue
            l_if = "if%d" % ch
            l_pol = self.get_pol(ch)
            l_rxsb = self.get_rx_sb(ch)
            l_bdcsb = self.get_bdc_ch(ch)
            l_station = self.get_station(ch)
            l_2bit = self.get_2bit_threshold(ch)
            s_vdif += "\n" + " "*len(self.host) + " :     : " + s_vdif_line % (l_if, l_pol, l_rxsb, l_bdcsb, l_station, l_2bit)
        s_outp_line = "%3s %17s  %15s.%5s  %17s  %15s.%5s"
        s_outp = " "*len(self.host) + " :  Net: " + s_outp_line % ("Ch", "MAC", "IP", "Port", "Dest MAC", "Dest IP", "Port")
        for ch in data.CH:
            # use VDIF-enabled bit to filter unconfigured channels
            dev = "r2dbe_vdif_%d_enable" % ch
            try:
                if not self._klc.read_int(dev):
                    # channel not configured, skip
                    continue
            except katcplight.KLClientError:
                s_vdif += "\n" + " "*len(self.host) + " :     : !! could not determine if channel %d configured" % ch
                continue
            s_outp += "\n" + " "*len(self.host) + " :     : " + s_outp_line % (("ch%d"%ch, ) + tuple(str(getattr(self, "get_%s"%a)(ch)) for a in (
              "mac", "ip", "port", "dest_mac", "dest_ip", "dest_port")))
        return "\n".join((s_idn, s_vdif, s_outp))

### macro procedures
    def is_configured(self):
        #~ logger.warn("%s does not yet implement is_configured, assuming device is unconfigured", self.__class__.__name__)
        #~ return False
        # is fpga programmed with correct firmware?
        try:
            expect, status = self._boffile, self._klc.fpgastatus()
            logger.debug("%s programmed with %s, expect %s", self._host, status, expect)
            if not expect == status:
                return False
            expect, status = int(self._version, 16), self._klc.get_rcs()["app_rev"]
            logger.debug("%s firmware version is %07x, expect %07x", self._host, status, expect)
            if not expect == status:
                return False
        except (katcplight.KLClientError, KeyError) as ke:
            logger.debug("%s %s: %s", self._host, ke.__class__.__name__, str(ke))
            return False
        # are VDIF headers written correctly?
        ch_expect_get = []
        for ch, par in list(self._channels.items()):
            ch_expect_get += [
              (ch, "adc", self.get_data_source),
              (ch, par["pol"], self.get_pol),
              (ch, par["bdc_ch"], self.get_bdc_ch),
              (ch, par["rx_sb"], self.get_rx_sb),
              (ch, par["station"], self.get_station),
              (ch, par["thread"], self.get_thread),
            ]
        for ch, expect, get in ch_expect_get:
            status = get(ch)
            logger.debug("%s ch %d source is %s, expect %s", self._host, ch, status, expect)
            if not status == expect:
                return False
        # is VDIF formwat written correctly?
        for ch in list(self._channels.keys()):
            for dev, expect in list({
              "r2dbe_vdif_%d_reorder_2b_samps" % ch: 1,
              "r2dbe_vdif_%d_little_end" % ch: 1,
              "r2dbe_vdif_%d_test_sel" % ch: 0,
            }.items()):
                try:
                    status = self._klc.read_uint(dev)
                    logger.debug("%s %s is %d, expect %d", self._host, dev, status, expect)
                    if not status == expect:
                        return False
                except katcplight.KLClientError as klce:
                    logger.error("%s KLClientError: %s", self._host, str(klce))
                    return False
        # is network configured correctly?
        for ch, par in list(self._channels.items()):
            mac_int = common.mac2int(par["mac"])
            ip_int = common.ip2int(par["ip"])
            dest_ip_int = common.ip2int(par["dest_ip"])
            dest_mac_int = common.mac2int(par["dest_mac"])
            try:
                core_conf = self._klc.get_10gbe_core_details("r2dbe_tengbe_%d_core" % ch)
            except katcplight.KLClientError as klce:
                logger.debug("%s failed reading 10GbE %d, KLCLientError: %s", self._host, ch, klce)
                return False
            logger.debug("%s ch %d IP is %s, expect %s", self._host, ch, common.int2ip(core_conf["my_ip"]), par["ip"])
            if not ip_int == core_conf["my_ip"]:
                return False
            logger.debug("%s ch %d port is %d, expect %d", self._host, ch, core_conf["fabric_port"], par["port"])
            if not par["port"] == core_conf["fabric_port"]:
                return False
            logger.debug("%s ch %d MAC is %s, expect %s", self._host, ch, common.int2mac(core_conf["mymac"]), par["mac"])
            if not ip_int == core_conf["my_ip"]:
                return False
            logger.debug("%s ch %d ARP entry %d is %s, expect %s", self._host, ch, dest_ip_int & 0xff, common.int2mac(core_conf["arp"][dest_ip_int & 0xff]), par["dest_mac"])
            if not dest_mac_int == core_conf["arp"][dest_ip_int & 0xff]:
                return False
            expect, status = par["dest_ip"], self.get_dest_ip(ch)
            logger.debug("%s ch %d destination IP is %s, expect %s", self._host, ch, status, expect)
            if not status == expect:
                return False
            expect, status = par["dest_port"], self.get_dest_port(ch)
            logger.debug("%s ch %d destination port is %d, expect %d", self._host, ch, status, expect)
            if not status == expect:
                return False
            # reset transmission
            dev = "r2dbe_tengbe_%d_rst" % ch
            expect = 0
            try:
                status = self._klc.read_int(dev)
            except katcplight.KLClientError as klce:
                logger.error("%s failed reading %s, KLClientError: %s", self._host, dev, str(klce))
                return False
            logger.debug("%s ch %d %s is %d, expect %d", self._host, ch, dev, status, expect)
            if not status == expect:
                return False
        # is data transmission enabled?
        for ch in list(self._channels.keys()):
            dev = "r2dbe_vdif_%d_enable" % ch
            expect = 1
            try:
                status = self._klc.read_int(dev)
            except katcplight.KLClientError as klce:
                logger.error("%s failed reading %s, KLClientError: %s", self._host, dev, str(klce))
                return False
            logger.debug("%s ch %d %s is %d, expect %d", self._host, ch, dev, status, expect)
            if not status == expect:
                return False
        return True

    def configure(self):
        if not self.conf_program():
            return False
        # test if system clock running here
        if not self.test_sys_clk_incr():
            return False
        if not self.conf_adc_interfaces():
            return False
        if not self.conf_vdif_headers():
            return False
        if not self.conf_vdif_data_format():
            return False
        if not self.conf_time_sync():
            return False
        if not self.conf_network():
            return False
        # pause for signals to propagate
        time.sleep(2)
        if not self.conf_data_transmission():
            return False
        if not self.conf_adc_quadcores():
            return False
        if not self.conf_2bit_thresholds():
            return False
        if not self.conf_start_r2daemon():
            return False
        return True

    def testgroup_preconfig(self):
        if not self.test_device_reachable():
            return False
        if not self.test_login_root():
            return False
        result = all([
          self.test_ntp(),
          self.test_system_time_utc(),
        ])
        # subsequent tests require tcpborphserver
        if not self.test_tcpborphserver_running():
            return False
        result = result and all([
          self.test_firmware_file_available(),
        ])
        return result

    def testgroup_postconfig(self):
        if not self.test_firmware_running_version():
            return False
        result = all([
          self.test_time_sync(),
          self.test_pps(),
          self.test_r2daemon_running(),
        ])
        return result

### config steps
    def conf_program(self):
        result = True
        # raises katcplight.KLClientError on failure
        try:
            logger.debug("%s using boffile='%s'", self._host, self._boffile)
            self._klc.progdev(self._boffile)
        except katcplight.KLClientError as klce:
            logger.debug("%s failed to program FPGA: '%s'", self._host, klce)
            result = False
        logger.info("%s programming FPGA " + ("succeeded" if result else "failed"), self._host)
        return result

    def conf_adc_interfaces(self):
        result = []
        for ch in list(self._channels.keys()):
            result.append(self.calibrate_adc_interface(ch))
        result = all(result)
        logger.info("%s calibrating ADC interfaces " + ("succeeded" if result else "failed"), self._host)
        return result

    def conf_vdif_headers(self):
        result = []
        for ch, par in list(self._channels.items()):
            src = "adc"
            result.append(self.set_data_source(ch, src))
            logger.debug("%s set ch %d data source '%s' " + ("succeeded" if result[-1] else "failed"), self._host, ch, src)
            result.append(self.set_pol(ch, par["pol"]))
            logger.debug("%s set ch %d polarisation '%s' " + ("succeeded" if result[-1] else "failed"), self._host, ch, par["pol"])
            result.append(self.set_bdc_ch(ch, par["bdc_ch"]))
            logger.debug("%s set ch %d BDC channel '%s' " + ("succeeded" if result[-1] else "failed"), self._host, ch, par["bdc_ch"])
            result.append(self.set_rx_sb(ch, par["rx_sb"]))
            logger.debug("%s set ch %d receiver sideband '%s' " + ("succeeded" if result[-1] else "failed"), self._host, ch, par["rx_sb"])
            eud_vers = 0x02
            result.append(self.set_eud_vers(ch, eud_vers))
            logger.debug("%s set ch %d VDIF EUD version %d " + ("succeeded" if result[-1] else "failed"), self._host, ch, eud_vers)
            result.append(self.set_station(ch, par["station"]))
            logger.debug("%s set ch %d station code '%s' " + ("succeeded" if result[-1] else "failed"), self._host, ch, par["station"])
            result.append(self.set_thread(ch, par["thread"]))
            logger.debug("%s set ch %d thread ID '%s' " + ("succeeded" if result[-1] else "failed"), self._host, ch, par["thread"])
        result = all(result)
        logger.info("%s configuring VDIF headers " + ("succeeded" if result else "failed"), self._host)
        return result

    def conf_vdif_data_format(self):
        result = []
        for ch in list(self._channels.keys()):
            for dev, val in list({
              "r2dbe_vdif_%d_reorder_2b_samps" % ch: 1,
              "r2dbe_vdif_%d_little_end" % ch: 1,
              "r2dbe_vdif_%d_test_sel" % ch: 0,
            }.items()):
                try:
                    self._klc.write_uint(dev, val)
                    logger.debug("%s set %s to %d", self._host, dev, val)
                    result.append(True)
                except katcplight.KLClientError as klce:
                    logger.debug("%s failed writing to %s: %s", self._host, dev, klce)
                    result.append(False)
        result = all(result)
        logger.info("%s configuring VDIF data format " + ("succeeded" if result else "failed"), self._host)
        return result

    def conf_time_sync(self):
        result = []
        # arm 1PPS
        dev = "r2dbe_onepps_ctrl"
        for val in (1<<31, 0):
            try:
                self._klc.write_uint(dev, val)
                logger.debug("%s set %s to %d", self._host, dev, val)
                result.append(True)
            except katcplight.KLClientError as klce:
                logger.debug("%s failed writing to %s: %s", self._host, dev, klce)
                result.append(False)
        # wait for a 1PPS pulse
        time.sleep(2)
        # reset VDIF time keeping
        for ch in list(self._channels.keys()):
            dev = "r2dbe_vdif_%d_hdr_w0_reset" % ch
            val = 1
            try:
                self._klc.write_int(dev, val)
                logger.debug("%s set %s to %d", self._host, dev, val)
                result.append(True)
            except katcplight.KLClientError as klce:
                logger.debug("%s failed writing to %s: %s", self._host, dev, klce)
                result.append(False)
        self._wait_until_mid_second()
        vdif_time = vdif.VDIFTime.from_datetime(datetime.datetime.utcnow())
        for ch in list(self._channels.keys()):
            dev = "r2dbe_vdif_%d_hdr_w0_reset" % ch
            val = 0
            try:
                self._klc.write_int(dev, val)
                logger.debug("%s set %s to %d", self._host, dev, val)
                result.append(True)
            except katcplight.KLClientError as klce:
                logger.debug("%s failed writing to %s: %s", self._host, dev, klce)
                result.append(False)
        # set VDIF reference time
        for ch in list(self._channels.keys()):
            result.append(self.set_ref_sec(ch, vdif_time.sec))
            logger.debug("%s set ch %d VDIF second %d " + ("succeeded" if result[-1] else "failed"), self._host, ch, vdif_time.sec)
            result.append(self.set_ref_epoch(ch, vdif_time.epoch))
            logger.debug("%s set ch %d VDIF reference epoch %d " + ("succeeded" if result[-1] else "failed"), self._host, ch, vdif_time.epoch)
        result = all(result)
        logger.info("%s configuring time-synchronisation " + ("succeeded" if result else "failed"), self._host)
        return result

    def conf_network(self):#, ch, mac, ip, port, dest_mac, dest_ip, dest_port):
        result = []
        for ch, par in list(self._channels.items()):
            # configure 10GbE core
            mac_int = common.mac2int(par["mac"])
            ip_int = common.ip2int(par["ip"])
            arp = [0xffffffff,] * 256
            arp_index = common.ip2int(par["dest_ip"]) & 0xff
            arp[arp_index] = common.mac2int(par["dest_mac"])
            try:
                self._klc.config_10gbe_core("r2dbe_tengbe_%d_core" % ch, mac_int, ip_int, par["port"], arp)
                logger.debug("%s set 10GbE %d mac=%d(%s), ip=%d(%s), port=%d", self._host, ch, mac_int, par["mac"], ip_int, par["ip"], par["port"])
                result.append(True)
            except katcplight.KLClientError as klce:
                logger.debug("%s failed configuring 10GbE %d: %s", self._host, ch, klce)
                result.append(False)
            # set destination IP and port (destination MAC part of 10GbE core config)
            result.append(self.set_dest_ip(ch, par["dest_ip"]))
            logger.debug("%s set ch %d destination IP %s " + ("succeeded" if result[-1] else "failed"), self._host, ch, par["dest_ip"])
            result.append(self.set_dest_port(ch, par["dest_port"]))
            logger.debug("%s set ch %d destination port %d " + ("succeeded" if result[-1] else "failed"), self._host, ch, par["dest_port"])
            # reset transmission
            dev = "r2dbe_tengbe_%d_rst" % ch
            for val in (1, 0):
                try:
                    self._klc.write_int(dev, val)
                    logger.debug("%s set %s to %d", self._host, dev, val)
                    result.append(True)
                except katcplight.KLClientError as klce:
                    logger.debug("%s failed writing to %s: %s", self._host, dev, klce)
                    result.append(False)
        result = all(result)
        logger.info("%s configuring network " + ("succeeded" if result else "failed"), self._host)
        return result

    def conf_data_transmission(self):
        result = []
        for ch in list(self._channels.keys()):
            dev = "r2dbe_vdif_%d_enable" % ch
            val = 1
            try:
                self._klc.write_int(dev, val)
                logger.debug("%s set %s to %d", self._host, dev, val)
                result.append(True)
            except katcplight.KLClientError as klce:
                logger.debug("%s failed writing to %s: %s", self._host, dev, klce)
                result.append(False)
        result = all(result)
        logger.info("%s configuring data transmission " + ("succeeded" if result else "failed"), self._host)
        return result

    def conf_adc_quadcores(self):
        result = []
        for ch in list(self._channels.keys()):
            result.append(self.calibrate_adc_cores(ch))
        result = all(result)
        logger.info("%s calibrating ADC cores " + ("succeeded" if result else "failed"), self._host)
        return result

    def conf_2bit_thresholds(self):
        result = []
        for ch in list(self._channels.keys()):
            result.append(self.set_2bit_threshold(ch))
        result = all(result)
        logger.info("%s setting 2-bit threshold " + ("succeeded" if result else "failed"), self._host)
        return result

    def conf_start_r2daemon(self):
        result = self.start_r2daemon()
        logger.info("%s starting r2daemon " + ("succeeded" if result else "failed"), self._host)
        return result

### tests
    def test_device_reachable(self):
        """Test if device is reachable on the network
        In case of failure:
          - check if R2DBE is powered on
          - check network cable (connect to PPC NET port on back of device)
          - check /etc/dnsmasq.conf on control computer has correct MAC address"""
        return common.ping_test(self._host)

    def test_tcpborphserver_running(self):
        """Test if tcpborphserver3 is running on R2DBE PowerPC
        In case of failure:
          - try '$ ssh root@<r2dbe-hostname> "tcpborphserver3"'
          - reboot R2DBE (process should start on boot)"""
        return common.remote_process_running("tcpborphserver3", "root", self._host)

    def test_firmware_file_available(self):
        """Test if the firmware file specified in configuration is available
        In case of failure:
          - check if file exists in /boffiles on R2DBE PowerPC"""
        try:
            result = self._boffile in self._klc.listbof()
            logger.info("%s firmware file '%s' available test " + ("passed" if result else "failed"), self._host, self._boffile)
            return result
        except katcplight.KLClientError as klce:
            logger.info("%s firmware file '%s' available test failed, an error occurred: %s", self._host, self._boffile, klce)
        return False

    def test_firmware_running_version(self):
        """Test if firmware file version matches configuration
        In case of failure:
          - contact support"""
        short = "%s programmed with correct firmware test "
        try:
            running = self._klc.fpgastatus()
            if not running:
                logger.info(short + "failed, FPGA not programmed", self._host)
                return False
        except katcplight.KLClientError as klce:
            logger.info(short + "failed, an error occurred while reading FPGA status: %s", self._host, klce)
            return False
        logger.debug("%s programmed with '%s', expect '%s'", self._host, running, self._boffile)
        if not running == self._boffile:
            logger.info(short + "failed, FPGA programmed with wrong file", self._host)
            return False
        try:
            if not all(d in self._klc.listdev() for d in ["rcs_app", "rcs_lib", "rcs_user"]):
                logger.info(short + "failed, no rcs_block found in firmware", self._host)
                return False
            rcs = self._klc.get_rcs()
        except katcplight.KLClientError as klce:
            logger.info(short + "failed, an error occurred while reading rcs_block: %s", self._host, klce)
            return False
        for k, v in list({
          "app_rcs_type": "git",
          "app_rev": int(self._version, 16),
          "app_dirty": False,
        }.items()):
            logger.debug("%s rcs[%s] = %s, expect %s", self._host, k, str(rcs[k]), str(v))
            if not rcs[k] == v:
                logger.info(short + "failed, mismatching version information in rcs_block", self._host)
                return False
        logger.info(short + "passed", self._host)
        return True

    def test_sys_clk_incr(self):
        """Test if system clock is running
        In case of failure:
          - check 2048MHz clock input to R2DBE"""
        short = "%s system clock test "
        cnt1 = self._klc.read_uint("sys_clkcounter")
        logger.debug("%s cnt1 = %d", self._host, cnt1)
        time.sleep(0.1)
        cnt2 = self._klc.read_uint("sys_clkcounter")
        logger.debug("%s cnt2 = %d (0.1s later)", self._host, cnt2)
        result = not (cnt1 == cnt2)
        logger.info(short + ("passed" if result else "failed"), self.host)
        return result

    def test_time_sync(self, micro=900000):
        """Test timestamp in VDIF output and compare to real-time
        In case of failure:
          - check NTP on control computer
          - reconfigure R2DBE"""
        assert isinstance(micro, int) and micro > 600000, "micro should be an integer greater than 600000"
        short = "%s time sync test "
        offsets = self.get_vdif_vs_sys_time_offset(micro=micro)
        result = []
        for ch,off in list(offsets.items()):
            if not off == 0:
                logger.debug("%s r2dbe ch %d and after should be equal at 1-second resolution", self._host, ch)
                result.append(False)
        result = all(result)
        logger.info(short + ("passed" if result else "failed"), self._host)
        return result

    def get_vdif_vs_sys_time_offset(self, micro=900000):
        result = {}
        self._wait_until_mid_second()
        before = datetime.datetime.utcnow()
        sec = self.get_vdif_seconds()
        after = datetime.datetime.utcnow()
        logger.debug("%s     before: %s", self._host, str(before))
        logger.debug("%s      after: %s", self._host, str(after))
        if not before.second == after.second or after.microsecond >= micro:
            logger.debug("%s before and after seconds should be equal, and after should be less than %dus into current second", self._host, micro)
            logger.info(short + "failed, took too long", self._host)
            return result
        for ch in list(self._channels.keys()):
            ep = self.get_ref_epoch(ch)
            vt = vdif.VDIFTime(ep, sec).to_datetime()
            logger.debug("%s r2dbe ch %d: %s", self._host, ch, str(vt))
            result[ch] = int(vt.strftime("%s")) - int(after.strftime("%s"))
        return result

    def test_pps(self, pps_wait=10):
        """Test PPS signal input, exactly N counts should be registered after N seconds
        In case of failure:
          - check PPS input connected to hb port on R2DBE
          - check PPS signal on oscilloscope / counter"""
        assert isinstance(pps_wait, int) and pps_wait > 0, "pps_wait should be a positive integer"
        self._wait_until_mid_second()
        before = time.time()
        pps_before = self.get_gps_pps_count()
        time.sleep(pps_wait - 0.5)
        self._wait_until_mid_second()
        pps_after = self.get_gps_pps_count()
        after = time.time()
        logger.debug("%s gps_pps_count at %.3f: %d", self._host, before, pps_before)
        logger.debug("%s gps_pps_count at %.3f: %d", self._host, after, pps_after)
        logger.debug("%s count difference %d, expect %d", self._host, pps_after - pps_before, pps_wait)
        if not pps_after - pps_before == pps_wait:
            logger.info("%s PPS test failed", self._host)
            return False
        logger.info("%s PPS test passed", self._host)
        return True

    def test_login_root(self):
        """Test if batch-mode login works
        In case of failure:
          - call support"""
        return common.remote_login_test(self._host, self._root)

    def test_ntp(self, max_offset=100.0, sys_peer="192.168.0.1"):
        """Test if NTP is properly sync'ed
        In case of failure:
          - as root on R2DBE try:
              service ntp stop
              ntpdate 192.168.0.1
              service ntp start
          - check /etc/ntp.conf on R2DBE PowerPC (should have 'server 192.168.0.1 iburst' line)
          - start NTP server on R2DBE PowerPC, 'ssh root@<r2dbe-hostname> "service ntp start"'
          - check and debug list of peers on R2DBE PowerPC, 'ssh root@<r2dbe-hostname> "ntpq -p"'"""
        return common.ntp_test(user_host=(self._root, self._host), max_offset=max_offset,
          sys_peer=sys_peer, cmd_is_ntp_running="service ntp status")

    def test_system_time_utc(self):
        """Test if system time is set to UTC
        In case of failure:
          - on R2DBE PowerPC, 'dpkg-reconfigure tzdata' and set to UTC"""
        return common.system_time_utc_test(user_host=(self._root, self._host))

    def _get_r2daemon_pid(self, name=None):
        extra = "--r2dbe %s --redis" % name if name else ""
        cmd = "\"pgrep -f 'python /root/vlbicontrol/r2daemon/r2daemon.py %s'\"" % extra
        rc, out, err = common.subp_remote_system_call(cmd, self._root, self._host)
        if rc == 1:
            return []
        # something else went wrong
        if not rc == 0:
            raise RuntimeError("%s call '%s' failed [returned %d: %s]" % (self._host, cmd, rc, err))
        return out.splitlines()

    def test_r2daemon_running(self):
        """Test if r2daemon is running on PowerPC (started during R2DBE configuring, required for monitoring)
        In case of failure:
          - check if redis server running on CC and bound to CC IP address on VLBI network
          - reconfigure R2DBE
          - on R2DBE PowerPC, try 'nohup r2daemon <r2dbe-devicename> 1>/dev/null 2>/dev/null &'"""
        # first check number daemons irrespective of r2dbe name
        pids = self._get_r2daemon_pid()
        logger.debug("%s found %d instances of r2daemon (should be exactly 1), PIDs are [%s]", self._host, len(pids), ", ".join(pids))
        result = len(pids) == 1
        # then check if the one daemon has correct name
        if result:
            pids = self._get_r2daemon_pid(name=self._name)
            logger.debug("%s found %d instances of r2daemon with name '%s' (should be exactly 1), PIDs are [%s]", self._host, len(pids), self._name, ", ".join(pids))
            result = len(pids) == 1
        logger.info("%s r2daemon running test " + ("passed" if result else "failed"), self._host)
        return result

### miscellaneous
    def start_r2daemon(self):
        # kill first any r2daemon instances (irrespective of name)
        self.kill_r2daemon()
        #~ if self._get_r2daemon_pid():
            #~ logger.debug("%s r2daemon already running, use kill_r2daemon first if necessary", self._host)
            #~ return True
        remote_stdout = "r2daemon.out"
        remote_stderr = "r2daemon.err"
        cmd = "'nohup r2daemon %s >%s 2>%s </dev/null &'" % (self.name, remote_stdout, remote_stderr)
        rc, out, err = common.subp_remote_system_call(cmd, self._root, self._host)
        if not rc == 0:
            logger.error("failed running r2daemon on %s, trying to read stderr / stdout ... ", self._host)
            cmd = "cat %s" % remote_stdout
            rc, out, err = common.subp_remote_system_call(cmd, self._root, self._host)
            if rc == 0:
                logger.error(" ... stdout from failed r2deamon call on %s is: %s", self._host, out)
            cmd = "cat %s" % remote_stdout
            rc, out, err = common.subp_remote_system_call(cmd, self._root, self._host)
            if rc == 0:
                logger.error(" ... stderr from failed r2deamon call on %s is: %s", self._host, out)
        return True

    def kill_r2daemon(self, name=None):
        pids = self._get_r2daemon_pid(name=name)
        for pid in pids:
            cmd = "kill %s" % pid
            rc, out, err = common.subp_remote_system_call(cmd, self._root, self._host)
            if not rc == 0:
                logger.debug("%s kill returned %d [%s, %s]", self._host, rc, out, err)
        return len(self._get_r2daemon_pid(name=name)) == 0

### adc functions
    def get_core_offsets(self, ch):
        return adc.get_core_offsets(self._klc, ch)

    def set_core_offsets(self, ch, offsets):
        adc.set_core_offsets(self._klc, ch, offsets)

    def adj_core_offsets(self, ch, offsets):
        adc.adj_core_offsets(self._klc, ch, offsets)

    def get_core_gains(self, ch):
        return adc.get_core_gains(self._klc, ch)

    def set_core_gains(self, ch, gains):
        adc.set_core_gains(self._klc, ch, gains)

    def adj_core_gains(self, ch, gains):
        adc.adj_core_gains(self._klc, ch, gains)

    _adc_core_file_pattern = "adc-qc-cal.{host}.{ch}.{og}"
    def _write_adc_core_result_to_file(self, ch, offset_or_gain, values):
        assert offset_or_gain in ["offset","gain"]
        filename = pathlib.os.sep.join([str(pathlib.Path.home()),"log",self._adc_core_file_pattern.format(host=self.host,ch=ch,og=offset_or_gain)])
        vstr = ",".join(["%.2f" % v for v in values])
        try:
            logger.debug("writing %s values [%s] for %s adc %d", offset_or_gain, vstr, self.host, ch)
            with open(filename,"w") as fh:
                fh.write("# %s\n" % datetime.datetime.utcnow().strftime('%Yy%jd%Hh%Mm%Ss'))
                fh.write("%s\n" % vstr)
        except FileNotFoundError as fnfe:
            logger.debug("FileNotFoundError: %s", str(fnfe))

    def _read_adc_core_result_from_file(self, ch, offset_or_gain):
        assert offset_or_gain in ["offset","gain"]
        filename = pathlib.os.sep.join([str(pathlib.Path.home()),"log",self._adc_core_file_pattern.format(host=self.host,ch=ch,og=offset_or_gain)])
        values = None
        try:
            logger.debug("reading %s values for %s adc %d", offset_or_gain, self.host, ch)
            with open(filename,"r") as fh:
                for line in fh.readlines():
                    comment_start = line.find('#')
                    if comment_start >= 0:
                        line = line[:comment_start]
                    vstrs = line.split(",")
                    if len(vstrs) != 4:
                        continue
                    try:
                        values = [float(vs) for vs in vstrs]
                        # accept first valid value array
                        break
                    except ValueError:
                        logger.debug("Value error: encountered invalid value array %s", vstrs)
        except FileNotFoundError as fnfe:
            logger.debug("FileNotFoundError: %s", str(fnfe))
        if values == None:
            logger.debug("could not read value array from file, using defaults")
            values = [0.0,]*4
        return values

    def calibrate_adc_cores(self, ch, gain_scale=0.5, max_iter=10,
      oconv=0.2, gconv=0.25, wait=2, adjust_scale=0.6):
        # mean and variance functions
        cmean = lambda c, s: [[1.0*sum([c3 * s[i] for i, c3 in enumerate(c2)])/sum(c2) for c2 in c1] for c1 in c]
        cvar = lambda c, s, m: [[1.0*sum([c3 * (s[i] - m[k][j])**2 for i, c3 in enumerate(c2)])/sum(c2) for j, c2 in enumerate(c1)] for k, c1 in enumerate(c)]
        #-- reset actuals and initialise references
        #   Attempt to read values logged to file, if no values are read from file the default is all zeros
        ref_core_offsets = self._read_adc_core_result_from_file(ch, "offset")
        self.set_core_offsets(ch, ref_core_offsets)
        ref_core_gains = self._read_adc_core_result_from_file(ch, "gain")
        self.set_core_gains(ch, ref_core_gains)
        # first set offsets
        num_iter = 0
        warned_about_using_reference = False
        while True:
            time.sleep(wait)
            count_dict = self.get_8bit_counts(ch)
            counts = count_dict["counts"]
            states = count_dict["states"]
            # compute only over last 1 second
            mean = cmean(counts[-1:], states)[0]
            logger.debug("      means: [%s]", ", ".join(["%+06.2f" % g for g in mean]))
            # calculate adjustment
            adjust = [-adjust_scale*m for m in mean]
            logger.debug("     adjust: [%s]", ", ".join(["%+06.2f" % g for g in adjust]))
            if max([abs(a) for a in adjust]) < oconv:
                logger.debug("max(adj) < %.3f, offset converged adc %d on %s" % (oconv, ch, self._host))
                # write a good solution to file
                self._write_adc_core_result_to_file(ch, "offset", ref_core_offsets)
                break
            # get the current offset values, but if they are bogus, use reference instead
            old_core_offsets = self.get_core_offsets(ch)
            logger.debug("old offsets: [%s]", ", ".join(["%+06.2f" % g for g in old_core_offsets]))
            logger.debug("ref offsets: [%s]", ", ".join(["%+06.2f" % g for g in ref_core_offsets]))
            if not all([abs(rr-oo) < adc.OFFSET_LSB_STEP for rr,oo in zip(ref_core_offsets,old_core_offsets)]):
                logger.debug("dif offsets: [%s]", ", ".join(["%+06.2f" % abs(rr-oo) for rr,oo in zip(ref_core_offsets,old_core_offsets)]))
                if not warned_about_using_reference:
                    warned_about_using_reference = True
                    logger.warning("reference and old offsets do not match, using reference as base for adjustment")
                # ignore the read values, use reference
                old_core_offsets = [rr for rr in ref_core_offsets]
            else:
                # set ref to read values, to avoid divergence
                ref_core_offsets = [oo for oo in old_core_offsets]
            # calculate new offsets
            new_core_offsets = [oo + aa for oo,aa in zip(old_core_offsets,adjust)]
            logger.debug("new offsets: [%s]", ", ".join(["%+06.2f" % g for g in new_core_offsets]))
            self.set_core_offsets(ch, new_core_offsets)
            # remember to udpate the reference offsets too, and limit to valid range
            for ii,oo in enumerate(new_core_offsets):
                if oo*adc.OFFSET_LSB_TO_MV_MULTIPLIER > adc.OFFSET_MV_MAX:
                    ref_core_offsets[ii] = adc.OFFSET_MV_MAX/adc.OFFSET_LSB_TO_MV_MULTIPLIER
                elif oo*adc.OFFSET_LSB_TO_MV_MULTIPLIER < adc.OFFSET_MV_MIN:
                    ref_core_offsets[ii] = adc.OFFSET_MV_MIN/adc.OFFSET_LSB_TO_MV_MULTIPLIER
                else:
                    ref_core_offsets[ii] = oo
            num_iter += 1
            if num_iter > max_iter:
                logger.warn("ADC core calibration num_iter > max_iter, offset not converged adc %d on %s" % (ch, self._host))
                break
        # then set gains
        num_iter = 0
        warned_about_using_reference = False
        while True:
            time.sleep(wait)
            count_dict = self.get_8bit_counts(ch)
            counts = count_dict["counts"]
            states = count_dict["states"]
            # compute only over last 1 second
            mean = cmean(counts[-1:], states)[0]
            var = cvar(counts[-1:], states, [mean,])[0]
            std = [v**0.5 for v in var]
            logger.debug("      std: [%s]", ", ".join(["%+06.2f" % g for g in std]))
            std_ref = sum(std)/4.0
            if std_ref < gconv:
                logger.warn("ADC core calibration std_ref < gconv, input signal power may be too low, aborting gain calibration adc %d on %s" % (ch, self._host))
                break
            adjust = [-adjust_scale*gain_scale*100.0*(s - std_ref)/std_ref for s in std]
            logger.debug("   adjust: [%s]", ", ".join(["%+06.2f" % g for g in adjust]))
            if max([abs(a) for a in adjust]) < gconv:
                logger.debug("max(adj) < %.3f, gain converged adc %d on %s" % (gconv, ch, self._host))
                # write a good solution to file
                self._write_adc_core_result_to_file(ch, "gain", ref_core_gains)
                break
            # get the current gain values, but if they are bogus, use reference instead
            old_core_gains = self.get_core_gains(ch)
            logger.debug("old gains: [%s]", ", ".join(["%+06.2f" % g for g in old_core_gains]))
            logger.debug("ref gains: [%s]", ", ".join(["%+06.2f" % g for g in ref_core_gains]))
            if not all([abs(rr-oo) < adc.GAIN_PER_STEP for rr,oo in zip(ref_core_gains,old_core_gains)]):
                logger.debug("dif gains: [%s] >?> %.2f", ", ".join(["%+06.2f" % abs(rr-oo) for rr,oo in zip(ref_core_gains,old_core_gains)]), adc.GAIN_PER_STEP)
                if not warned_about_using_reference:
                    warned_about_using_reference = True
                    logger.warning("reference and old gains do not match, using reference as base for adjustment")
                # ignore the read values, use reference
                old_core_gains = [rr for rr in ref_core_gains]
            else:
                # set ref to read values, to avoid divergence
                ref_core_gains = [oo for oo in old_core_gains]
            # calculate new gains
            new_core_gains = [oo + aa for oo,aa in zip(old_core_gains,adjust)]
            logger.debug("new gains: [%s]", ", ".join(["%+06.2f" % g for g in new_core_gains]))
            self.set_core_gains(ch, new_core_gains)
            # remember to udpate the reference gains too, and limit to valid range
            for ii,nn in enumerate(new_core_gains):
                if nn > adc.GAIN_PER_MAX:
                    ref_core_gains[ii] = adc.GAIN_PER_MAX
                elif nn < adc.GAIN_PER_MIN:
                    ref_core_gains[ii] = adc.GAIN_PER_MIN
                else:
                    ref_core_gains[ii] = nn
            num_iter += 1
            if num_iter > max_iter:
                logger.warn("ADC core calibration num_iter > max_iter, gain not converged adc %d on %s" % (ch, self._host))
                break
        # successful unless something breaks
        return True

    def calibrate_adc_interface(self, ch):
        result = []
        # store current data source
        src = self.get_data_source(ch)
        self.set_data_source(ch, "adc")
        # claim snapshot
        lock_name, lock_value = self._claim_snapshot(timeout=60)
        # perform calibration
        adc5g.set_test_mode(self._klc, ch)
        adc5g.sync_adc(self._klc)
        opt, glitches = adc5g.calibrate_mmcm_phase(self._klc, ch, ["r2dbe_snap_8bit_%d_data" % ch], wait_period=0.001)
        if opt is None:
            pretty = adc5g.pretty_glitch_profile(-1, glitches)
            logger.debug("%s ADC %d found no optimal phase, glitches in range (%d, %d): [%s]", self._host, ch, min(glitches), max(glitches), pretty)
            logger.warn("%s ADC interface calibration failed for channel %d", self._host, ch)
            result.append(False)
        else:
            pretty = adc5g.pretty_glitch_profile(opt, glitches)
            logger.debug("%s ADC %d optimal phase: %d [%s]", self._host, ch, opt, pretty)
            result.append(True)
        adc5g.unset_test_mode(self._klc, ch)
        # release snapshot
        self._yield_snapshot(lock_name, lock_value)
        # restore data source
        self.set_data_source(ch, src)
        return all(result)

### get-set methods
    def get_uptime(self):
        return self._get_decode_param("uptime")

    def get_gps_pps_count(self):
        return self._get_decode_param("gps_pps_count")

    def get_gps_pps_period(self):
        return self._get_decode_param("gps_pps_period")

    def get_maser_pps_count(self):
        return self._get_decode_param("maser_pps_count")

    def get_maser_pps_period(self):
        return self._get_decode_param("maser_pps_period")

    def get_pps_offset(self):
        return self._get_decode_param("pps_offset")

    def get_vdif_seconds(self):
        return self._get_decode_param("vdif_seconds")

    def get_8bit_data(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("8bit_data_%d" % ch)

    def get_2bit_data(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("2bit_data_%d" % ch)

    def get_2bit_counts(self, ch, num_snap=4):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))

        x = []
        for n in range(num_snap):
            x += self.get_2bit_data(ch)
        result = dict({"states": [-2, -1, 0, 1]})
        result.update({"counts": [len([xx for xx in x if xx == s]) for s in result["states"]]})
        return result

    def recommend_power_adjustment_dB(self, ch, wait_nmsec=10):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(wait_nmsec, int) and \
          wait_nmsec > 0 and wait_nmsec < 16384, "wait_nmsec should be a positive integer less than 16384"

        time.sleep(wait_nmsec * 0.001)
        pwr_samp = self.get_8bit_power(ch)["power"][-wait_nmsec:]
        avg_pwr = 1.0*sum(pwr_samp)/len(pwr_samp)
        target = data.scale_8bit_rms_to_int_power(32)
        adj = 10*math.log10(target / avg_pwr)
        return adj

    def get_8bit_power(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("8bit_power_%d" % ch)

    def _check_8bit_counts_invalid(self, c):
        return any([any([any([c[i][j][k] == -1 \
          for k in range(256)]) \
          for j in range(4)]) \
          for i in range(len(c))])

    def get_8bit_counts(self, ch, retry_on_invalid=True):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        # check for -1 entries, and if found, try reading again
        c = self._get_decode_param("8bit_counts_%d" % ch)
        while retry_on_invalid and self._check_8bit_counts_invalid(c["counts"]):
            c = self._get_decode_param("8bit_counts_%d" % ch)
        return c

    def get_data_source(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("data_source_%d" % ch)

    def set_data_source(self, ch, source):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(source, str) and \
          source in data.DATA_SOURCE, "source should be a string from the set {'%s'}" % ("', '".join(data.DATA_SOURCE))
        return self._encode_set_param("data_source_%d" % ch, source)

    def get_delay(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("delay_%d" % ch)

    def set_delay(self, ch, delay):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._encode_set_param("delay_%d" % ch, delay)

    def get_2bit_threshold(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("2bit_th_%d" % ch)

    def set_2bit_threshold(self, ch, level=None, outer_frac=0.16):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        if not level:
            count_dict = self.get_8bit_counts(ch)
            counts = count_dict["counts"]
            states = count_dict["states"]
            d1, d2, d3 = len(counts), len(counts[0]), len(counts[0][0])
            counts = [sum([counts[-1][i][j] for i in range(d2)]) for j in range(d3)]
            cdf = [1.0*sum(counts[:n])/sum(counts) for n in range(1, d3+1)]
            th_n = states[[c > outer_frac for c in cdf].index(True)]
            th_p = states[[c < 1.0 - outer_frac for c in cdf].index(False)]
            level = int(round((abs(th_p) + abs(th_n))/2.0))
        return self._encode_set_param("2bit_th_%d" % ch, level)

    def get_mac(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("mac_%d" % ch)

    def set_mac(self, ch, mac):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(mac, str) and \
          len(mac.split(":")) == 6 and \
          all(len(h) == 2 for h in mac.split(":")) and \
          all(c in string.hexdigits for c in "".join(mac.split(":"))), "mac should be a string in colon-hexidecimal notation"
        return self._encode_set_param("mac_%d" % ch, mac)

    def get_ip(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("ip_%d" % ch)

    def set_ip(self, ch, ip):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(ip, str) and \
          len(ip.split(".")) == 4 and \
          all(c in string.digits for c in "".join(ip.split("."))) and \
          all(int(n) < 256 and int(n) >= 0 for n in ip.split(".")), "ip should be a string in dot-decimal notation"
        return self._encode_set_param("ip_%d" % ch, ip)

    def get_port(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("port_%d" % ch)

    def set_port(self, ch, port):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(port, int) and \
          port >= 0 and port < 2**16, "port should be 16-bit unsigned integer"
        return self._encode_set_param("port_%d" % ch, port)

    def get_dest_ip(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("dest_ip_%d" % ch)

    def set_dest_ip(self, ch, ip):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(ip, str) and \
          len(ip.split(".")) == 4 and \
          all(c in string.digits for c in "".join(ip.split("."))) and \
          all(int(n) < 256 and int(n) >= 0 for n in ip.split(".")), "ip should be a string in dot-decimal notation"
        return self._encode_set_param("dest_ip_%d" % ch, ip)

    def get_dest_port(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("dest_port_%d" % ch)

    def set_dest_port(self, ch, port):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(port, int) and \
          port >= 0 and port < 2**16, "port should be 16-bit unsigned integer"
        return self._encode_set_param("dest_port_%d" % ch, port)

    def get_dest_mac(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))

        arp_index = int(self.get_dest_ip(ch).split(".")[-1])
        return self._get_decode_param("arp_%d" % ch)[arp_index]

    def set_dest_mac(self, ch, mac):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(mac, str) and \
          len(mac.split(":")) == 6 and \
          all(len(h) == 2 for h in mac.split(":")) and \
          all(c in string.hexdigits for c in "".join(mac.split(":"))), "mac should be a string in colon-hexidecimal notation"

        arp_index = int(self.get_dest_ip(ch).split(".")[-1])
        arp = self._get_decode_param("arp_%d" % ch)
        arp[arp_index] = mac
        return self._encode_set_param("arp_%d" % ch, arp)

    def get_ref_sec(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("ref_sec_%d" % ch)

    def set_ref_sec(self, ch, sec):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(sec, int) and \
          sec >= 0 and sec < 2**30, "sec should be an unsigned 30-bit integer"
        return self._encode_set_param("ref_sec_%d" % ch, sec)

    def get_ref_epoch(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("ref_epoch_%d" % ch)

    def set_ref_epoch(self, ch, refep):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(refep, int) and \
          refep >= 0 and refep < 64, "refep should be a 6-bit unsigned integer"
        return self._encode_set_param("ref_epoch_%d" % ch, refep)

    def get_station(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("station_%d" % ch)

    def set_station(self, ch, station):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(station, str) and \
          len(station) == 2, "station should be a two-character string"
        return self._encode_set_param("station_%d" % ch, station)

    def get_thread(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("thread_%d" % ch)

    def set_thread(self, ch, thread):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(thread, int) and \
          thread >= 0 and thread < 2**10, "thread should be an unsigned 10-bit integer"
        return self._encode_set_param("thread_%d" % ch, thread)

    def get_pol(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("pol_%d" % ch)

    def set_pol(self, ch, pol):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(pol, str) and \
          pol in data.POL, "pol should be a string from the set {'%s'}" % ("', '".join(data.POL))
        return self._encode_set_param("pol_%d" % ch, pol)

    def get_bdc_ch(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("bdc_ch_%d" % ch)

    def set_bdc_ch(self, ch, bdc_chan):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(bdc_chan, str) and \
          bdc_chan in data.BDC_CH, "bdc_chan should be a string from the set {'%s'}" % ("', '".join(data.BDC_CH))
        return self._encode_set_param("bdc_ch_%d" % ch, bdc_chan)

    def get_rx_sb(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("rx_sb_%d" % ch)

    def set_rx_sb(self, ch, rx_sideband):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(rx_sideband, str) and \
          rx_sideband in data.RX_SB, "rx_sideband should be a string from the set {'%s'}" % ("', '".join(data.RX_SB))
        return self._encode_set_param("rx_sb_%d" % ch, rx_sideband)

    def get_eud_vers(self, ch):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        return self._get_decode_param("eud_vers_%d" % ch)

    def set_eud_vers(self, ch, eud_vers):
        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
        assert isinstance(eud_vers, int) and \
          eud_vers < 256 and eud_vers >= 0, "eud_vers should be an integer in range [0,256)"
        return self._encode_set_param("eud_vers_%d" % ch, eud_vers)
### hidden
    def __init__(self, redishost="localhost", redisport=6379, **kwargs):
        assert all(p in list(kwargs.keys()) for p in self.PARAMS), "kwargs has to include the following keys for %s:\n  - %s" % (self.__class__.__name__, "\n  - ".join(self.PARAMS))
        for k, v in list(kwargs.items()):
            setattr(self, "_" + k, v)
        for ch, d in list(self._channels.items()):
            for k in self.CH_PARAMS:
                assert k in list(d.keys()), "channel %d missing key %s" % (ch, k)
        self._redis = redis.Redis(host=redishost, port=redisport)
        self._klc = katcplight.KLClient(self._host)

    def __str__(self):
        return self._host

    def _claim_snapshot(self, timeout=10):
        lock_name = "%s.lock.snap" % self._name
        lock_value = b"%8x" % random.randint(0,2**32-1)
        # write unique value that expires after few seconds
        # call returns True only if key does not exist
        while not self._redis.set(lock_name, lock_value, ex=timeout, nx=True):
            time.sleep(0.05)
        # caller will need lock name / value to unlock afterwards
        return lock_name, lock_value

    def _yield_snapshot(self, lock_name, lock_value):
        # only delete lock if it matches value
        if self._redis.get(lock_name) == lock_value:
            self._redis.delete(lock_name)

    def _get_decode_param(self, name):
        entry = data.lookup_param(name)
        if "snap" in list(entry.keys()):
            lock_name, lock_value = self._claim_snapshot()
            if not self._klc.snapshot_arm_and_wait(entry["dev"], man_trig=True):
                raise RuntimeError("snap '%s' failed to trigger for %s" % (entry["dev"], self._host))
            raw = self._klc.read(entry["snap"]["data"], entry["size"], offset=entry["offset"])
            self._yield_snapshot(lock_name, lock_value)
            return data.decode_param(name, raw)
        raw = self._klc.read(entry["dev"], entry["size"], offset=entry["offset"])
        return data.decode_param(name, raw)

    def _encode_set_param(self, name, value):
        result = data.encode_param(name, value)
        if "mask" in list(result.keys()):
            old = self._klc.read(result["dev"], result["size"], offset=result["offset"])
            result["data"] = data.mask_param(name, result["data"], old)
        try:
            self._klc.write(result["dev"], result["data"], offset=result["offset"])
        except katcplight.KLClientError as klce:
            logger.error("%s failed to write to device '%s': '%s'", self._host, result["dev"], klce)
            return False
        return True

    def _wait_until_mid_second(self, tol=50000, wait=0.025):
        while abs(datetime.datetime.utcnow().microsecond - 500000) > 50000:
            time.sleep(0.025)
        logger.debug("%s mid-second time is %.6f", self._host, time.time())
