import math
import socket
import struct
import numpy as np
#~ import sys

from .. import common

#~ import logging
#~ logger = logging.getLogger(__name__)

### miscellaneous constants
CH = (0, 1)
POL = ("lcp", "rcp")
BDC_CH = ("low", "high")
RX_SB = ("lsb", "usb")
DATA_SOURCE = ("zero", "adc", "noise", "tone")

### r2dbe_monitor?_power
def scale_8bit_rms_to_int_power(x):
    return x**2 * 4096000.0 / 16.0

def scale_int_power_to_8bit_rms(x):
    return (x * 16.0 / 4096000.0)**0.5

def unpack_8bit_power(raw_data):
    assert not len(raw_data) % 8, "raw_data length should be multiple of 8"

    n = len(raw_data) // 8
    Q = struct.unpack(">%dQ" % n, raw_data)
    ts, pwr = [-1.0,]*n, [-1.0,]*n
    for i, q in enumerate(Q):
        msec = q & 0x3ff
        sec = (q >> 10) & 0x3fffff
        ts[i] = msec/1000.0 + sec
        pwr[i] = (q >> 32) & 0xffffffff
    ts, pwr = list(zip(*sorted(zip(ts, pwr), key=lambda k: k[0])))

    return {"time": ts, "power": pwr}

### r2dbe_monitor?_count
def unpack_8bit_counts(raw_data):
    assert not len(raw_data) % 8192, "raw_data length should be multiple of 8192"

    core_order = [0, 2, 1, 3]
    N = len(raw_data) // 8192
    n = len(raw_data) // 8
    Q = list(struct.unpack(">%dQ" % n, raw_data))
    # first determine all timestamps
    ts_all = [[[-1.0,]*256 for j in range(4)] for i in range(N)]
    # loop over data blocks
    enum = enumerate(Q)
    for i in range(N):
        # loop over number of cores
        for j in core_order:
            # loop over 8bit values
            for k in range(256):
                q = enum.__next__()[1]
                ts_all[i][j][k] = (q >> 10) & 0x3fffff
    # create sorted list of unique timestamps
    # flatten first
    ts = [ttt for t in ts_all for tt in t for ttt in tt]
    ts = sorted(list(set(ts)))
    # now determine counts
    val = list(range(-128,128))
    cnt = [[[-1.0,]*256 for j in range(4)] for i in range(N)]
    # loop over data blocks
    enum = enumerate(Q)
    for ii in range(N):
        # loop over core-order
        for j in core_order:
            # loop over 8bit values
            for kk in range(256):
                i = ts.index(ts_all[ii][j][k])
                k = val.index(kk if kk < 128 else kk - 256)
                q = enum.__next__()[1]
                try:
                    cnt[i][j][k] = (q >> 32) & 0x3fffffff
                except IndexError as ie:
                    # Do nothing, this simply means there is a currently-incomplete batch
                    # of counts at the latest timestamp, and an incomplete set of counts
                    # at the earliest timestamp (indicated by -1 entries). The caller
                    # should check for -1 if necessary and handle it appropriately.
                    pass
                    #~ f = sys._getframe()
                    #~ logger.error("%s.%s(%d): %s. i=%d, j=%d, k=%d, q=%d, ts = [%s]", __name__, f.f_code.co_name, f.f_lineno, ie, i, j, k, q, ", ".join(str(_t) for _t in ts))

    return {"time": ts, "states": val, "counts": cnt}

### r2dbe_snap_?bit_?_data
def unpack_2bit_data(raw_data):
    assert not len(raw_data) % 4, "raw_data length should be multiple of 4"

    n = len(raw_data) // 4
    X = struct.unpack(">%dI" % n, raw_data)

    ##-- orig
    #N = 16 * n
    #d = [0,]*N
    #for i, x in enumerate(X):
    #    d[i*16:i*16+16] = [((x >> (30 - 2*k)) & 0x3) for k in range(16)]
    #d = [dd - 2 for dd in d]

    #-- vectorize
    k = 30 - 2*np.arange(16)
    d = np.array(X, dtype=np.uint32)
    d = np.repeat(d, 16).reshape([-1, 16])
    d = np.right_shift(d, k)
    d = np.bitwise_and(d, 0x3) - 2
    d = d.reshape([-1])

    return d

def unpack_8bit_data(raw_data):
    assert not len(raw_data) % 4, "raw_data length should be multiple of 4"
    n = len(raw_data)
    d = struct.unpack(">%db" % n, raw_data)
    return np.array(d, dtype=np.int8)

### tengbe_?_core
def unpack_tengbe_data(raw_data):
    assert len(raw_data) == 16384, "raw_data length should be exactly 16384"

    mac, _, _, _, _, _, _, _, en, port = struct.unpack(">QIIIIIIBBH", raw_data[:36])
    arp = struct.unpack(">256Q", raw_data[0x3000:0x3800])
    mac2str = lambda m: ":".join([("%012x" % m)[i:i+2] for i in range(0,12,2)])
    result = {
      "arp": [mac2str(a) for a in arp],
      "port": port,
      "ip": socket.inet_ntoa(raw_data[16:20]),
      "mac": mac2str(mac),
      "enabled": (en & 0x01) == 1,
      "gateway": socket.inet_ntoa(raw_data[12:16]),
    }

    return result

### parameter table
_dec_H = lambda x: struct.unpack(">H", x)[0]
_dec_i = lambda x: struct.unpack(">i", x)[0]
_dec_I = lambda x: struct.unpack(">I", x)[0]
_dec_Q = lambda x: struct.unpack(">Q", x)[0]
_enc_H = lambda x: struct.pack(">H", x)
_enc_i = lambda x: struct.pack(">i", x)
_enc_I = lambda x: struct.pack(">I", x)
_enc_Q = lambda x: struct.pack(">Q", x)
_PARAM_TABLE = {
  "r2dbe_onepps_alive": {
    "uptime": {
      "decode": _dec_I,
      "offset": 0,
      "size": 4,
    },
  },
  "r2dbe_onepps_gps_pps_cnt": {
    "gps_pps_count": {
      "decode": _dec_I,
      "offset": 0,
      "size": 4,
    },
  },
  "r2dbe_onepps_gps_pps_per": {
    "gps_pps_period": {
      "decode": _dec_I,
      "offset": 0,
      "size": 4,
    },
  },
  "r2dbe_onepps_msr_pps_cnt": {
    "maser_pps_count": {
      "decode": _dec_I,
      "offset": 0,
      "size": 4,
    },
  },
  "r2dbe_onepps_msr_pps_per": {
    "maser_pps_period": {
      "decode": _dec_I,
      "offset": 0,
      "size": 4,
    },
  },
  "r2dbe_onepps_offset": {
    "pps_offset": {
      "decode": _dec_i,
      "offset": 0,
      "size": 4,
    },
  },
  "r2dbe_onepps_since_epoch": {
    "vdif_seconds": {
      "decode": _dec_I,
      "offset": 0,
      "size": 4,
    },
  },
}
for c in CH:
    _PARAM_TABLE.update({
      "r2dbe_snap_8bit_%d_data" % c: {
        "8bit_data_%d" % c: {
          "decode": unpack_8bit_data,
          "offset": 0,
          "size": 262144,
          "snap": {
            "ctrl": "r2dbe_snap_8bit_%d_data_ctrl" % c,
            "data": "r2dbe_snap_8bit_%d_data_bram" % c,
          },
        },
      },
      "r2dbe_snap_2bit_%d_data" % c: {
        "2bit_data_%d" % c: {
          "decode": unpack_2bit_data,
          "offset": 0,
          "size": 65536,
          "snap": {
            "ctrl": "r2dbe_snap_2bit_%d_data_ctrl" % c,
            "data": "r2dbe_snap_2bit_%d_data_bram" % c,
          },
        },
      },
      "r2dbe_monitor%d_power" % c: {
        "8bit_power_%d" % c: {
          "decode": unpack_8bit_power,
          "offset": 0,
          "size": 131072,
        },
      },
      "r2dbe_monitor%d_counts" % c: {
        "8bit_counts_%d" % c: {
          "decode": unpack_8bit_counts,
          "offset": 0,
          "size": 131072,
        },
      },
      "r2dbe_data_mux_%d_sel" % c: {
        "data_source_%d" % c: {
          "decode": lambda x: DATA_SOURCE[_dec_I(x)],
          "encode": lambda x: _enc_I(DATA_SOURCE.index(x)),
          "offset": 0,
          "size": 4,
        },
      },
      "r2dbe_delay_%d_samples" % c: {
        "delay_%d" % c: {
          "decode": _dec_I,
          "encode": _enc_I,
          "offset": 0,
          "size": 4,
        },
      },
      "r2dbe_quantize_%d_thresh" % c: {
        "2bit_th_%d" % c: {
          "decode": lambda x: _dec_I(x) & 0x7f,
          "encode": lambda x: _enc_I(x & 0x7f),
          "offset": 0,
          "size": 4,
        },
      },
      "r2dbe_tengbe_%d_core" % c: {
        "mac_%d" % c: {
          "decode": lambda x: common.int2mac(_dec_Q(x)),
          "encode": lambda x: _enc_Q(common.mac2int(x)),
          "offset": 0,
          "size": 8,
        },
        "ip_%d" % c: {
          "decode": socket.inet_ntoa,
          "encode": socket.inet_aton,
          "offset": 16,
          "size": 4,
        },
        "port_%d" % c: {
          "decode": _dec_H,
          "encode": _enc_H,
          "offset": 34,
          "size": 2,
        },
        "arp_%d" % c: {
          "decode": lambda x: [common.int2mac(a) for a in struct.unpack(">256Q",x)],
          "encode": lambda x: struct.pack(">256Q",*[common.mac2int(a) for a in x]),
          "offset": 0x3000,
          "size": 0x800,
        },
      },
      "r2dbe_tengbe_%d_dest_ip" % c: {
        "dest_ip_%d" % c: {
          "decode": socket.inet_ntoa,
          "encode": socket.inet_aton,
          "offset": 0,
          "size": 4,
        },
      },
      "r2dbe_tengbe_%d_dest_port" % c: {
        "dest_port_%d" % c: {
          "decode": _dec_I,
          "encode": _enc_I,
          "offset": 0,
          "size": 4,
        },
      },
      "r2dbe_vdif_%d_hdr_w0_sec_ref_ep" % c: {
        "ref_sec_%d" % c: {
          "decode": _dec_I,
          "encode": _enc_I,
          "offset": 0,
          "size": 4,
        },
      },
      "r2dbe_vdif_%d_hdr_w1_ref_ep" % c: {
        "ref_epoch_%d" % c: {
          "decode": _dec_I,
          "encode": _enc_I,
          "offset": 0,
          "size": 4,
        },
      },
      "r2dbe_vdif_%d_hdr_w3_station_id" % c: {
        "station_%d" % c: {
          "decode": lambda x: "".join(chr(xx) for xx in x[2:]), #lambda x: chr((_dec_I(x) & 0xff00)>>8) + chr(_dec_I(x) & 0xff)},
          "encode": lambda x: b"\x00\x00" + bytes(ord(xx) for xx in x),
          "offset": 0,
          "size": 4,
        },
      },
      "r2dbe_vdif_%d_hdr_w3_thread_id" % c: {
        "thread_%d" % c: {
          "decode": _dec_I,
          "encode": _enc_I,
          "offset": 0,
          "size": 4,
            },
      },
      "r2dbe_vdif_%d_hdr_w4" % c: {
        "pol_%d" % c: {
          "decode": lambda x: POL[_dec_I(x) & 0x01],
          "encode": lambda x: _enc_I(POL.index(x) & 0x01),
          "offset": 0,
          "size": 4,
          "mask": "\xff\xff\xff\xfe",
        },
        "bdc_ch_%d" % c: {
          "decode": lambda x: BDC_CH[(_dec_I(x) & 0x02)>>1],
          "encode": lambda x: _enc_I((BDC_CH.index(x) << 1) & 0x02),
          "offset": 0,
          "size": 4,
          "mask": "\xff\xff\xff\xfd",
        },
        "rx_sb_%d" % c: {
          "decode": lambda x: RX_SB[(_dec_I(x) & 0x04)>>2],
          "encode": lambda x: _enc_I((RX_SB.index(x) << 2) & 0x04),
          "offset": 0,
          "size": 4,
          "mask": "\xff\xff\xff\xfb",
        },
        "eud_vers_%d" % c: {
          "decode": lambda x: (_dec_I(x) & 0xff000000)>>24,
          "encode": lambda x: _enc_I((x << 24) & 0xff000000),
          "offset": 0,
          "size": 4,
          "mask": "\x00\xff\xff\xff",
        },
      },
    })

def _get_param_dev_entry(name):
    for k, entry in list(_PARAM_TABLE.items()):
        if name in list(entry.keys()):

            return k, entry

    raise ValueError("no device entry found for parameter '%s'" % name)

def lookup_param(name):
    k, entry = _get_param_dev_entry(name)
    result = entry[name]
    result.update({"dev": k})

    return result

def decode_param(name, data):
    assert isinstance(data, bytes), "data should be bytes"

    k, entry = _get_param_dev_entry(name)
    sub_entry = entry[name]
    func = sub_entry["decode"]

    return func(data)

def encode_param(name, value):
    result = lookup_param(name)
    sub_entry = _PARAM_TABLE[result["dev"]][name]
    func = sub_entry["encode"]
    result["data"] = func(value)
    if "mask" in list(sub_entry.keys()):
        result["mask"] = sub_entry["mask"]

    return result

def mask_param(name, update, existing):
    assert isinstance(update, bytes), "update should be bytes"
    assert isinstance(existing, bytes), "existing should be bytes"
    assert len(update) == len(existing), "update and existing should be of equal length"

    entry = _PARAM_TABLE[lookup_param(name)["dev"]]
    mask = entry[name]["mask"]
    new = bytes(existing[ii] & ord(mask[ii]) for ii in range(len(existing)))

    return bytes(update[ii] | new[ii] for ii in range(len(existing)))
