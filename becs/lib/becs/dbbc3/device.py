import datetime
import math
import random
import re
import string
import time

#import adc5g
#import redis

#import katcplight
#import vdif

from .. import common

#from . import adc
#from . import data

import shutil

import logging
logger = logging.getLogger(__name__)

class DBBC3(object):

### exposed
    PARAMS = ["name", "host", "channels", "mode", "sw_major", "sw_minor", "port", "chainID", "chains", "ports"]
    CH_PARAMS = [
      "band",
      "pol",
      "rx_sb",
      "thread",
      "mac",
      "ip",
      "port",
      "dest_mac",
      "dest_ip",
      "dest_port",
    ]

### hidden
#    _root = "root"

### properties
    @property
    def host(self):
        return self._host

    @property
    def name(self):
        return self._name

    @property
    def serial(self):
        return common.get_iface_mac_addr("eth0", "root", self.host).replace(":","")

    @property
    def configuration(self):
        pass

### macro procedures
    def is_configured(self):
        '''
        Determines whether the DBBC3 is configured. 

        This method makes little sense for the DBBC3 as it must
        be configured by manually starting the control software
        on the DBBC3 machine. The method is kept for
        compatibility with the vlbicontrol architecture.
        
        In practise for the DBBC3 the commands "configure" 
        is identical to "reconfigure" regardless of the return value
        of this method.
        
        Returns:
            boolean: True in case of success False otherwise
        ''' 
        if not self.test_software_running_version():
            return False

        # are VDIF headers written correctly?
        # extended userdata in VDIF header currently not set on DBBC3
        ch_expect_get = []
        for ch, par in list(self._channels.items()):
            ch_expect_get += [
            #  (ch, "adc", self.get_data_source),
              #(ch, par["pol"], self.get_pol),
              #(ch, par["bdc_ch"], self.get_bdc_ch),
              #(ch, par["rx_sb"], self.get_rx_sb),
              #(self._dbbc3.boardToDigit(par["board"]), par["station"], self.get_station),
              #(ch, par["thread"], self.get_thread),
            ]
        for ch, expect, get in ch_expect_get:
            status = get(ch)
            logger.debug("%s ch %d source is %s, expect %s", self._host, ch, status, expect)
            if not status == expect:
                return False

        return True

    def configure(self):
        """
        Do the DBBC3 configuration

        Note:
            Most of the configuration e.g. loading the control software etc.
            must be done manually prior to running backendctl. 
            However a few configuration steps e.g. loading the filters and setting the IF
            properties should be carried out here.

        Returns:
            boolean: True in case of success, False otherwise
        """
        logger.info("Configuring...")
        if not self.conf_filters():
            return False
        if not self.conf_if():
            return False
        if not self.conf_synth_freq():
            return False
        if not self.conf_vdif_stationcode():
            return False
        if not self.conf_vdif_streams():
            return False

        return True

    def testgroup_preconfig(self):
        """
        Do pre configuration tests

        Returns:
            boolean: True in case of success, False otherwise
        """

        if not self._dbbc3:
            return False

        state = []
        state.append(all([self.test_device_reachable(),
          self.test_dbbc3_package_exists(),
          self.test_software_running_version()
          ]))
        if not all(state):
            return False

        for board in self._check_boards:
            state.append(self.test_bitfile_version(board))
        return all(state)

    def testgroup_postconfig(self):
        """
        Do post configuration tests

        Returns:
            boolean: True in case of success, False otherwise
        """
        states = []

        states.append(self.test_vdif())

        for board in self._check_boards:
            boardId = int(self._dbbc3.boardToDigit(board))
            rep = self._val.validateFilter(board, 1, self._boards[boardId]["filter1"])
            rep.log(logger)
            states.append(not rep.exit)
            rep = self._val.validateFilter(board, 2, self._boards[boardId]["filter2"])
            rep.log(logger)
            states.append(not rep.exit)

        for board in self._check_boards:
            boardId = int(self._dbbc3.boardToDigit(board))
            rep = self._val.validateSynthesizerFreq(board, self._boards[boardId]["synth_freq"])
            rep.log(logger)
            states.append(not rep.exit)

        globalchecks = [self._val.validateSamplerPhases,self._val.validatePPS]
        boardchecks = [self._val.validateIFLevel,
             self._val.validateTimesync,
             self._val.validateSynthesizerLock,
             self._val.validateSynthesizerFreq,
             self._val.validateSamplerPower,
             self._val.validateSamplerOffsets,
             self._val.validateBitStatistics]

        for i in range(len(globalchecks)):
            rep = globalchecks[i]()
            rep.log(logger)
            states.append(not rep.exit)

        for board in self._check_boards:
            for i in range(len(boardchecks)):
                rep = boardchecks[i](board)
                rep.log(logger)
                states.append(not rep.exit)
       
        result = all(states)

        return result

### config steps
    def conf_vdif_streams(self):
        """ start vdif output

        In case of failure:
            - contact DBBC3 expert 
        """
     
        results = []
        # For simplicity vdif will be enabled an started for all 4 output ports of each of the 
        # DBBC3 core boards to be configured

        for board in self._check_boards:
            boardId = int(self._dbbc3.boardToDigit(board))
            result = True
            try:
                self._dbbc3.core3h_start(boardId, format='vdif')
            except Exception as e:
                logger.debug("%s board %d failed to enable vdif output", self._host, boardId)
                result = False
            logger.info("%s board %d enabling vdif output " + ("succeeded" if result else "failed"), self._host, boardId)
            results.append(result)

        return all(results)

    def conf_filters(self):
        """
        Load filter1 and filter2 with the filter files defined in backendctl.conf

        In case of failure:
            -check that filter files in backendctl.conf are present on the DBBC3
        """
        
        results = []
        for board in self._check_boards:
            result = True
            boardId = int(self._dbbc3.boardToDigit(board))
            try:
                self._dbbc3.tap(boardId, 1, self._boards[boardId]["filter1"])
                self._dbbc3.tap(boardId, 2, self._boards[boardId]["filter2"])
            except Exception as e:
                logger.debug("%s board %d failed to load tap filters", self._host, boardId)
                result = False
            logger.info("%s board %d loading filters " + ("succeeded" if result else "failed"), self._host, boardId)
            results.append(result)
        return all(results)
            
    def conf_if(self):
        """ Configure the IF(s)
            - turn agc on
            - switch on down conversion
        In case of failure:
            - contact DBBC3 expert
        """
        results = []
        for board in self._check_boards:
            result = True
            try:
                boardId = int(self._dbbc3.boardToDigit(board))
                self._dbbc3.dbbcif(boardId, 2, "agc")
            except Exception as e:
                logger.debug("%s board %d failed to configure the IFs", self._host, boardId)
                result = False
            logger.info("%s board %d IF configuration (agc=on, downconversion=on) " + ("succeeded" if result else "failed"), self._host, boardId)
            results.append(result)
            
        return (all(results))

    def conf_synth_freq(self):
        """ Configure the GCoMo synthesizer frequencies
        
        In case of failure:
            - contact DBBC3 expert
        """

        results = []
        for board in self._check_boards:
            result = True
            try:
                boardId = int(self._dbbc3.boardToDigit(board))
                self._dbbc3.synthFreq(boardId, int(self._boards[boardId]["synth_freq"]))
            except Exception as e:
                logger.debug("%s board %d failed to set synthesizer frequency", self._host, boardId)
                result = False
            logger.info("%s board %d setting synthesizer frequency to %d MHz " + ("succeeded" if result else "failed"), self._host, boardId, self._boards[boardId]["synth_freq"])
            results.append(result)
        return all(results)

    def conf_vdif_stationcode(self):
        """ Sets the VDIF station code

        In case of failure:
            - verify that stationcode is defined in backendctl.conf
            - contact DBBC3 expert
        """
        results = []
        for board in self._check_boards:
            result = True
            boardId = int(self._dbbc3.boardToDigit(board))
            try:
                self._dbbc3.core3h_vdif_station(boardId, self._boards[boardId]["station"])
            except Exception as e:
                logger.debug("%s board %d failed to set VDIF station code", self._host, boardId)
                result = False
            logger.info("%s board %d setting VDIF station code to %s " + ("succeeded" if result else "failed"), self._host, boardId, self._boards[boardId]["station"])
            results.append(result)
        return all(results)


### tests
    def test_device_reachable(self):
        """Test if device is reachable on the network
        In case of failure:
          - check that DBBC3 is powered on
          - check network cable 
        """
        return common.ping_test(self._host)

    def test_dbbc3_package_exists(self):
        """ Test that the dbbc3ctl package is installed and included in the PYTHONPATH
        
        In case of failure:
            - install the dbbc3 package from github:
                https://github.com/mpifr-vlbi/dbbc3
            - make sure that the package is included in PYTHONPATH """
        import importlib
        if importlib.util.find_spec("DBBC3") is None:
            logger.info('dbbc3 package test failed')
            return False
        else:
            logger.info('dbbc3 package test passed')
            return True

    def test_dbbc3ctl_exists(self):
        """
        Test that dbbc3ctl.py is installed and included in the path
        
        In case of failure:
            - install the dbbc3 package from github:
                https://github.com/mpifr-vlbi/dbbc3
            - make sure that dbbc3ctl.py is included in the path
        """
        if shutil.which('dbbc3ctl.py') is None:
            logger.info('dbbc3ctl.py exist test failed')
            return False
        else:
            logger.info('dbbc3ctl.py exist test passed')
            return True

    def test_vdif(self):
        """
        Test the configuration of the VDIF streams

        In case of failure:
            - contact the DBBC3 experts
        """

        state = []
        for board in self._check_boards:
            boardId = int(self._dbbc3.boardToDigit(board))

            # check station code
            expect, status = self._boards[boardId]["station"], self.get_station(boardId)
            if not expect == status:
                logger.debug("%s board %s has station code %s, expect %s failed", self._host, board, status, expect)
                state.append(False)
            else:
                logger.info("%s board %s has station code %s test passed", self._host, board, status)

            # check if output is enabled
            stats = self._dbbc3.core3h_sysstat(boardId)
            if not stats["output"] == "started":
                logger.debug("%s board %s vdif output started test failed", self._host, board)
                state.append(False)
            else:
                logger.info("%s board %s vdif output started test passed", self._host, board)

        return all(state)
                
            

    def test_bitfile_version(self, board):
        """ Test that the DBBC3 bitfile version of the DBBC3 configuration matches the expected version
        In case of failure:
            - upload the correct bitfile to the DBBC3 
            - insert the bitfile reference to the DBBC3 configuration file
        """
        short = "%s configuration for board %s references correct bitfile test " 

        # use the printmainconfig directive for validating the DBBC3 configuration
        boardId = int(self._dbbc3.boardToDigit(board))
        ret = self._dbbc3.sendCommand("printmainconfig").splitlines()

        expect, status = self._boards[boardId]["bitfile"].strip(), ret[3+boardId].split()[1].strip()
        logger.debug("%s board %s configured with %s, expect %s", self._host, board, status, expect)
        if not expect == status:
            return False

        logger.info(short + "passed", self._host, board)
        return True
    
    def test_software_running_version(self):
        """ Test if DBBC3 control software version matches configuration
        In case of failure:
          - restart DBBC3 control software in the correct version"""
        short = "%s runs correct control software test "

        matchVersion = False
        try:
            # check for correct firmware mode
            expect, status = self._mode, self._dbbc3.version()["mode"]
            logger.debug("%s programmed with %s, expect %s", self._host, status, expect)
            if not expect == status:
                return False

            # check major version number of firmware.
            expect, status = self._sw_major, self._dbbc3.version()["majorVersion"]
            logger.debug("%s firmware major version is %d, expect %d", self._host, status, expect)
            if not expect <= status:
                return False
            elif (expect == status):
                matchVersion = True

            # check minor version number of firmware
            if matchVersion:
                expect, status = self._sw_minor, self._dbbc3.version()["minorVersion"]
                logger.debug("%s firmware minor version is %d, expect %d", self._host, status, expect)
                if not expect <= status:
                    return False
        except Exception as e:
            logger.debug("%s %s: %s", self._host, e.__class__.__name__, str(e))
            return False

        logger.info(short + "passed", self._host)
        return True


    def test_pps(self, board, pps_wait=10):
        """Test PPS signal input, exactly N counts should be registered after N seconds
        Deprecated: do not use. DBBC3 sysstat is not sufficiently timed to do this test
        In case of failure:
          - check PPS input connected to DBBC3 port
          - check PPS signal on oscilloscope / counter"""
        
        boardId = int(self._dbbc3.boardToDigit(board))

        self._wait_until_mid_second()
        before = time.time()
        ret = self._dbbc3.core3h_sysstat(boardId)
        pps_before = int(ret['pps_count'])
        time.sleep(pps_wait - 0.5)
        self._wait_until_mid_second()
        after = time.time()
        ret = self._dbbc3.core3h_sysstat(boardId)
        pps_after = int(ret['pps_count'])
        logger.debug("%s pps_count at %.3f: %d", self._host, before, pps_before)
        logger.debug("%s pps_count at %.3f: %d", self._host, after, pps_after)
        logger.debug("%s count difference %d, expect %d", self._host, pps_after - pps_before, pps_wait)
        if not pps_after - pps_before == pps_wait:
            logger.info("%s PPS test failed", self._host)
            return False
        logger.info("%s PPS test passed", self._host)
        return True



### miscellaneous


### get-set methods

    def get_station(self, board):
        '''
        Returns the station code set in the VDIF header
        '''
        assert board <= self._dbbc3.config.numCoreBoards, "board should be <= {%d}" % (self._dbbc3.config.numCoreBoards)
        return (self._dbbc3.core3h_vdif_station(board))

#    def set_station(self, ch, station):
#        assert ch in data.CH, "ch should be an integer from the set {%s}" % (", ".join(data.CH))
#        assert isinstance(station, str) and \
#          len(station) == 2, "station should be a two-character string"
#        return self._encode_set_param("station_%d" % ch, station)


    def __init__(self, redishost="localhost", redisport=6379, **kwargs):

        assert all(p in list(kwargs.keys()) for p in self.PARAMS), "kwargs has to include the following keys for %s:\n  - %s" % (self.__class__.__name__, "\n  - ".join(self.PARAMS))
        for k, v in list(kwargs.items()):
            setattr(self, "_" + k, v)

        for ch, d in list(self._channels.items()):
            for k in self.CH_PARAMS:
                assert k in list(d.keys()), "channel %d missing key %s" % (ch, k)

        assert self.test_dbbc3_package_exists(), "DBBC3 package not installed"

        import DBBC3 as d3
        from dbbc3.DBBC3Validation import ValidationFactory
        from dbbc3.DBBC3Validation import ValidationReport
        try:
            self._dbbc3 = d3.DBBC3(host=self.host, timeout=120)
            valFactory = ValidationFactory()
            self._val = valFactory.create(self._dbbc3, ignoreErrors=False)
            self._reporter = ValidationReport()
        except d3.DBBC3Exception as e:
            logger.error("Cannot connect to %s on port %d. Is there another client running?" % (self.host, self._port))
            self._dbbc3 = None
            return

        # for OCT_D mode there are 2 output channels per Core3H board
        self._max_channels = self._dbbc3.config.numCoreBoards * 2

        # determine the boards responsible for processing the chain
        self._check_boards = []
        for chan in list(self._channels.values()):
            port = "{}:{}_{}".format(self._name,chan["board"], chan["out_port"])
            if port in self._ports and chan["board"] not in self._check_boards:
                self._check_boards.append(chan["board"])

    def __str__(self):
        return self._host

    def _wait_until_mid_second(self, tol=50000, wait=0.025):
        while abs(datetime.datetime.utcnow().microsecond - 500000) > 50000:
            time.sleep(0.025)
        logger.debug("%s mid-second time is %.6f", self._host, time.time())
