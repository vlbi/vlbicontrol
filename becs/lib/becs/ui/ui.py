import inspect
import io
import functools
import gzip
import os
import pathlib
import subprocess
import sys

import logging
import logging.handlers
logger = logging.getLogger()
# set logger to lowest level
logger.setLevel(logging.NOTSET)
# remove all handlers and add a single NullHanlder
[logger.removeHandler(h) for h in logger.handlers]
logger.addHandler(logging.NullHandler())
# create stream handlers with default output to stdout / stderr
h_stdout = logging.StreamHandler(sys.stdout)
h_stdout.setLevel(logging.CRITICAL + 1)
h_stderr = logging.StreamHandler(sys.stderr)
h_stderr.setLevel(logging.CRITICAL + 1)

RC_OKAY, RC_ERROR, RC_FAIL = list(range(3))

class ColorFormatter(logging.Formatter):

    def __init__(self, lvlcolmap, *args, **kwargs):
        self.map = lvlcolmap
        super(ColorFormatter, self).__init__(*args, **kwargs)

    def format(self, record):
        msg = super(ColorFormatter, self).format(record)
        if record.levelno in list(self.map.keys()):
            msg = format(msg, color=self.map[record.levelno], bold=True)
        return msg

def prompt(message):
    sys.stdout.write(message)
    sys.stderr.write(message)
    response = input()
    return response

def format(s, color=None, bold=False):
    try:
        s = {
          "b": "\033[34m",
          "g": "\033[92m",
          "y": "\033[93m",
          "r": "\033[91m",
        }[color] + ("\033[1m" if bold else "") + s + "\033[0m"
    except KeyError:
        pass
    return s

def colorise_outcomes(s, success=["passed", "succeeded"], failure=["failed"]):
    for phrase in success:
        s = s.replace(phrase, "\033[92m\033[1m%s\033[0m" % phrase)
    for phrase in failure:
        s = s.replace(phrase, "\033[91m\033[1m%s\033[0m" % phrase)
    return s

_lvl = 0
def _format_lines(s):
    s = colorise_outcomes(s)
    #~ global _lvl
    pre = "  %s" % ("  " * _lvl)
    lines = [l for l in s.splitlines() if l]
    return pre + ("\n%s" % pre).join(lines) + "\n"

def _format_docstr(s, prefix_0="", prefix_other=""):
    out_lines = []
    for i, line in enumerate(s.splitlines()):
        if i == 0 and prefix_0:
            out_lines.append(prefix_0 + line.lstrip())
            continue
        out_lines.append(prefix_other + line.lstrip())
    return "\n".join(out_lines) + ("\n" if s.endswith("\n") else "")

# Define blocks in stderr by splitting on lines in stdout, and then for each block,
# take the stdout line that trails the stderr block and write it before the stderr block.
# Each stderr-only line is prefixed with prefix_stderr (after leading space), and
# lines in both stdout and stderr are prefixed with prefix_stdout (after leading space)
def _reorder_stdout_in_stderr(stdout, stderr, prefix_stderr="", prefix_stdout=""):
    new_lines = []
    buf = ""
    err_lines = stderr.splitlines()
    err_lines.reverse()
    out_lines = stdout.splitlines()
    for l in err_lines:
        l_nw = l.lstrip()
        l_w = l[:-len(l_nw)]
        if l in out_lines:
            if buf:
                new_lines.append(buf)
                buf = ""
            buf = l_w + prefix_stdout + l_nw
        else:
            new_lines.append(l_w + prefix_stderr + l_nw)
    if buf:
        new_lines.append(buf)
    new_lines.reverse()
    return "\n".join(new_lines) + ("\n" if stderr.endswith("\n") else "")

# wrapper that captures log messages, and write them to stdout and stderr
# depending on whether test_result(func(*args, **kwargs)) evaluates to True or False
#   - True: info+ --> stdout, debug+ --> stderr
#   - False: debug+ --> stdout, debug+ --> stderr
def _intecept_log(func, test_result):
    @functools.wraps(func)
    def wrapped(self, *args, **kwargs):
        global _lvl
        _lvl += 1
        tmp_o, tmp_e = h_stdout.stream, h_stderr.stream
        try:
            h_stdout.stream, h_stderr.stream = io.StringIO(), io.StringIO()
            r = func(self, *args, **kwargs)
            s_out, s_err = h_stdout.stream.getvalue(), h_stderr.stream.getvalue()
            # format all lines uniformly
            if s_out:
                s_out = _format_lines(s_out)
            if s_err:
                s_err = _format_lines(s_err)
            # do reordering of stdout/stderr, and prefix stderr-only lines with indent and tree-expand symbol
            s_err = _reorder_stdout_in_stderr(s_out, s_err, prefix_stderr="  L.. ")
            if test_result(r):
                sys.stdout.write(s_out)
            else:
                sys.stdout.write(s_err)
                if func.__doc__:
                    s_doc = _format_docstr(func.__doc__, prefix_0="  L.. ### ", prefix_other="      ### ")
                    s_doc = _format_lines(s_doc)
                    sys.stdout.write(s_doc)
            sys.stderr.write(s_err)
        finally:
            h_stdout.stream, h_stderr.stream = tmp_o, tmp_e
        _lvl -= 1
        return r
    return wrapped

# decorator that wraps all methods in the class hierarchy starting with pattern
# in _intecept_log
def intercept_log(pattern, test=lambda x: x):
    def _inner(cls):
        # helper function to find matching attributes all the way down
        def _build_attr(cls):
            attrs = {}
            # when we reach object level, stop
            if cls.__name__ == "object":
                return attrs
            # iterate over base classes
            for base in cls.__bases__:
                # recurse down in base class, updating attributes at this level
                attrs.update(_build_attr(base))
            # add matching attributes in this class
            for attr, val in cls.__dict__.items():
                if callable(val) and attr.startswith(pattern):
                    #~ print "%s in %s" % (attr, base)
                    attrs[attr] = _intecept_log(val, test)
            return attrs
        # get the attributes
        attrs = _build_attr(cls)
        # add them to the class
        for k, v in list(attrs.items()):
            setattr(cls, k, v)
        return cls
    return _inner

def init_ui(log_to_file=True):
    # add handlers to root logger and set levels
    h_stdout.setLevel(logging.INFO)
    h_stdout.setFormatter(ColorFormatter({logging.WARNING:"y",logging.ERROR:"r"}))
    logger.addHandler(h_stdout)
    h_stderr.setLevel(logging.DEBUG)
    h_stderr.setFormatter(ColorFormatter({logging.WARNING:"y",logging.ERROR:"r"}))
    logger.addHandler(h_stderr)
    if log_to_file:
        # initialise logging
        init_logging()

def init_logging():
    """Log all messages to file.

    Messages are handled by a RotatingFileHandler with maxBytes set to 0, except
    for the very first log entry where maxBytes is set to 10MB. This ensures
    that roll-over will only occur at the start of script execution, so that a
    complete execution is always contained in a single log-file.

    Custom rotator and namer callables are used to gzip-compress backup log
    files."""
    # create rotating file handler, all levels
    filename = pathlib.os.sep.join([str(pathlib.Path.home()),"log","becs.log"])
    rfh = logging.handlers.RotatingFileHandler(filename, maxBytes=10485760, backupCount=5)
    rfh.setLevel(logging.DEBUG)
    # define a rotator with compression
    def compression_rotator(source, dest):
        with open(source, mode="rb") as f_in, gzip.open(dest, mode="wb", compresslevel=9) as f_out:
            f_out.write(f_in.read())
        os.remove(source)
    rfh.rotator = compression_rotator
    # define namer with .gz extension
    def compression_namer(name):
        return name + ".gz"
    rfh.namer = compression_namer
    # create special log-entry when logging first initialised ..
    msg = ""
    this_file_path = pathlib.Path(__file__).absolute()
    #   .. first try to get the command-line calls for PID and parent's PID
    for n, pid in enumerate((os.getppid(), os.getpid())):
        subp = subprocess.run(["ps","-p",str(pid),"-o","args="], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if subp.returncode == 0:
            msg += "\n..: %s : %s " % ("proc" if n else "parent", subp.stdout.decode("ascii").strip())
    #   .. then get git information
    subp = subprocess.run(["git", "describe", "--always", "--dirty"], cwd=str(this_file_path.parent), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if subp.returncode == 0:
        msg += "\n..: git : %s" % subp.stdout.decode("ascii").strip()
    #   .. set special formatting for this log-entry
    rfh.setFormatter(logging.Formatter(fmt="@ %(asctime)s : %(message)s", datefmt="%Y-%m-%d %H:%M:%S"))
    logrec = logging.LogRecord("root", logging.INFO, str(this_file_path),
      inspect.getframeinfo(inspect.currentframe()).lineno, msg, (), None)
    #   .. and send it directly to handler (bypass other potential handlers)
    rfh.handle(logrec)
    # now set maxBytes to zero for remainder of this session
    rfh.maxBytes = 0
    # set default format for future messages
    rfh.setFormatter(logging.Formatter(fmt="%(asctime)s : %(name)s : %(funcName)s : %(levelname)s : %(message)s", datefmt="%Y-%m-%d %H:%M:%S"))
    # add as handler to root logger
    logger.addHandler(rfh)
