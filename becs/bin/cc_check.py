try:
    import collections
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import config

    import becs.cc

    import becs.ui as ui

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class ControlComputer(becs.cc.ControlComputer):
        pass

    def main(args):
        cfg = config.Config(args.conffile)
        # initialise devobjs with control-computer
        devname = "control-computer"
        obj = ControlComputer()
        host = obj.host
        result = False
        try:
            logger.info("%s (hostname: %s) running pre-config tests ...", devname, host)
            if not obj.testgroup_preconfig():
                logger.info("%s (hostname: %s) one or more pre-config tests failed, aborting device configuration", devname, host)
                return result
            logger.info("%s (hostname: %s) all pre-config tests passed", devname, host)
            # for any reachable device, get configuration
            devcfg = obj.configuration
            logger.info("%s (hostname: %s) checking if device is configured ...", devname, host)
            if not obj.is_configured():
                logger.info("%s (hostname: %s) device is not configured", devname, host)
                return result
            logger.info("%s (hostname: %s) running post-config tests ...", devname, host)
            if not obj.testgroup_postconfig():
                logger.info("%s (hostname: %s) one or more post-config tests failed", devname, host)
                return result
            logger.info("%s (hostname: %s) all post-config tests passed", devname, host)
            result = True
        except Exception as ex:
            exc_type, exc_value, exc_tb = sys.exc_info()
            out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
            out = "\n".join("  !!! " + line for line in out.strip().splitlines())
            out = "  !!! Caught %s while checking %s: %s\n" % (ex.__class__.__name__, devname, str(ex)) + out
            logger.error(out)
        logger.info("%s configuration:\n%s", devname, devcfg)
        return result

    arg_parser = ArgumentParser(description="check control-computer")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    args = arg_parser.parse_args()
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
