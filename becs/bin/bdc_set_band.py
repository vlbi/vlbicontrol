try:
    import multiprocessing
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import config

    import becs.config
    import becs.ui as ui
    import becs.bdc as device

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("test")
    class DualBandBDC(device.DualBandBDC):
        pass

    # make instance generator function
    get_bdc_2band_inst = lambda name, cfg: becs.config.get_bdc_2band_inst(name, cfg, DualBandBDC)

    def _parallel_main(devname_args_cfg):
        try:
            devname, args, cfg = devname_args_cfg
            dev = cfg.devices[devname]
            host = dev["hostname"]
            if dev["layout"] == "bdc_1band":
                logger.info("%s is a single-band BDC, set band not allowed, aborting", devname)
                return False
            bdc = get_bdc_2band_inst(devname, cfg)
            if not bdc:
                logger.info("%s invalid device specification in config file, aborting", devname)
                return False
            if not bdc.test_device_reachable():
                logger.info("%s (hostname: %s) is not reachable, aborting", devname, host)
                return False
            if not bdc.set_band({"4to8": "4-8", "5to9": "5-9"}[args.band]):
                logger.info("%s (hostname: %s) switch to band '%s' failed", devname, host, args.band)
                return False
            logger.info("%s (hostname: %s) switched to band '%s'", devname, host, args.band)
            return True
        except Exception as ex:
            exc_type, exc_value, exc_tb = sys.exc_info()
            out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
            out = "\n".join("  !!! " + line for line in out.strip().splitlines())
            out = "  !!! Caught %s while processing device %s: %s\n" % (ex.__class__.__name__, devname, str(ex)) + out
            logger.error(out)
            return False

    def main(args):
        cfg = config.Config(args.conffile)
        # if no devices specified, use all from config file
        if not args.devnames:
            setattr(args, "devnames", [name for name, dev in list(cfg.devices.items()) if dev["layout"] in {"bdc_2band"}])
        devname_args_cfg = [(name, args, cfg) for name in args.devnames]
        p = multiprocessing.Pool(max(1, len(devname_args_cfg)))
        results = p.map(_parallel_main, devname_args_cfg)
        if not all(results):
            if any(results):
                logger.info("Band setting succeeded on the following devices:\n    %s", "\n    ".join([f[0] for f in [devname_args_cfg[i] for i, r in enumerate(results) if r]]))
            logger.info("Band setting failed on the following devices:\n    %s", "\n    ".join([f[0] for f in [devname_args_cfg[i] for i, r in enumerate(results) if not r]]))
            return False
        logger.info("Band setting succeeded on all devices")
        return True

    arg_parser = ArgumentParser(description="set bdc control state",
      epilog="If no device-name is given, then apply to all bdc instances in config-file. " + \
      "Use the --clear option when temporary lock-out is encountered.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    arg_parser.add_argument("band", choices=["4to8", "5to9"],
      help="control state to apply")
    arg_parser.add_argument("devnames", metavar="device-name", nargs="*",
      help="bdc instance name in config-file")
    args = arg_parser.parse_args()

    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
