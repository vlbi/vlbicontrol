#!/bin/usr/bin/python2
try:
    import collections
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import config

    import becs.cc
    import becs.config
    import becs.bdc
    import becs.mark6
    import becs.r2dbe
    import becs.dbbc3

    import becs.ui as ui

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class ControlComputer(becs.cc.ControlComputer):
        pass

    @ui.intercept_log("is_configured")
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class SingleBandBDC(becs.bdc.SingleBandBDC):
        pass

    @ui.intercept_log("is_configured")
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class DualBandBDC(becs.bdc.DualBandBDC):
        pass

    @ui.intercept_log("is_configured")
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class Mark6(becs.mark6.Mark6):
        pass

    @ui.intercept_log("is_configured")
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class R2DBE(becs.r2dbe.R2DBE):
        pass

    @ui.intercept_log("is_configured")
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class DBBC3(becs.dbbc3.DBBC3):
        pass

    # mapping from layout parameter value to function that will return device class instance
    _layout_get_obj = {
      "bdc_1band": lambda name, cfg: becs.config.get_bdc_1band_inst(name, cfg, SingleBandBDC),
      "bdc_2band": lambda name, cfg: becs.config.get_bdc_2band_inst(name, cfg, DualBandBDC),
      "mark6": lambda name, cfg: becs.config.get_mark6_inst(name, cfg, Mark6),
      "r2dbe": lambda name, cfg: becs.config.get_r2dbe_inst(name, cfg, R2DBE),
      "dbbc3": lambda name, cfg: becs.config.get_dbbc3_inst(name, cfg, args.chainid, DBBC3),
    }

    def get_all_dev_obj_param(cfg, devnames):
        devobjs = collections.OrderedDict()
        if not devnames:
            devnames = becs.config.get_all_device_names(cfg)
        for devname in devnames:
            dev = cfg.devices[devname]
            if dev["layout"] not in list(_layout_get_obj.keys()):
                continue
            obj = _layout_get_obj[dev["layout"]](devname, cfg)
            if not obj:
                logger.info("%s invalid device specification in config file", devname)
                continue
            devobjs[devname] = (dev["hostname"], obj)
        return devobjs

    def main(args):
        cfg = config.Config(args.conffile)
        # initialise devobjs with control-computer
        _cc_name_ = "control-computer"
        _cc_obj = ControlComputer()
        _cc_host = _cc_obj.host
        devobjs = collections.OrderedDict([(_cc_name_, (_cc_host, _cc_obj))])
        # then ad all other devices
        devobjs.update(get_all_dev_obj_param(cfg, args.devnames))
        results = collections.OrderedDict()
        for devname, host_obj in list(devobjs.items()):
            try:
                # if control-computer checks / configure failed, abort all other devices
                if _cc_name_ in list(results.keys()) and not results[_cc_name_]:
                    logger.info("%s failed test or configure, abort all other backend devices", _cc_name_)
                    break
                host, obj = host_obj
                logger.info("%s (hostname: %s) running pre-config tests ...", devname, host)
                if not obj.testgroup_preconfig():
                    logger.info("%s (hostname: %s) one or more pre-config tests failed, aborting device configuration", devname, host)
                    results[devname] = False
                    continue
                logger.info("%s (hostname: %s) all pre-config tests passed", devname, host)
                if not args.force_reconfig and obj.is_configured():
                    logger.info("%s (hostname: %s) already configured, skipping configuration", devname, host)
                else:
                    logger.info("%s (hostname: %s) being configured ...", devname, host)
                    if not obj.configure():
                        logger.info("%s (hostname: %s) congfigure has failed", devname, host)
                        results[devname] = False
                        continue
                    logger.info("%s (hostname: %s) configuring done, verifying configuration ...", devname, host)
                    if not obj.is_configured():
                        logger.info("%s (hostname: %s) configure verification has failed", devname, host)
                        results[devname] = False
                        continue
                    logger.info("%s (hostname: %s) configuration verified", devname, host)
                logger.info("%s (hostname: %s) running post-config tests ...", devname, host)
                if not obj.testgroup_postconfig():
                    logger.info("%s (hostname: %s) one or more post-config tests failed", devname, host)
                    results[devname] = False
                    continue
                logger.info("%s (hostname: %s) all post-config tests passed", devname, host)
                results[devname] = True
            except Exception as ex:
                exc_type, exc_value, exc_tb = sys.exc_info()
                out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
                out = "\n".join("  !!! " + line for line in out.strip().splitlines())
                out = "  !!! Caught %s while configuring %s: %s\n" % (ex.__class__.__name__, devname, str(ex)) + out
                logger.error(out)
                results[devname] = False
        if not all(results.values()):
            if any(results.values()):
                logger.info("Configure succeeded on the following devices:\n    %s", "\n    ".join([k for k, v in list(results.items()) if v]))
            logger.info("Configure failed on the following devices:\n    %s", "\n    ".join([k for k, v in list(results.items()) if not v]))
        else:
            logger.info("Configure succeeded on all devices")
        return all(results.values())

    arg_parser = ArgumentParser(description="configure backend",
      epilog="For each device do pre-config tests and if all pass configure. " + \
      "If no devices specified, apply to all devices in config-file. " + \
      "By default skip reconfiguring devices that are already configured.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    arg_parser.add_argument("--force-reconfig", action="store_true",
      help="force reconfigure on already configured devices")
    arg_parser.add_argument("--chain-id", dest="chainid", metavar="chainid", required=False, help="id of signal chain to process")
    arg_parser.add_argument("devnames", metavar="device-name", nargs="*",
      help="names of devices to configure")
    args = arg_parser.parse_args()
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
