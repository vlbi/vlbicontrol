try:
    import datetime
    import glob
    import os
    import string
    import sys
    import time
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import actionparser
    import config
    import vexinterpreter
    import vexparser

    import becs.ui as ui
    import becs.common as common

    from common import ArgumentParser

    def _execute_cmds(cmds):
        results = [None, ]*len(cmds)
        for i, cmd in enumerate(cmds):
            rc, out, err = common.subp_system_call(str(cmd))
            results[i] = rc == 0
            if out.strip():
                logger.info(out.strip())
            if err.strip():
                logger.debug(err.strip())
        return all(results)

    def _execute_timed_cmdslist(timed_cmdslist):
        for t, cmds in timed_cmdslist:
            cmds_disp = "\n    : " + "\n    : ".join([str(c) for c in cmds])
            now = datetime.datetime.utcnow()
            logger.info("(run_sched.py) time is %s, next commands at %s", now.strftime("%H:%M:%S"), t.strftime("%H:%M:%S"))
            logger.debug("(run_sched.py) commands are:%s", cmds_disp)
            wait = (t - now).total_seconds()
            if wait > 0:
                time.sleep(wait)
            # skip actions more than 1s in the past
            # (commands that are late by less than 1s still carried out)
            elif wait < -1:
                logger.info("(run_sched.py) ... commands were late by >1s, skipping")
                continue
            # execute commands
            if not _execute_cmds(cmds):
                logger.info("(run_sched.py) one or more commands failed")

    def _reference_actspec_list(scan_list, actspec_list):
        timed_cmdslist = []
        for i, scan in enumerate(scan_list):
            # calculate gap until next scan
            if i+1 < len(scan_list):
                next_gap = (scan_list[i+1].start - scan.stop).total_seconds()
            else:
                next_gap = None
            # calculate gap until previous scan
            if i-1 >= 0:
                prev_gap = (scan_list[i-1].stop - scan.start).total_seconds()
            else:
                prev_gap = None
            for act in actspec_list:
                ts = act.timespec
                if ts.ref == ts.REF_SCAN_START:
                    ref = scan.start
                    # if offset is larger than scan length, do not add
                    if ts.offset > scan.duration:
                        logger.debug("timespec offset %d > scan duration %d, skipping", ts.offset, scan.duration)
                        continue
                    # if negative offset is smaller than prev-gap length, do not add
                    if prev_gap and -ts.offset < prev_gap:
                        logger.debug("timespec neg.offset %d > previous gap %d, skipping", -ts.offset, prev_gap)
                        continue
                elif ts.ref == ts.REF_SCAN_END:
                    ref = scan.stop
                    # if offset is larger than next gap, do not add
                    if next_gap and ts.offset > next_gap:
                        logger.debug("timespec offset %d > next gap %d, skipping", ts.offset, next_gap)
                        continue
                    # if negative offset is smaller than scan length
                    if -ts.offset > scan.duration:
                        logger.debug("timespec neg.offset %d > scan duration %d, skipping", -ts.offset, scan.duration)
                        continue
                else:
                    logger.debug("unknown timespec reference '%s'", ts.ref)
                    continue
                absolute = ref + datetime.timedelta(seconds=ts.offset)
                timed_cmdslist.append((absolute, act.commands))
        return timed_cmdslist

    def _make_test_vex(inpath, outpath, stationcode, delay, numscan, scanlen, gaplen):
        if not os.path.exists(inpath):
            logger.info("template schedule does not exist: '%s'", inpath)
            return False
        vex_dict = vexparser.vexparser(inpath)
        vex = vexinterpreter.Vex(vex_dict)
        # delete all scans, set the station code, and add new scans
        vex.clr_sched()
        vex.replace_station(vex.stations[0], stationcode)
        start = datetime.datetime.utcnow() + datetime.timedelta(seconds=delay)
        for n in range(numscan):
            vex.add_scan(start, scanlen)
            start += datetime.timedelta(seconds=scanlen+gaplen)
        try:
            with open(outpath, "w") as fh:
                fh.write(vex.to_string())
                return True
        except IOError as ioe:
            logger.info("error while trying to write to %s: %s", str(ioe))
        return False

    def get_valid_schedules(cfg, area="trigger"):
        stationcode = cfg.instruct["stationcode"]
        valids = []
        # look for schedules in the trigger area
        candidates = glob.glob(os.sep.join([cfg.instruct["vexstore"], area, "*.vex"]))
        if not candidates:
            logger.info("no vex-files found in %s area", area)
            return valids
        # filter out invalid schedules
        now = datetime.datetime.utcnow()
        for candidate in candidates:
            vex_dict = vexparser.vexparser(candidate)
            vex = vexinterpreter.Vex(vex_dict)
            vex.schedule.set_station(stationcode)
            # any scans?
            if not vex.schedule.scans:
                logger.debug("candidate schedule %s contains no scans for station %s, invalid", candidate, stationcode)
                continue
            # scans with future end date? (only check last scan)
            if vex.schedule.scans[-1].stop <= now:
                logger.debug("candidate schedule %s last scan for %s ends at %s, invalid", candidate, stationcode, vex.schedule.scans[-1].stop.strftime("%Yy%jd-%Hh%Mm%Ss"))
                continue
            logger.info("candidate schedule %s is valid for station %s", candidate, stationcode)
            valids.append(candidate)
        return valids

    def main(args):
        cfg = config.Config(args.conffile)
        stationcode = cfg.instruct["stationcode"]
        # find schedule full path
        if args.testvex:
            # use test schedule ...
            full_path = os.sep.join([cfg.instruct["vexstore"], "testing", args.testvex])
            valids = get_valid_schedules(cfg, area="testing")
            if not full_path in valids:
                logger.info("schedule '%s' is invalid (no scans for station '%s' with future end-time)", full_path, stationcode)
                return False
            msg = "using TEST schedule: %s" % full_path
            logger.info(ui.format(msg, color="y", bold=True))
        else:
            # ... otherwise get valid schedules from trigger area
            valids = get_valid_schedules(cfg, area="trigger")
            if not len(valids) == 1:
                logger.info("found %d valid schedules in trigger area, there should be exactly 1, aborting", len(valids))
                return False
            full_path = valids[0]
        setattr(args, "vexfile", os.path.basename(full_path))
        setattr(args, "vexpath", full_path)
        vex_dict = vexparser.vexparser(full_path)
        vex = vexinterpreter.Vex(vex_dict)
        vex.schedule.set_station(stationcode)
        scan_list_str = "\n    ".join(["%10d   %s   %s   %9ds   %10s" % (i, s.start.strftime("%Yy%jd-%Hh%Mm%Ss"), s.stop.strftime("%jd-%Hh%Mm%Ss"), s.duration, s.source) for i, s in enumerate(vex.schedule.scans)])
        hdr_str = "%10s   %19s   %14s   %10s   %10s" % ("#scan", "start", "stop", "duration", "source")
        logger.info("schedule file %s contains the following scans for station '%s':\n    %s\n    %s", args.vexfile, stationcode, hdr_str, scan_list_str)
        # parse action file
        try:
            actlist = actionparser.parse(args.actnfile)
        except actionparser.ActionParserError as ape:
            logger.info("an error occurred parsing action-file, aborting [%s]", str(ape))
            return False
        # get absolute-time commands
        timed_cmdslist = _reference_actspec_list(vex.schedule.scans, actlist)
        # and start running
        logger.info("running the schedule now ...")
        _execute_timed_cmdslist(timed_cmdslist)
        logger.info("... schedule has completed.")
        return True

    arg_parser = ArgumentParser(description="run a schedule",
      epilog="This script will automatically select a valid schedule from the trigger area to load, unless the --test-vex option is invoked in which case the given TESTVEX in the testing area is used." + \
      "A valid schedule is defined as having one or more scans for this station that have a future end-time. " + \
      "An error is reported if the number of valid schedules in the trigger area is not exactly equal to 1. " + \
      "If no devices are specified, the operation is performed for all mark6s found in the config file.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="FILE", required=True,
      help="backend config file")
    arg_parser.add_argument("--test-vex", dest="testvex", metavar="TESTVEX",
      help="test schedule filename (basename) to run")
    arg_parser.add_argument("--action-file", dest="actnfile", metavar="FILE", required=True,
      help="action specifications file (WARNING: not implemented yet)")
    args = arg_parser.parse_args()
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
