try:
    import argparse
    import datetime
    import multiprocessing
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import config

    import becs.config
    import becs.ui as ui
    import becs.r2dbe as device

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class R2DBE(device.R2DBE):
        pass

    # make instance generator function
    get_r2dbe_inst = lambda name, cfg: becs.config.get_r2dbe_inst(name, cfg, R2DBE)

    def _parallel_main(cfg_devname_inp_op):
        try:
            cfg, devname, inp, op = cfg_devname_inp_op
            dev = cfg.devices[devname]
            host = dev["hostname"]
            r2dbe = get_r2dbe_inst(devname, cfg)
            if not r2dbe:
                logger.info("%s invalid device specification in config file, aborting", devname)
                return False
            if not r2dbe.test_device_reachable():
                logger.info("%s (hostname: %s) is not reachable, aborting", devname, host)
                return False
            if not r2dbe.test_tcpborphserver_running():
                logger.info("%s (hostname: %s) tcpborphserver not running, aborting", devname, host)
                return False
            if not r2dbe.test_firmware_running_version():
                logger.info("%s (hostname: %s) correct firmware not running, aborting", devname, host)
                return False
            if not r2dbe.test_sys_clk_incr():
                logger.info("%s (hostname: %s) system clock not running, aborting", devname, host)
                return False
            if not inp:
                inp = [i.split(cfg.delim_port)[-1] for i in cfg.devices[devname]["inports"]]
            result = True
            for i in inp:
                ch = becs.config.get_r2dbe_inport_channel(i)
                if op == "2bit":
                    outcome = r2dbe.set_2bit_threshold(ch)
                    timestamp = datetime.datetime.utcnow().strftime('%Yy%jd%Hh%Mm%Ss')
                    if not outcome:
                        logger.info("%s (hostname: %s) setting 2-bit threshold for %s failed", devname, host, i)
                        result = False
                        continue
                    th = r2dbe.get_2bit_threshold(ch)
                    logger.info("%s (hostname: %s) 2-bit threshold for %s set to %d at %s", devname, host, i, th, timestamp)
                elif op == "qcore":
                    outcome = r2dbe.calibrate_adc_cores(ch)
                    timestamp = datetime.datetime.utcnow().strftime('%Yy%jd%Hh%Mm%Ss')
                    if not outcome:
                        logger.info("%s (hostname: %s) calibrating adc quad-cores for %s failed", devname, host, i)
                        result = False
                        continue
                    gains = r2dbe.get_core_gains(ch)
                    offsets = r2dbe.get_core_offsets(ch)
                    logger.info("%s (hostname: %s) adc quad-core calibration results for %s at %s are:\n" + \
                      "      gains = [%s]\n" + \
                      "    offsets = [%s]", devname, host, i, timestamp, ", ".join("%.2f" % g for g in gains), ", ".join("%.2f" % o for o in offsets))
            return result
        except Exception as ex:
            exc_type, exc_value, exc_tb = sys.exc_info()
            out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
            out = "\n".join("  !!! " + line for line in out.strip().splitlines())
            out = "  !!! Caught %s while processing device %s: %s\n" % (ex.__class__.__name__, devname, str(ex)) + out
            logger.error(out)
            return False

    def main(args):
        cfg = config.Config(args.conffile)
        if not args.devices:
            devices_ports = [(k, []) for k, v in list(cfg.devices.items()) if v["layout"] == "r2dbe"]
            setattr(args, "devices", dict(devices_ports))
        cfg_devname_inp_op = [(cfg, k, v, args.cal_type) for k, v in list(args.devices.items())]
        p = multiprocessing.Pool(max(1, len(cfg_devname_inp_op)))
        results = p.map(_parallel_main, cfg_devname_inp_op)
        if not all(results):
            if any(results):
                logger.info("%s calibration completed on the following devices:\n    %s", args.cal_type, "\n    ".join([f[1] for f in [cfg_devname_inp_op[i] for i, r in enumerate(results) if r]]))
            logger.info("%s calibration failed on the following devices:\n    %s", args.cal_type, "\n    ".join([f[1] for f in [cfg_devname_inp_op[i] for i, r in enumerate(results) if not r]]))
            return False
        logger.info("%s calibration completed on all devices", args.cal_type)
        return True

    all_ports = ["if0", "if1"]
    all_cal_types = ["2bit", "qcore"]
    def arg_type_port(arg):
        dev_port = arg.split(":")
        if not len(dev_port) == 2 or not dev_port[0] or not dev_port[1]:
            raise argparse.ArgumentTypeError("port should be in the format <dn>:<pn>")
        return tuple(dev_port)
    arg_parser = ArgumentParser(description="calibrate r2dbe channels",
      epilog="Ports are specified in full as defined in the config-file, and have the form <device-name>:<port-name>. " + \
      "If no ports given, perform the operation for all channels for all r2dbe devices.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    arg_parser.add_argument("--cal-type", dest="cal_type", metavar="cal-type", choices=all_cal_types, required=True,
      help="set the calibration type (one of {'%s'})" % ("', '".join(all_cal_types)))
    arg_parser.add_argument("ports", metavar="port", nargs="*", type=arg_type_port,
      help="add port to operand list")
    args = arg_parser.parse_args()
    devices = {}
    for dev, port in args.ports:
        if dev not in list(devices.keys()):
            devices[dev] = []
        devices[dev].append(port)
    setattr(args, "devices", devices)
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
