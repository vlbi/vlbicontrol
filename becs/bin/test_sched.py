try:
    import argparse
    import datetime
    import os
    import string
    import sys
    import time
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import actionparser
    import config
    import vexinterpreter
    import vexparser

    import becs.ui as ui
    import becs.common as common

    from common import ArgumentParser

    def _make_test_vex(inpath, outpath, stationcode, delay, numscan, scanlen, gaplen):
        if not os.path.exists(inpath):
            logger.info("template schedule does not exist: '%s'", inpath)
            return False
        vex_dict = vexparser.vexparser(inpath)
        vex = vexinterpreter.Vex(vex_dict)
        # delete all scans, set the station code, set experiment name, and add new scans
        vex.clr_sched()
        vex.replace_station(vex.stations[0], stationcode)
        vex.set_experiment_name(os.path.basename(outpath)[:-4])
        start = datetime.datetime.utcnow() + datetime.timedelta(seconds=delay)
        for n in range(numscan):
            logger.debug("adding scan of %ds starting at %s", scanlen, start.strftime("%Yy%jd-%Hh%Mm%Ss"))
            vex.add_scan(start, scanlen)
            start += datetime.timedelta(seconds=scanlen+gaplen)
            if n < numscan-1:
                logger.debug("adding gap of %ds", gaplen)
        try:
            with open(outpath, "w") as fh:
                logger.debug("writing vex to %s", outpath)
                fh.write(vex.to_string())
                logger.info("wrote vex to %s", outpath)
                return True
        except IOError as ioe:
            logger.info("error while trying to write to %s: %s", outpath, str(ioe))
        return False

    def main(args):
        # parse config file
        cfg = config.Config(args.conffile)
        stationcode = cfg.instruct["stationcode"]
        # auto-generate vex-file if no name given
        if not args.vexfile:
            setattr(args, "vexfile", "test-%s-%s.vex" % (stationcode, datetime.datetime.utcnow().strftime("%Yy-%jd-%Hh%Mm%Ss")))
        # full path of vexfile to read / create
        full_path = os.sep.join([cfg.instruct["vexstore"], "testing", args.vexfile])
        if args.test:
            # if just testing, create a vex-file first using this template
            vex_tmpl_path = os.sep.join(vexparser.__file__.split(os.sep)[:-1] + ["test.vex"])
            if not _make_test_vex(vex_tmpl_path, full_path, stationcode, args.test["delay"], args.test["numscan"], args.test["scanlen"], args.test["gaplen"]):
                logger.info("unable to create test schedule '%s', aborting", full_path)
                return False
        return True

    def parse_test_sched_args(args):
        delay, numscan, scanlen, gaplen = args
        try:
            delay = int(delay)
        except ValueError:
            return "DELAY should be an integer"
        try:
            nscan = int(numscan)
        except ValueError:
            return "NUMSCAN should be an integer"
        try:
            lscan = int(scanlen)
        except ValueError:
            return "SCANLEN should be an integer"
        try:
            gaplen = int(gaplen)
        except ValueError:
            return "GAPLEN should be an integer"
        return {"delay": delay, "numscan": nscan, "scanlen": lscan, "gaplen": gaplen}
    def arg_type_md5sum(arg):
        if not len(arg) == 7 or any(a not in string.hexdigits for a in arg):
            raise argparse.ArgumentTypeError("md5sum should contain exactly 7 hexadecimal digits")
        return arg
    def arg_type_vexfilename(arg):
        if not arg.lower().endswith(".vex"):
            raise argparse.ArgumentTypeError("VEX file should have .vex extension")
        if not os.path.basename(arg) == arg:
            raise argparse.ArgumentTypeError("VEX file should only be the basename (full path not allowed)")
        return arg
    arg_parser = ArgumentParser(description="run a schedule",
      epilog="This script will create a test vex-file in the schedule trigger-area.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="FILE", required=True,
      help="backend config file")
    arg_parser.add_argument("--sched-param", dest="test", metavar=("DELAY", "NUMSCAN", "SCANLEN", "GAPLEN"), nargs=4, default=(120, 2, 120, 30),
      help="schedule parameters")
    arg_parser.add_argument("vexfile", metavar="vex-file", nargs="?", type=arg_type_vexfilename,
      help="basename of VEX file to load")
    args = arg_parser.parse_args()
    if args.test:
        result = parse_test_sched_args(args.test)
        if isinstance(result, str):
            arg_parser.exit(status=1, message=arg_parser.format_usage() + \
            "%s: error: argument --test-sched: %s\n" % (arg_parser.prog, result))
        setattr(args, "test", result)
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
