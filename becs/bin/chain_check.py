try:
    import collections
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import config

    import becs.config
    import becs.bdc
    import becs.mark6
    import becs.r2dbe
    import becs.interfaces

    import becs.ui as ui

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("test")
    class SingleBandBDC(becs.bdc.SingleBandBDC):
        pass

    @ui.intercept_log("test")
    class DualBandBDC(becs.bdc.DualBandBDC):
        pass

    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class Mark6(becs.mark6.Mark6):
        pass

    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class R2DBE(becs.r2dbe.R2DBE):
        pass

    @ui.intercept_log("test_")
    class Interface(becs.interfaces.Interface):
        pass

    @ui.intercept_log("test_")
    class InterfaceBDCR2DBE(becs.interfaces.InterfaceBDCR2DBE):
        pass

    @ui.intercept_log("test_")
    class InterfaceR2DBEMark6(becs.interfaces.InterfaceR2DBEMark6):
        pass

    # mapping from layout parameter value to function that will return device class instance
    _layout_get_obj = {
      "bdc_1band": lambda name, cfg: becs.config.get_bdc_1band_inst(name, cfg, SingleBandBDC),
      "bdc_2band": lambda name, cfg: becs.config.get_bdc_2band_inst(name, cfg, DualBandBDC),
      "mark6": lambda name, cfg: becs.config.get_mark6_inst(name, cfg, Mark6),
      "r2dbe": lambda name, cfg: becs.config.get_r2dbe_inst(name, cfg, R2DBE),
    }

    def get_all_dev_obj_param(cfg, devnames):
        devobjs = collections.OrderedDict()
        if not devnames:
            devnames = becs.config.get_all_device_names(cfg)
        for devname in devnames:
            dev = cfg.devices[devname]
            if dev["layout"] not in list(_layout_get_obj.keys()):
                continue
            obj = _layout_get_obj[dev["layout"]](devname, cfg)
            if not obj:
                logger.info("%s invalid device specification in config file", devname)
                continue
            devobjs[devname] = (dev["hostname"], obj)
        return devobjs

    _layout_get_iface = {
      "bdc_1band>>r2dbe": lambda send, recv: InterfaceBDCR2DBE(send, recv),
      "bdc_2band>>r2dbe": lambda send, recv: InterfaceBDCR2DBE(send, recv),
      "r2dbe>>mark6": lambda send, recv: InterfaceR2DBEMark6(send, recv),
      "dbbc3>>mark6": lambda send, recv: Interface(send, recv),
    }

    def get_interface_instance(cfg, devobjs, send, recv):
        send_devname, tx_port = send.split(cfg.delim_port)
        recv_devname, rx_port = recv.split(cfg.delim_port)
        ls = cfg.devices[send_devname]["layout"]
        lr = cfg.devices[recv_devname]["layout"]
        try:
            init = _layout_get_iface[">>".join([ls, lr])]
        except KeyError:
            pass
        else:
            tx_host, tx_obj = devobjs[send_devname]
            rx_host, rx_obj = devobjs[recv_devname]
            iface = init((tx_obj, tx_port), (rx_obj, rx_port))
            return iface

    def _parallel_main(link_interface_args):
        link, interface, args = link_interface_args
        test = getattr(interface, "test_" + args.chk_type)
        return test()

    def main(args):
        cfg = config.Config(args.conffile)
        if not args.chains:
            args.chains = list(cfg.chains.keys())
        # create dictionary of all device instances
        devnames = []
        for chain in args.chains:
            try:
                links = cfg.chains[chain]
            except KeyError:
                logger.error("chain %s not in config file, skipping", chain)
                args.chains.remove(chain)
                continue
            for link in links:
                ends = link.split(cfg.delim_link)
                for end in ends:
                    devname, port = end.split(cfg.delim_port)
                    if devname not in devnames:
                        devnames.append(devname)
        if not devnames:
            logger.error("no valid chains, nothing to test")
            return False
        devobjs = get_all_dev_obj_param(cfg, devnames)
        # mark all devices that are not available
        for devname, host_obj in list(devobjs.items()):
            host, obj = host_obj
            if not obj.test_device_reachable():
                devobjs[devname] = (host, None)
        # create list of interface instances
        interfaces = {}
        # start building structures for reporting
        chain_from_link = {}
        outcome_per_link = {}
        result = True
        for chain in args.chains:
            links = cfg.chains[chain]
            for link in links:
                chain_from_link[link] = chain
                send, recv = link.split(cfg.delim_link)
                devsend, portsend = send.split(cfg.delim_port)
                devrecv, portrecv = recv.split(cfg.delim_port)
                if devsend not in list(devobjs.keys()) or devrecv not in list(devobjs.keys()):
                    outcome_per_link[link] = "unsupported (device)"
                    continue
                tx_host, tx_obj = devobjs[devsend]
                rx_host, rx_obj = devobjs[devrecv]
                if tx_obj and rx_obj:
                    interface = get_interface_instance(cfg, devobjs, send, recv)
                    if not interface:
                        logger.info("no interface defined for link %s in chain %s, skipping %s test", link, chain, args.chk_type)
                        outcome_per_link[link] = "unsupported (interface)"
                        continue
                    if not hasattr(interface, "test_" + args.chk_type):
                        logger.info("%s does not implement the specified test, skipping %s test", interface, args.chk_type)
                        outcome_per_link[link] = "unsupported (test)"
                        continue
                    logger.info("%s performing device-level tests ...", interface)
                    test = getattr(interface, "test_" + args.chk_type)
                    if not interface.testgroup_pretest(test):
                        logger.info("%s one or more device-level tests failed on this interface, aborting interface %s test", interface, args.chk_type)
                        outcome_per_link[link] = ui.colorise_outcomes("aborted interface-test (device-level test(s) failed)", failure=["aborted"])
                        continue
                    logger.info("%s ... device-level tests passed", interface)
                    # only add interfaces that are ready to test
                    interfaces[link] = interface
                    logger.debug("%s added to interface list", interface)
                    outcome_per_link[link] = ""
                    continue
                else:
                    missing_devices = []
                    if not devobjs[devsend][1]:
                        missing_devices.append("%s (hostname: %s) device unreachable" % (devsend, tx_host))
                    if not devobjs[devrecv][1]:
                        missing_devices.append("%s (hostname: %s) device unreachable" % (devrecv, rx_host))
                    logger.info("one or more devices not reachable on link %s in chain %s, cannot test %s:\n     %s", link, chain, args.chk_type, "\n     ".join(missing_devices))
                    outcome_per_link[link] = "unreachable device(s)"
                    # unreachable devices result in immediate test failure
                    result = False
        # now perform tests and update final result
        links_interfaces_args = [(k, v, args) for k, v in list(interfaces.items())]
        results = list(map(_parallel_main, links_interfaces_args))
        result = result and all(results)
        # complete outcome_per_link using test results
        for r, lia in zip(results, links_interfaces_args):
            outcome_per_link[lia[0]] = ui.colorise_outcomes("passed" if r else "failed")
        # compile summary
        summary = ""
        outcome_per_chain = {}
        for link, outcome in list(outcome_per_link.items()):
            # skip outcomes for "unsupported (anything)"
            if outcome.startswith("unsupported"):
                continue
            chain = chain_from_link[link]
            if chain not in list(outcome_per_chain.keys()):
                outcome_per_chain[chain] = []
            outcome_per_chain[chain].append(("\n        %s on link %s", outcome, link))
        summary = ""
        for chain, outcomes in list(outcome_per_chain.items()):
            sorted_outcomes = sorted(outcomes, key=lambda o: cfg.chains[chain].index(o[-1]))
            summary = summary + "\n    chain %s:%s\n" % (chain, "".join([o[0] % o[1:] for o in sorted_outcomes]))
        logger.info("Summary of %s test results:%s", args.chk_type, summary)
        return result

    all_chk_types = ["routing", "timesync"]
    arg_parser = ArgumentParser(description="perform checks on a chain",
      epilog="If no chains given, do for all.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    arg_parser.add_argument("--chk-type", dest="chk_type", metavar="chk-type", choices=all_chk_types, required=True,
      help="set the check to perform (one of {'%s'})" % ("', '".join(all_chk_types)))
    arg_parser.add_argument("chains", metavar="chain-name", nargs="*",
      help="names of chains to check")
    args = arg_parser.parse_args()
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
