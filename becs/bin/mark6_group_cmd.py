try:
    import multiprocessing
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import config

    import becs.config
    import becs.ui as ui
    import becs.mark6 as device
    from becs.mark6 import VSIError

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class Mark6(device.Mark6):
        pass

    # make instance generator function
    get_mark6_inst = lambda name, cfg: becs.config.get_mark6_inst(name, cfg, Mark6)

    def _parallel_main(cfg_devname_op):
        try:
            cfg, devname, op = cfg_devname_op
            mark6 = get_mark6_inst(devname, cfg)
            host = cfg.devices[devname]["hostname"]
            if not mark6:
                logger.info("%s invalid device specification in config file, aborting", devname)
                return False
            if not mark6.test_device_reachable():
                logger.info("%s (hostname: %s) is not reachable, aborting", devname, host)
                return False
            if not mark6.test_cplane_running():
                logger.info("%s (hostname: %s) cplane not running, aborting", devname, host)
                return False
            # first group ref for each module
            try:
                group_refs = [mark6.cp_mstat(m)["group_ref"] for m in mark6._mod_grp]
            except VSIError as vsie:
                logger.error(str(vsie))
                return False
            # if all modules have no group ref, make new group
            if all(r == "-" for r in group_refs):
                logger.debug("%s (hostname: %s) all modules have group_ref '-', making new group", devname, host)
                try:
                    mark6.cp_group_new()
                    group_refs = [mark6.cp_mstat(m)["group_ref"] for m in mark6._mod_grp]
                except VSIError as vsie:
                    logger.error(str(vsie))
                    return False
            # now check each module to see if it has correct group ref
            group_ref_issue = False
            for m, gr in zip(mark6._mod_grp, group_refs):
                if not sorted(gr) == sorted(mark6._mod_grp):
                    logger.debug("%s (hostname: %s) slot %s, module group_ref is '%s', expected '%s'", devname, host, m, gr, mark6._mod_grp)
                    group_ref_issue = True
            # filter out all commands that should not complete when group_ref issues detected
            if group_ref_issue and op in ["mount"]:
                logger.info("%s (hostname: %s) modules with incorrect group information found, modules %s may need initialization", devname, host, mark6._mod_grp)
                return False
            # finally ready to perform the requested operation
            if op == "mount":
                call = mark6.cp_group_mount
            elif op == "unmount":
                call = mark6.cp_group_unmount
            else:
                logger.info("%s unsupported group command '%s'", devname, op)
                return False
            try:
                result = call()
            except VSIError as vsie:
                logger.error(str(vsie))
                return False
            return True
        except Exception as ex:
            exc_type, exc_value, exc_tb = sys.exc_info()
            out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
            out = "\n".join("  !!! " + line for line in out.strip().splitlines())
            out = "  !!! Caught %s while processing device %s: %s\n" % (ex.__class__.__name__, devname, str(ex)) + out
            logger.error(out)
            return False

    def main(args):
        cfg = config.Config(args.conffile)
        if not args.devnames:
            devices = [k for k, v in list(cfg.devices.items()) if v["layout"] == "mark6"]
            setattr(args, "devnames", devices)
        cfg_devname_op = [(cfg, devname, args.cmd_type) for devname in args.devnames]
        p = multiprocessing.Pool(max(1, len(cfg_devname_op)))
        results = p.map(_parallel_main, cfg_devname_op)
        if not all(results):
            if any(results):
                logger.info("%s group succeeded on the following devices:\n    %s", args.cmd_type, "\n    ".join([f[1] for f in [cfg_devname_op[i] for i, r in enumerate(results) if r]]))
            logger.info("%s group failed on the following devices:\n    %s", args.cmd_type, "\n    ".join([f[1] for f in [cfg_devname_op[i] for i, r in enumerate(results) if not r]]))
            return False
        logger.info("%s group succeeded on all devices", args.cmd_type)
        return True

    all_cmd_types = ["mount", "unmount"]
    arg_parser = ArgumentParser(description="execute mark6 cplane group command",
      epilog="Module group reference is determined as the set of modules specified in the 'modules' field of each device instance in config-file. " + \
      "For initialized modules the user is prompted to confirm forming a new group.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    arg_parser.add_argument("--cmd-type", dest="cmd_type", metavar="cmd-type", choices=all_cmd_types, required=True,
      help="set the command to execute (one of {'%s'})" % ("', '".join(all_cmd_types)))
    arg_parser.add_argument("devnames", metavar="device-name", nargs="*",
      help="names of devices to configure")
    args = arg_parser.parse_args()
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
