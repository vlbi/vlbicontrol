try:
    import datetime
    import glob
    import multiprocessing
    import os
    import string
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import redis

    import config

    import becs.config
    import becs.ui as ui
    import becs.mark6 as device

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class Mark6(device.Mark6):
        pass

    # make instance generator function
    get_mark6_inst = lambda name, cfg: becs.config.get_mark6_inst(name, cfg, Mark6)

    def delete_sched_in_redis(host):
        try:
            r = redis.Redis()
            r.delete("%s.sched" % host)
        except redis.RedisError as re:
            logger.error("%s (hostname: %s) could not remove loaded schedule in redis: %s", devname, host, str(re))

    def _parallel_main(cfg_devname_args):
        try:
            cfg, devname, args = cfg_devname_args
            mark6 = get_mark6_inst(devname, cfg)
            host = cfg.devices[devname]["hostname"]
            if not mark6:
                logger.info("%s invalid device specification in config file, aborting", devname)
                return False
            logger.info("%s (hostname: %s) checking if device is reachable ...", devname, host)
            if not mark6.test_device_reachable():
                logger.info("%s (hostname: %s) is not reachable, aborting", devname, host)
                return False
            logger.info("%s (hostname: %s) ... device is reachable", devname, host)
            logger.info("%s (hostname: %s) checking if scheduler running ...", devname, host)
            if mark6.test_no_sched_running():
                logger.info("%s (hostname: %s) no scheduler running, no further action needed", devname, host)
                delete_sched_in_redis(host)
                return True
            logger.info("%s (hostname: %s) ... scheduler running, will be terminated and pending / running recordings aborted", devname, host)
            if not mark6.unload_sched():
                logger.info("%s (hostname: %s) could not unload schedule", devname, host)
                return False
            delete_sched_in_redis(host)
            return True
        except Exception as ex:
            exc_type, exc_value, exc_tb = sys.exc_info()
            out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
            out = "\n".join("  !!! " + line for line in out.strip().splitlines())
            out = "  !!! Caught %s while processing device %s: %s\n" % (ex.__class__.__name__, devname, str(ex)) + out
            logger.error(out)
            return False

    def main(args):
        cfg = config.Config(args.conffile)
        stationcode = cfg.instruct["stationcode"]
        # start parallel processing of mark6s
        if not args.devnames:
            devices = [k for k, v in list(cfg.devices.items()) if v["layout"] == "mark6"]
            setattr(args, "devnames", devices)
        # first prompt user to confirm unloading schedule
        msg = ui.format("About to unload schedule on the following Mark6s:\n  > ", color="y")
        msg += ui.format(",".join(args.devnames), color="y")
        msg += ui.format("\nAfter unloading,\n  - ", color="y")
        msg += ui.format("NO FURTHER SCHEDULED SCANS WILL BE PROCESSED", color="y", bold=True)
        msg += ui.format("\n  - ", color="y")
        msg += ui.format("PENDING / RUNNING RECORDINGS ABORTED", color="y", bold=True)
        msg += ui.format("\nContinue? (y/n):", color="y")
        while True:
            response = ui.prompt(msg)
            if response.lower() == "y":
                break
            elif response.lower() == "n":
                logger.info("schedule unload cancelled")
                return False
            logger.info("(invalid input)")
        # continue with unload
        cfg_devname_args = [(cfg, devname, args) for devname in args.devnames]
        p = multiprocessing.Pool(max(1, len(cfg_devname_args)))
        results = p.map(_parallel_main, cfg_devname_args)
        if not all(results):
            if any(results):
                logger.info("schedule unload succeeded on the following devices:\n    %s", "\n    ".join([f[1] for f in [cfg_devname_args[i] for i, r in enumerate(results) if r]]))
            logger.info("schedule unload failed on the following devices:\n    %s", "\n    ".join([f[1] for f in [cfg_devname_args[i] for i, r in enumerate(results) if not r]]))
            return False
        logger.info("schedule unload succeeded on all devices")
        return True

    arg_parser = ArgumentParser(description="unload schedule on mark6",
      epilog="This script will stop any running instances of the schedule executor on the given devices. " + \
      "A record=off command is also issued to cplane, and may also unqueue pending / stop running recordings issued outside the schedule executor. " + \
      "If no devices are specified, the operation is performed for all mark6s found in the config file.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    arg_parser.add_argument("devnames", metavar="device-name", nargs="*",
      help="names of devices to configure")
    args = arg_parser.parse_args()
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
