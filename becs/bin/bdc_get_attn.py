try:
    import argparse
    import multiprocessing
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import config

    import becs.config
    import becs.ui as ui
    import becs.bdc as device

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("test")
    class SingleBandBDC(device.SingleBandBDC):
        pass

    @ui.intercept_log("test")
    class DualBandBDC(device.DualBandBDC):
        pass

    # make instance generator function
    get_bdc_1band_inst = lambda name, cfg: becs.config.get_bdc_1band_inst(name, cfg, SingleBandBDC)
    get_bdc_2band_inst = lambda name, cfg: becs.config.get_bdc_2band_inst(name, cfg, DualBandBDC)

    def _parallel_main(cfg_devname_outp):
        try:
            cfg, devname, outp = cfg_devname_outp
            host = cfg.devices[devname]["hostname"]
            if cfg.devices[devname]["layout"] == "bdc_1band":
                bdc = get_bdc_1band_inst(devname, cfg)
                _attn_name_map = becs.config.get_bdc_1band_attn_from_outport
            elif cfg.devices[devname]["layout"] == "bdc_2band":
                bdc = get_bdc_2band_inst(devname, cfg)
                _attn_name_map = becs.config.get_bdc_2band_attn_from_outport
            else:
                logger.info("%s (hostname: %s) is not a bdc", devname, host)
                return (devname, {})
            if not outp:
                outp = [o.split(cfg.delim_port)[-1] for o in cfg.devices[devname]["outports"]]
            attn_names = [_attn_name_map(o) for o in outp]
            logger.debug("%s (hostname: %s) reading the following attenuator values:\n  - %s\n", devname, host, "\n  - ".join(n for n in attn_names))
            if not bdc.test_device_reachable():
                logger.info("%s (hostname: %s) not reachable, aborting", devname, host)
                return (devname, {})
            attn_values = bdc.get_attn(*attn_names)
            if not attn_values:
                logger.info("%s (hostname: %s) reading attenuator values failed", devname, host)
                return (devname, {})
            logger.debug("%s (hostname: %s) attenuator values were read successfully", devname, host)
            return (devname, attn_values)
        except Exception as ex:
            exc_type, exc_value, exc_tb = sys.exc_info()
            out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
            out = "\n".join("  !!! " + line for line in out.strip().splitlines())
            out = "  !!! Caught %s while processing device %s: %s\n" % (ex.__class__.__name__, devname, str(ex)) + out
            logger.error(out)
            return (devname, {})

    def main(args):
        cfg = config.Config(args.conffile)
        if not args.devices:
            devices_ports = [(k, []) for k, v in list(cfg.devices.items()) if v["layout"] in ["bdc_1band", "bdc_2band"]]
            setattr(args, "devices", dict(devices_ports))
        cfg_devname_outp = [(cfg, k, v) for k, v in list(args.devices.items())]
        p = multiprocessing.Pool(max(1, len(cfg_devname_outp)))
        results = p.map(_parallel_main, cfg_devname_outp)
        failed = []
        for r in results:
            devname, attn_values = r
            if not attn_values:
                failed.append(devname)
                continue
            width = 10
            out_str = "%{0:d}s: %-{0:d}s %-{0:d}s %-{0:d}s\n".format(width) % (devname, "port", "attn", "value")
            for attn, value in list(attn_values.items()):
                if cfg.devices[devname]["layout"] == "bdc_1band":
                    _attn_name_map = becs.config.get_bdc_1band_outport_from_attn
                elif cfg.devices[devname]["layout"] == "bdc_2band":
                    _attn_name_map = becs.config.get_bdc_2band_outport_from_attn
                outp = _attn_name_map(attn)
                out_str += "%{0:d}s  %-{0:d}s %-{0:d}s %{0:d}.1f dB\n".format(width) % ("", outp, attn, value)
            logger.info(out_str)
        if failed:
            logger.info("Attenuator reading failed on the following devices:\n    %s", "\n    ".join(failed))
        return not failed

    def arg_type_port(arg):
        dev_port = arg.split(":")
        if not len(dev_port) == 2 or not dev_port[0] or not dev_port[1]:
            raise argparse.ArgumentTypeError("port should be in the format <dn>:<pn>")
        return tuple(dev_port)
    arg_parser = ArgumentParser(description="get bdc attenuators",
      epilog="Ports are specified in full as defined in the config-file, and have the form <device-name>:<port-name>. " + \
      "If no ports given, return the values for all attenuators for all bdc devices.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file",
      help="backend config file")
    arg_parser.add_argument("ports", nargs="*", metavar="port", type=arg_type_port,
      help="add port to operand list")
    args = arg_parser.parse_args()
    devices = {}
    for dev, port in args.ports:
        if dev not in list(devices.keys()):
            devices[dev] = []
        devices[dev].append(port)
    setattr(args, "devices", devices)
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
