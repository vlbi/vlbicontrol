try:
    import datetime
    import glob
    import multiprocessing
    import os
    import string
    import sys
    import time
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import redis

    import config

    import vexparser
    import vexinterpreter

    import becs.config
    import becs.ui as ui
    import becs.mark6 as device

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class Mark6(device.Mark6):
        pass

    # make instance generator function
    get_mark6_inst = lambda name, cfg: becs.config.get_mark6_inst(name, cfg, Mark6)

    def _parallel_main(cfg_devname_args):
        try:
            cfg, devname, args = cfg_devname_args
            xml = args.vexfile.replace(".vex", ".xml")
            mark6 = get_mark6_inst(devname, cfg)
            host = cfg.devices[devname]["hostname"]
            if not mark6:
                logger.info("%s invalid device specification in config file, aborting", devname)
                return False
            logger.info("%s (hostname: %s) checking if device is reachable ...", devname, host)
            if not mark6.test_device_reachable():
                logger.info("%s (hostname: %s) is not reachable, aborting", devname, host)
                return False
            logger.info("%s (hostname: %s) ... device is reachable", devname, host)
            logger.info("%s (hostname: %s) checking if scheduler already running ...", devname, host)
            if not mark6.test_no_sched_running():
                if mark6.test_sched_running(xml):
                    logger.info("%s (hostname: %s) scheduler already running '%s', no further action needed", devname, host, xml)
                    return True
                else:
                    logger.info("%s (hostname: %s) already running a different schedule, aborting (stop running schedule and try again if necessary)", devname, host)
                    return False
            logger.info("%s (hostname: %s) ... device is not already running scheduler", devname, host)
            logger.info("%s (hostname: %s) running post-config tests ...", devname, host)
            if not mark6.testgroup_postconfig():
                logger.info("%s (hostname: %s) did not pass post-configure tests, aborting", devname, host)
                return False
            logger.info("%s (hostname: %s) ... post-config tests passed", devname, host)
            logger.info("%s (hostname: %s) loading schedule ...", devname, host)
            if not mark6.load_sched(args.vexpath):
                logger.info("%s (hostname: %s) schedule loading failed", devname, host)
                return False
            logger.info("%s (hostname: %s) ... schedule loading succeeded.", devname, host)
            logger.info("%s (hostname: %s) verifying running schedule ...", devname, host)
            if not mark6.test_sched_running(xml):
                logger.info("%s (hostname: %s) schedule is not running", devname, host)
                return False
            logger.info("%s (hostname: %s) ... running schedule verified", devname, host)
            # at this point schedule is confirmed to be running, report ot redis
            endat = args.vexobj.schedule.scans[-1].stop.timestamp()
            now = time.time()
            ttl = int(endat - now + 5)
            r = redis.Redis()
            key = ".".join([host, "sched"])
            value = repr(args.vexobj)
            try:
                r.set(key, value, ex=ttl)
            except redis.RedisError as re:
                logger.error("%s (hostname: %s) could not log loaded schedule to redis: %s", devname, host, str(re))
            return True
        except Exception as ex:
            exc_type, exc_value, exc_tb = sys.exc_info()
            out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
            out = "\n".join("  !!! " + line for line in out.strip().splitlines())
            out = "  !!! Caught %s while processing device %s: %s\n" % (ex.__class__.__name__, devname, str(ex)) + out
            logger.error(out)
            return False

    def get_valid_schedules(cfg, area="trigger"):
        stationcode = cfg.instruct["stationcode"]
        valids = []
        # look for schedules in the trigger area
        candidates = glob.glob(os.sep.join([cfg.instruct["vexstore"], area, "*.vex"]))
        if not candidates:
            logger.info("no vex-files found in %s area", area)
            return valids
        # filter out invalid schedules
        now = datetime.datetime.utcnow()
        for candidate in candidates:
            vex_dict = vexparser.vexparser(candidate)
            vex = vexinterpreter.Vex(vex_dict)
            vex.schedule.set_station(stationcode)
            # any scans?
            if not vex.schedule.scans:
                logger.debug("candidate schedule %s contains no scans for station %s, invalid", candidate, stationcode)
                continue
            # scans with future end date? (only check last scan)
            if vex.schedule.scans[-1].stop <= now:
                logger.debug("candidate schedule %s last scan for %s ends at %s, invalid", candidate, stationcode, vex.schedule.scans[-1].stop.strftime("%Yy%jd-%Hh%Mm%Ss"))
                continue
            logger.info("candidate schedule %s is valid for station %s", candidate, stationcode)
            valids.append(candidate)
        return valids

    def main(args):
        cfg = config.Config(args.conffile)
        stationcode = cfg.instruct["stationcode"]
        # find schedule full path
        if args.testvex:
            # use test schedule ...
            full_path = os.sep.join([cfg.instruct["vexstore"], "testing", args.testvex])
            valids = get_valid_schedules(cfg, area="testing")
            if not full_path in valids:
                logger.info("schedule '%s' is invalid (no scans for station '%s' with future end-time)", full_path, stationcode)
                return False
            msg = "using TEST schedule: %s" % full_path
            logger.info(ui.format(msg, color="y", bold=True))
        else:
            # ... otherwise get valid schedules from trigger area
            valids = get_valid_schedules(cfg, area="trigger")
            if not len(valids) == 1:
                logger.info("found %d valid schedules in trigger area, there should be exactly 1, aborting", len(valids))
                return False
            full_path = valids[0]
        setattr(args, "vexfile", os.path.basename(full_path))
        setattr(args, "vexpath", full_path)
        vex_dict = vexparser.vexparser(full_path)
        vex = vexinterpreter.Vex(vex_dict)
        vex.schedule.set_station(stationcode)
        # add vex to arguments to be used in parallel processing
        setattr(args, "vexobj", vex)
        scan_list_str = "\n    ".join(["%10d   %s   %s   %9ds   %10s" % (i, s.start.strftime("%Yy%jd-%Hh%Mm%Ss"), s.stop.strftime("%jd-%Hh%Mm%Ss"), s.duration, s.source) for i, s in enumerate(vex.schedule.scans)])
        hdr_str = "%10s   %19s   %14s   %10s   %10s" % ("#scan", "start", "stop", "duration", "source")
        logger.info("schedule file %s contains the following scans for station '%s':\n    %s\n    %s", args.vexfile, stationcode, hdr_str, scan_list_str)
        # start parallel processing of mark6s
        if not args.devnames:
            devices = [k for k, v in list(cfg.devices.items()) if v["layout"] == "mark6"]
            setattr(args, "devnames", devices)
        cfg_devname_args = [(cfg, devname, args) for devname in args.devnames]
        p = multiprocessing.Pool(max(1, len(cfg_devname_args)))
        results = p.map(_parallel_main, cfg_devname_args)
        if not all(results):
            if any(results):
                logger.info("schedule load succeeded on the following devices:\n    %s", "\n    ".join([f[1] for f in [cfg_devname_args[i] for i, r in enumerate(results) if r]]))
            logger.info("schedule load failed on the following devices:\n    %s", "\n    ".join([f[1] for f in [cfg_devname_args[i] for i, r in enumerate(results) if not r]]))
            return False
        logger.info("schedule load succeeded on all devices")
        return True

    arg_parser = ArgumentParser(description="load schedule on mark6",
      epilog="This script will automatically select a valid schedule from the trigger area to load, unless the --test-vex option is invoked in which case the given TESTVEX in the testing area is used." + \
      "A valid schedule is defined as having one or more scans for this station that have a future end-time. " + \
      "An error is reported if the number of valid schedules in the trigger area is not exactly equal to 1. " + \
      "If no devices are specified, the operation is performed for all mark6s found in the config file.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    arg_parser.add_argument("--test-vex", dest="testvex", metavar="TESTVEX",
      help="test schedule filename (basename) to run")
    arg_parser.add_argument("devnames", metavar="device-name", nargs="*",
      help="names of devices to configure")
    args = arg_parser.parse_args()
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
