try:
    import argparse
    import multiprocessing
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import config

    import becs.config
    import becs.ui as ui
    import becs.mark6 as device
    from becs.mark6 import VSIError

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class Mark6(device.Mark6):
        pass

    # make instance generator function
    get_mark6_inst = lambda name, cfg: becs.config.get_mark6_inst(name, cfg, Mark6)

    def _parallel_main(obj_slot_kwargs):
        try:
            mark6, slot, kwargs = obj_slot_kwargs
            try:
                if not mark6.cp_mod_init(slot, **kwargs):
                    logger.info("%s slot %d mod_init failed, cp_mod_init returned False", mark6.host, slot)
                    return False
                # if no failure, check if msn correct
                mstat = mark6.cp_mstat(slot)
            except VSIError as vsie:
                logger.error(str(vsie))
                return False
            else:
                msn = mstat["MSN"]
                logger.debug("%s slot %d MSN is '%s', expect '%s'", mark6.host, slot, msn, kwargs["msn"])
                if not msn == kwargs["msn"]:
                    logger.info("%s slot %d mod_init failed, MSN mismatch", args.devname, host, slot, msn)
                    return False
                return True
        except Exception as ex:
            exc_type, exc_value, exc_tb = sys.exc_info()
            out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
            out = "\n".join("  !!! " + line for line in out.strip().splitlines())
            out = "  !!! Caught %s while processing device %s: %s\n" % (ex.__class__.__name__, mark6.host, str(ex)) + out
            logger.error(out)
            return False

    def main(args):
        cfg = config.Config(args.conffile)
        host = cfg.devices[args.devname]["hostname"]
        mark6 = get_mark6_inst(args.devname, cfg)
        if not mark6:
            logger.info("%s invalid device specification in config file, aborting", args.devname)
            return False
        if not mark6.test_device_reachable():
            logger.info("%s (hostname: %s) is not reachable, aborting", args.devname, host)
            return False
        if not mark6.test_cplane_running():
            logger.info("%s (hostname: %s) cplane not running, aborting", args.devname, host)
            return False
        # first loop over all slots to get input and filter out errors
        cp_mod_init_params = []
        for slot, msn in list(args.slot_msn.items()):
            # first get disk info
            try:
                disk_info = mark6.cp_disk_info(slot, "serial")
            except VSIError as vsie:
                logger.error(str(vsie))
                logger.info("%s (hostname: %s) slot %d encountered an error, aborting mod_init", args.devname, host, slot)
                continue
            # check no disks present
            ndisk = len(disk_info["serial"])
            ndash = disk_info["serial"].count("-")
            if ndash:
                if ndash == ndisk:
                    logger.info("%s (hostname: %s) slot %d, no disks detected (is module installed and are cables connected?)", args.devname, host, slot)
                elif ndash == 4:
                    logger.info("%s (hostname: %s) slot %d, only %d / %d disks detected (perhaps a cable is not connected?) ", args.devname, host, slot, ndisk - ndash, ndisk)
                else:
                    logger.info("%s (hostname: %s) slot %d, only %d / %d disks detected ", args.devname, host, slot, ndisk - ndash, ndisk)
                logger.info("%s (hostname: %s) slot %d has some disks undetected, aborting mod_init for this slot", args.devname, host, slot)
                continue
            logger.debug("%s (hostname: %s) slot %d detected %d disks", args.devname, host, slot, ndisk)
            # then get module status
            try:
                mstat = mark6.cp_mstat(slot)
            except VSIError as vsie:
                logger.error(str(vsie))
                logger.info("%s (hostname: %s) slot %d encountered an error, aborting mod_init for this slot", args.devname, host, slot)
                continue
            # check module status1 if request should be carried out
            if mstat["status1"] not in ["incomplete", "initialized", "unmounted", "unknown"]:
                logger.info("%s (hostname: %s) slot %d module status1 is '%s', aborting mod_init for this slot (try unmount first)", args.devname, host, slot, mstat["status1"])
                continue
            if mstat["status1"] == "unknown" and not args.new:
                logger.info("%s (hostname: %s) slot %d module status1 is '%s' but --new option omitted, aborting mod_init for this slot", args.devname, host, slot, mstat["status1"])
                continue
            # finally, if provided msn is empty, get it from mstat
            if not msn:
                msn = mstat["MSN"]
                if not len(msn) == 8:
                    logger.info("%s (hostname: %s) slot %d no MSN provided and no existing MSN found on module, aborting mod_init for this slot", args.devname, host, slot)
                    continue
            cp_mod_init_params.append({
              "slot": slot,
              "old_msn": mstat["MSN"],
              "kwargs": {"msn": msn, "new": args.new},
            })
        # abort operation if errors encountered for any slot
        if not len(cp_mod_init_params) == len(list(args.slot_msn.items())):
            logger.info("%s (hostname: %s) errors encountered for one or more slots, aborting mod_init for all", args.devname, host)
            return False
        # now process mod_inits
        obj_slot_kwargs = [(mark6, v["slot"], v["kwargs"]) for v in cp_mod_init_params]
        result = True
        if obj_slot_kwargs:
            # prompt user to confirm initialisation
            msg = ui.format("Module(s) %s are about to be initialised, " % (",".join(str(par["slot"]) for par in cp_mod_init_params)), color="y")
            if args.new:
                msg += ui.format("ALL DATA WILL BE ERASED", color="y", bold=True)
            else:
                msg += ui.format("METADATA WILL BE RESET", color="y", bold=True)
            msg += ui.format(", continue? (y/n):", color="y")
            while True:
                response = ui.prompt(msg)
                if response.lower() == "y":
                    break
                elif response.lower() == "n":
                    logger.info("%s (hostname: %s) module initialisation cancelled", args.devname, host)
                    return False
                logger.info("(invalid input)")
            # check if MSNs change
            changed_msns = []
            # Prompt the user to confirm MSNs
            msg = ui.format("The following module serial numbers will be written:", color="y")
            for i in cp_mod_init_params:
                # if changed MSNs, write the change explicitly
                if not i["old_msn"] == i["kwargs"]["msn"]:
                    msg += ui.format("\n  --> slot %d: '%s' will be CHANGED to '%s'" % (i["slot"], i["old_msn"], i["kwargs"]["msn"]), color="y", bold=True)
                # ... otherwise just write the result
                else:
                    msg += ui.format("\n  --> slot %d: '%s'" % (i["slot"], i["kwargs"]["msn"]), color="y", bold=True)
            msg += ui.format("\nOnly proceed if the module serial numbers are correct. Continue? (y/n):", color="y")
            while True:
                response = ui.prompt(msg)
                if response.lower() == "y":
                    break
                elif response.lower() == "n":
                    logger.info("%s (hostname: %s) module initialisation cancelled", args.devname, host)
                    return False
                logger.info("(invalid input)")
            logger.info("%s (hostname: %s) will now initalize the following modules:\n    %s", args.devname, host, "\n    ".join(["%d <-- %s" % (v["slot"], v["kwargs"]["msn"]) for v in cp_mod_init_params]))
            p = multiprocessing.Pool(max(1, len(obj_slot_kwargs)))
            mod_init_results = p.map(_parallel_main, obj_slot_kwargs)
            #~ mod_init_results = map(_parallel_main, obj_slot_kwargs)
            # summary of initialization outcomes
            successful_slots = [osk[1] for osk in [obj_slot_kwargs[i] for i, r in enumerate(mod_init_results) if r]]
            if successful_slots:
                logger.info("%s (hostname: %s) module initialization succeeded for the following modules: %s", args.devname, host, ", ".join([str(s) for s in successful_slots]))
            failed_slots = [osk[1] for osk in [obj_slot_kwargs[i] for i, r in enumerate(mod_init_results) if not r]]
            if failed_slots:
                logger.info("%s (hostname: %s) module initialization failed for the following modules: %s", args.devname, host, ", ".join([str(f) for f in failed_slots]))
                result = False
        aborted_slots = [s for s in list(args.slot_msn.keys()) if s not in [osk[1] for osk in obj_slot_kwargs]]
        if aborted_slots:
            logger.info("%s (hostname: %s) module initialization aborted for the following modules: %s", args.devname, host, ", ".join([str(a) for a in aborted_slots]))
            result = False
        return result

    all_slots = [1, 2, 3, 4]
    def arg_type_slot_msn_spec(arg):
        if "=" in arg:
            try:
                slot, msn = arg.split("=")
            except ValueError:
                raise argparse.ArgumentTypeError("if msn provided for slot argument should be of the form <slot>=<msn>")
            if not len(msn) == 8:
                raise argparse.ArgumentTypeError("msn should be exactly 8 characters in length, received '%s' (%d)" % (msn, len(msn)))
        else:
            slot = arg
            msn = ""
        try:
            slot = int(slot)
            if slot not in all_slots:
                raise ValueError()
        except ValueError:
            raise argparse.ArgumentTypeError("slot should be one of {'%s'}", "', '".join(str(s) for s in all_slots))
        return (slot, msn)
    arg_parser = ArgumentParser(description="initialize modules on mark6",
      epilog="Each slot-msn-spec should be of the form <slot>=<MSN>. " + \
        "Operation is only allowed on single device at a time, and every module slot has to be specified explicitly.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    arg_parser.add_argument("--new", action="store_true",
      help="pass the 'new' argument to cplane (WARNING: using this option erases all data on module)")
    arg_parser.add_argument("devname", metavar="device-name",
      help="name of device")
    arg_parser.add_argument("slot_msn_spec", metavar="slot[=msn]", nargs="+", type=arg_type_slot_msn_spec,
      help="slots to initialise with MSN optionally provided")
    args = arg_parser.parse_args()
    slot_msn = {}
    for slot, msn in args.slot_msn_spec:
        if slot in list(slot_msn.keys()):
            arg_parser.exit(status=1, message=arg_parser.format_usage() + \
              "%s: error: argument slot-msn-spec: slot %d can only be specified once" % (arg_parser.prog, slot))
        slot_msn[slot] = msn
    setattr(args, "slot_msn", slot_msn)
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
