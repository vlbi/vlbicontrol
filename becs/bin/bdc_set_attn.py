try:
    import argparse
    import multiprocessing
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import config

    import becs.config
    import becs.ui as ui
    import becs.bdc as device

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("test")
    class SingleBandBDC(device.SingleBandBDC):
        pass

    @ui.intercept_log("test")
    class DualBandBDC(device.DualBandBDC):
        pass

    # make instance generator function
    get_bdc_1band_inst = lambda name, cfg: becs.config.get_bdc_1band_inst(name, cfg, SingleBandBDC)
    get_bdc_2band_inst = lambda name, cfg: becs.config.get_bdc_2band_inst(name, cfg, DualBandBDC)

    def _parallel_main(cfg_devname_outpattn):
        try:
            cfg, devname, outpattn = cfg_devname_outpattn
            host = cfg.devices[devname]["hostname"]
            if cfg.devices[devname]["layout"] == "bdc_1band":
                bdc = get_bdc_1band_inst(devname, cfg)
                _attn_name_map = becs.config.get_bdc_1band_attn_from_outport
            elif cfg.devices[devname]["layout"] == "bdc_2band":
                bdc = get_bdc_2band_inst(devname, cfg)
                _attn_name_map = becs.config.get_bdc_2band_attn_from_outport
            else:
                logger.info("%s (hostname: %s) is not a bdc", devname, host)
                return False
            attns_vals = dict()
            for outp, attn_value in list(outpattn.items()):
                attn_name = _attn_name_map(outp)
                if not attn_name:
                    continue
                attns_vals[attn_name] = attn_value
            logger.debug("%s (hostname: %s) setting the following attenuator values:\n  - %s\n", devname, host, "\n  - ".join(k + " = " + str(v) for k, v in list(attns_vals.items())))
            if not bdc.test_device_reachable():
                logger.info("%s (hostname: %s) not reachable, aborting", devname, host)
                return False
            try:
                if not bdc.set_attn(**attns_vals):
                    logger.info("%s (hostname: %s) setting attenuator values failed", devname, host)
                    return False
                logger.info("%s (hostname: %s) attenuator values were set successfully", devname, host)
                return True
            except device.BDCRemoteDisabledError as brde:
                logger.info("%s (hostname: %s) remote control disabled; enable remote control, then try again", devname, host)
            except device.BDCTemporaryLockoutError as btle:
                logger.info("%s (hostname: %s) temporary lock-out, manual control in progress; clear and enable remote control; then try again", devname, host)
            except device.BDCParameterOutOfRangeError as bpore:
                logger.info("%s (hostname: %s) invalid attenuator value, '%s'", devname, host, bpore)
            return False
        except Exception as ex:
            exc_type, exc_value, exc_tb = sys.exc_info()
            out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
            out = "\n".join("  !!! " + line for line in out.strip().splitlines())
            out = "  !!! Caught %s while processing device %s: %s\n" % (ex.__class__.__name__, devname, str(ex)) + out
            logger.error(out)
            return False

    def main(args):
        cfg = config.Config(args.conffile)
        cfg_devname_outpattn = [(cfg, k, v) for k, v in list(args.devices.items())]
        p = multiprocessing.Pool(max(1, len(cfg_devname_outpattn)))
        results = p.map(_parallel_main, cfg_devname_outpattn)
        if not all(results):
            if any(results):
                logger.info("Attenuator setting succeeded on the following devices:\n    %s", "\n    ".join([f[1] for f in [cfg_devname_outpattn[i] for i, r in enumerate(results) if r]]))
            logger.info("Attenuator setting failed on the following devices:\n    %s", "\n    ".join([f[1] for f in [cfg_devname_outpattn[i] for i, r in enumerate(results) if not r]]))
            return False
        logger.info("Attenuator setting succeeded on all devices")
        return True

    def arg_type_portval(arg):
        valid_attn = [x/2.0 for x in range(0, 64)]
        devport_val = arg.split("=")
        if not len(devport_val) == 2 or not devport_val[0] or not devport_val[1]:
            raise argparse.ArgumentTypeError("port-value should be in the format <dn>:<pn>=<val>")
        value = devport_val[1]
        dev_port = devport_val[0].split(":")
        if not len(dev_port) == 2 or not dev_port[0] or not dev_port[1]:
            raise argparse.ArgumentTypeError("port should be in the format <dn>:<pn>")
        dev = dev_port[0]
        port = dev_port[1]
        try:
            attn = float(value)
            if attn not in valid_attn:
                raise ValueError()
        except ValueError:
            raise argparse.ArgumentTypeError("value should be one of {0.0, 0.5, ..., 31.5} but got %s" % value)
        return (dev, port, attn)
    arg_parser = ArgumentParser(description="set bdc attenuators",
      epilog="Each port=value should be of the form <dn>:<pn>=<value>. " + \
      "Each <value> should be an integer multiple of 0.5 in the range [0, 31.5].")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    arg_parser.add_argument("portvals", metavar="port=value", nargs="+", type=arg_type_portval,
      help="add port-value pair to operation")
    args = arg_parser.parse_args()
    devices = {}
    for dev, port, attn in args.portvals:
        if dev not in list(devices.keys()):
            devices[dev] = {}
        devices[dev][port] = attn
    setattr(args, "devices", devices)
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
