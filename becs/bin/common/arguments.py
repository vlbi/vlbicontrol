import argparse
import sys

import becs.ui as ui

# derive argument parser with custom error-handler to:
#   - write to both stdout and stderr (backendctl interface)
#   - exit with the correct error code
class ArgumentParser(argparse.ArgumentParser):

    def error(self, message):
        out = self.format_usage()
        out += "error : " + message + "\n"
        sys.stdout.write(out)
        sys.stderr.write(out)
        sys.exit(ui.RC_ERROR)
