try:
    import argparse
    import datetime
    import multiprocessing
    import sys
    import time
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import config

    import becs.config
    import becs.ui as ui
    import becs.mark6 as device
    from becs.mark6 import VSIError

    from common import ArgumentParser

    # decorate classes
    @ui.intercept_log("conf_")
    @ui.intercept_log("test_")
    class Mark6(device.Mark6):
        pass

    # make instance generator function
    get_mark6_inst = lambda name, cfg: becs.config.get_mark6_inst(name, cfg, Mark6)

    def _parallel_main(cfg_devname_args):
        try:
            cfg, devname, args = cfg_devname_args
            mark6 = get_mark6_inst(devname, cfg)
            host = cfg.devices[devname]["hostname"]
            if not mark6:
                logger.info("%s invalid device specification in config file, aborting", devname)
                return False
            logger.info("%s (hostname: %s) checking if device is reachable ...", devname, host)
            if not mark6.test_device_reachable():
                logger.info("%s (hostname: %s) is not reachable, aborting", devname, host)
                return False
            logger.info("%s (hostname: %s) ... device is reachable", devname, host)
            logger.info("%s (hostname: %s) running post-config tests ...", devname, host)
            if not mark6.testgroup_postconfig():
                logger.info("%s (hostname: %s) did not pass post-configure tests, aborting", devname, host)
                return False
            logger.info("%s (hostname: %s) ... post-config tests passed", devname, host)
            when = datetime.datetime.utcnow() + datetime.timedelta(seconds=args.delay)
            data_rate = sum([s["payload_size"] * s["packet_rate"] for s in list(mark6._streams.values())])
            data_size = int(args.duration * data_rate/1e9)
            # create scan name if none given
            if not args.scan:
                setattr(args, "scan", when.strftime("%Y%m%d-%H%M%S"))
            # check for duplicate scan already existing
            try:
                scan_list = mark6.cp_list()
            except VSIError as vsie:
                logger.error(str(vsie))
                return False
            else:
                scan_names = [s['name'].strip() for s in scan_list['scans']]
                scan_name = "%s_%s_%s" % (args.exp, mark6._station, args.scan)
                if scan_name in scan_names:
                    logger.info("%s (hostname: %s) scan name '%s' already exists, retry with a different --scan argument (or omit the option to use a timestamp), aborting", devname, host, scan_name)
                    return False
            # issue the record
            try:
                logger.debug("%s (hostname: %s) issuing record when=%s, duration=%d, data_size=%d, scan=%s, exp=%s, code=%s", devname, host, when.strftime("%F %T"), args.duration, data_size, args.scan, args.exp, mark6._station)
                if not mark6.cp_record_at(when, args.duration, data_size, args.scan, args.exp, mark6._station):
                    logger.info("%s (hostname: %s) issuing record command failed, aborting", devname, host)
                    return False
                # check successfully enqueued
                qr = mark6.cp_record_query()
            except VSIError as vsie:
                    logger.error(str(vsie))
                    return False
            else:
                if not qr["scan_name"] == args.scan:
                    logger.info("% (hostname: %s) record query returned different scan name '%s', expected '%s', aborting", devname, host, qr["scan_name"], args.scan)
                    return False
            # wait until done plus 3 seconds
            wait_for = max(5, (when - datetime.datetime.utcnow()).total_seconds() + args.duration + 5)
            logger.info("%s (hostname: %s) recording enqueued for scan '%s', sleeping for %d seconds ... ", devname, host, scan_name, wait_for)
            time.sleep(wait_for)
            logger.info("%s (hostname: %s) ... awake, checking recording results ... ", devname, host)
            # check that recording is off
            try:
                retries = 0
                while True:
                    qr = mark6.cp_record_query()
                    # make sure it is the same scan
                    if not qr["scan_name"] == args.scan:
                        logger.info("% (hostname: %s) record query returned different scan name '%s', expected '%s', aborting", devname, host, qr["scan_name"], args.scan)
                        return False
                    # check for "off" status or try to get it to that state
                    if qr["status"] == "off":
                        break
                    logger.warn("  - %s record status is '%s', expected 'off', trying record=off", host, qr["status"])
                    time.sleep(1)
                    if not mark6.cp_record_off():
                        logger.warn("  - %s record=off failed", host)
                        return False
                    retries += 1
                    if retries > 2:
                        logger.info("%s (hostname: %s) cannot get record to 'off' state, aborting", devname, host)
                        return False
            except VSIError as vsie:
                logger.error(str(vsie))
                return False
            # test whether any issues with the last scan
            result = all([
              mark6.test_wrong_length_packets(),
              mark6.test_fill_rate(),
              mark6.test_recorded_data(),
            ])
            return result
        except Exception as ex:
            exc_type, exc_value, exc_tb = sys.exc_info()
            out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
            out = "\n".join("  !!! " + line for line in out.strip().splitlines())
            out = "  !!! Caught %s while processing device %s: %s\n" % (ex.__class__.__name__, devname, str(ex)) + out
            logger.error(out)
            return False

    def main(args):
        cfg = config.Config(args.conffile)
        if not args.devnames:
            devices = [k for k, v in list(cfg.devices.items()) if v["layout"] == "mark6"]
            setattr(args, "devnames", devices)
        cfg_devname_args = [(cfg, devname, args) for devname in args.devnames]
        p = multiprocessing.Pool(max(1, len(cfg_devname_args)))
        results = p.map(_parallel_main, cfg_devname_args)
        if not all(results):
            if any(results):
                logger.info("recording test succeeded on the following devices:\n    %s", "\n    ".join([f[1] for f in [cfg_devname_args[i] for i, r in enumerate(results) if r]]))
            logger.info("recording test failed on the following devices:\n    %s", "\n    ".join([f[1] for f in [cfg_devname_args[i] for i, r in enumerate(results) if not r]]))
            return False
        logger.info("recording test succeeded on all devices")
        return True

    def arg_type_delay(arg):
        try:
            _min = 5
            if int(arg) < _min:
                raise ValueError()
        except ValueError:
            raise argparse.ArgumentTypeError("DELAY should be > %d" % _min)
        return int(arg)
    def arg_type_exp(arg):
        _max = 8
        if len(arg) > _max:
            raise argparse.ArgumentTypeError("EXP should be at most %d characters long" % _max)
        return arg
    def arg_type_scan(arg):
        _max = 32
        if len(arg) > _max:
            raise argparse.ArgumentTypeError("EXP should be at most %d characters long" % _max)
        return arg
    arg_parser = ArgumentParser(description="perform recording test on mark6",
      epilog="If no devices are specified, the operation is performed for all mark6s found in the config file.")
    arg_parser.add_argument("--config-file", dest="conffile", metavar="config-file", required=True,
      help="backend config file")
    arg_parser.add_argument("--duration", metavar="LEN", type=int, default=120,
      help="length of recording in seconds (default is 120)")
    arg_parser.add_argument("--delay", metavar="SEC", type=arg_type_delay, default=30,
      help="delay start of recording (default is 30)")
    arg_parser.add_argument("--exp", metavar="EXP", type=arg_type_exp, default="test",
      help="experiment name (default is 'test')")
    arg_parser.add_argument("--scan", metavar="NAME", type=arg_type_scan,
      help="scan name (from timestamp if not supplied)")
    arg_parser.add_argument("devnames", metavar="device-name", nargs="*",
      help="names of devices to configure")
    args = arg_parser.parse_args()
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
