try:
    import argparse
    import glob
    import os
    import shutil
    import sys
    import traceback

    import logging
    logger = logging.getLogger(__name__)

    import becs.ui as ui

    from common import ArgumentParser

    def main(args):
        src = os.sep.join([args.staging, args.vexfile])
        if not os.path.exists(src):
            logger.info("failed to trigger %s, no such file", src)
            return False
        if not os.path.exists(args.trigger):
            logging.debug("trigger directory %s does not exist, trying mkdir", args.trigger)
            try:
                os.mkdir(args.trigger)
            except OSError as ose:
                logger.info("failed to trigger, could not mkdir %s: %s", args.trigger, str(ose))
                return False
        dst = os.sep.join([args.trigger, args.vexfile])
        try:
            shutil.copy(src, args.trigger)
        except (FileNotFoundError, PermissionError) as err:
            logger.info("failed to trigger %s: %s", args.vexfile, str(err))
            return False
        if not os.path.exists(dst):
            logger.info("schedule %s trigger failed", args.vexfile)
            return False
        logger.info("schedule %s trigger successful", args.vexfile)
        return True

    def arg_type_vexfilename(arg):
        if not arg.lower().endswith(".vex"):
            raise argparse.ArgumentTypeError("VEX file should have .vex extension")
        if not os.path.basename(arg) == arg:
            raise argparse.ArgumentTypeError("VEX file should only be the basename")
        return arg
    arg_parser = ArgumentParser(description="manually trigger a schedule",
      epilog="The given vexfile is copied from the staging area to the trigger area.")
    arg_parser.add_argument("--staging-path", dest="staging", default="/srv/vexstore/staging",
      help=argparse.SUPPRESS)
    arg_parser.add_argument("--trigger-path", dest="trigger", default="/srv/vexstore/trigger",
      help=argparse.SUPPRESS)
    arg_parser.add_argument("vexfile", type=arg_type_vexfilename,
      help="schedule VEX file to trigger")
    args = arg_parser.parse_args()
    ui.init_ui()
    if main(args):
        exit(ui.RC_OKAY)
    exit(ui.RC_FAIL)
except Exception:
    exc_type, exc_value, exc_tb = sys.exc_info()
    out = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    sys.stdout.write(out)
    sys.stderr.write(out)
    exit(ui.RC_ERROR)
