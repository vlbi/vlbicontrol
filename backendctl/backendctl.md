backendctl
===
syntax

	backendctl [OPTIONS] OBJECT [COMMAND]

	COMMAND := ACTION SUBSYSTEM [ARGUMENT...]

This syntax follows other modern command line tools, such as iproute2, NetworkManager, nftables, systemd, ...

control computer

	backendctl cc[ID] status ntp
	backendctl cc[ID] { run | status | stop } schedule


bdc

	backendctl bdc[ID] get { lock | frequency }

	backendctl bdc[ID] set band { 4to8 | 5to9 }

	#-- output
	backendctl bdc[ID] list outputs
	backendctl bdc[ID] list output {outputID}
	backendctl bdc[ID] get output {outputID} attenuator
	backendctl bdc[ID] set output {outputID} attenuator {level}


r2dbe

	backendctl r2dbe[ID] { setup | status }

	#-- calibration
	backendctl r2dbe[ID] start calibration adc-core
	backendctl r2dbe[ID] start calibration 2bit-thres


recorder

	backendctl recorder[ID] status ntp

	#-- cdplane
	backendctl recorder[ID] { start | restart | status | help } cdplane

	#-- input
	backendctl recorder[ID] list inputs
	backendctl recorder[ID] add input {inputSpec...}
	backendctl recorder[ID] { list | delete } input {inputID}

	#-- output
	backendctl recorder[ID] list disks
	backendctl recorder[ID] list disk {diskID}

	backendctl recorder[ID] list modules
	backendctl recorder[ID] { list | init } module {moduleID}

	backendctl recorder[ID] list groups
	backendctl recorder[ID] add group { moduleID ... }
	backendctl recorder[ID] { list | open | close | mount | unmount } group {groupID}

	#-- packet
	backendctl recorder[ID] check packet { routing | timestamps | header | two-bit-statistics }

	#-- scan
	backendctl recorder[ID] list scans
	backendctl recorder[ID] { list | status | fillfraction } scan {ScanID}
	backendctl recorder[ID] add scan {scanSpec...}

	#-- schedule
	backendctl recorder[ID] start schedule {vexfile}
	backendctl recorder[ID] stop schedule
