#!/usr/bin/python
"""
This python module helps representing a usage doc as a tree of
command-argument nodes against which usage lines can be matched and
interpreted.
"""
from collections import namedtuple
from contextlib import suppress
import itertools
import pprint
import sys

# Maximum number of args that a command-argument group can have.
# Example: command group "init" in the following input has two arguments
# "backendctl mark6 ID modules init SLOT MSN"
# Another example: command group "test" in the following has five arguments
# "backendctl schedule test test.vex DLEAY NSCAN SCANLEN GAPLEN
N_ARGS_MAX = 5

# debugging output
DEBUG = 0

# name for nodes that are to be dropped from the tree
PLACEHOLDERNODE_NAME = '.'

# Match class holds a sequence of command groups, the arguments found
# for each group, and the error for the match.  Multiple matches may be
# found as the input is parsed against the tree.
Match = namedtuple('Match', 'cmds args err')
Match.__doc__ = """
Represent a command line match with a doc tree.
   1. sequence of node fullnames
   2. sequence of matched arguments for each node
   3. error"""

class DocparseException(Exception): pass
class MissingArgument(DocparseException): pass
class UnmatchedArgument(DocparseException): pass
class MissingCommand(DocparseException): pass
class UnmatchedCommand(DocparseException): pass
class TooManyKeywords(DocparseException): pass
class IncompatibleValues(DocparseException): pass

class Node(tuple):
    """Class for representing command-argument nodes in a usage tree.

Nodes have one parent and zero or more children.  New nodes are
added as children of an existing node with their given name.  A
sibling may already exists with that same name.  In that case the
sibling must already be a leaf node if the new node is supposed to
become one, and vice versa, or an error is raised.  This catches
unsupported use cases.  Placeholder nodes, named PLACEHOLDERNODE_NAME,
must be used for providing a description to non-leaf nodes.
"""
    def __new__(cls, name, args, desc='', parent=None, children={}):
        assert isinstance(name, str)    #-- immutable command
        assert isinstance(args, tuple)  #-- immutable arguments
        assert isinstance(desc, str)    #-- immutable description
        assert isinstance(parent, (Node, type(None)))
        assert isinstance(children, dict)
        return super().__new__(cls, [name, tuple(args), str(desc), parent, dict(children)])

    #-- getters
    name     = property(lambda self: self[0])
    args     = property(lambda self: self[1])
    desc     = property(lambda self: self[2])
    parent   = property(lambda self: self[3])
    children = property(lambda self: self[4])

    #-- setter
    def add_child(self, name, args, desc, lastdecendant):
        assert isinstance(name, str)
        assert isinstance(args, tuple)
        assert isinstance(lastdecendant, bool)  #-- support commands with different numbers of arguments

        #-- verify persistent (non-)leaf node
        fullname = name + '%d'%len(args)
        #if (me := self.children.get(fullname, None)) != None:
        me = self.children.get(fullname, None)
        if me != None:
            #-- arguments can only be distinguised by position
            #-- complain if different names are used at the same positions
            if not args == me.args:
                raise DocparseException('indistinguishable argument lists', name, me.args, args)
            #-- assert consistent branch end
            if lastdecendant == bool(me.children):
                raise DocparseException('last decendant may not have children', name)
        #-- kill placeholder sibling now that a real sibling gets added
        self.children.pop(PLACEHOLDERNODE_NAME+'0', None)
        return self.children.setdefault(fullname, Node(name, args, desc, self))

    #-- linearize decendant tree
    def descend(self):
        def traverse(node, acc, master):
            acc = [*acc, node.name, *node.args]
            if not node.children:
                master.append(acc)
                return master
            for c in node.children.values():
                traverse(c, acc, master)
            return master
        return traverse(self, [], [])

    #-- path back to root
    def ancestors(self):
        #-- back up in tree
        anc = []
        node = self
        while node.parent != None:
            node = node.parent
            anc.extend(node.args)
            anc.append(node.name)
        return anc[::-1]

    #-- from root to current node and branch out over all decendants
    def usage(self):
        linear = self.descend()
        anc = self.ancestors()
        return [' '.join(anc + l) for l in linear]


class Tree(Node):
    """Class for generating a Node tree from a usage doc and matching input lines against it.

A Node-tree is generated from a doc that specifies all possible usage
lines.  Each line contains a sequence of command-arguments sets followed
by a comment after a '#' symbol.  Commands are lower cased, arguments
are upper cased.  In the doc, placeholder nodes with the name
PLACEHOLDERNODE_NAME can be used for providing a description to non-leaf
nodes.
"""
    @classmethod
    def from_doc(obj, doc, rootname='tool'):
        #-- each list element is a doc line
        assert isinstance(doc, list)
        root = obj(rootname, tuple())
        for line in doc:
            #-- empty line
            sublines = line.split('#')
            words = sublines[0].strip().split()
            if len(words) == 0:
                continue

            desc = ''
            with suppress(IndexError): desc = sublines[1].strip()

            child = root
            cmd = None
            args = []
            for w in words:
                #-- Argument
                if w.isupper():
                    #-- new arg
                    args.append(w)
                #-- Command
                else:
                    if cmd is not None:
                        child = child.add_child(cmd, tuple(args), desc, lastdecendant=False)
                        args = []
                    cmd = w
            child.add_child(cmd, tuple(args), desc, lastdecendant=True)

        #-- complain about leftover placeholder nodes
        for path in root.descend():
            if path[-1][0] == PLACEHOLDERNODE_NAME: raise Exception('Placeholder found')
        return root

    def testsuite(self, selectnode_defs, leafnode_defs, constants):
        '''Traverse the tree and execute each node with dummy arguments'''
        #-- dummy values that may satisfy each of the verification functions
        DUMMYVALUES = ['0', '0'*8]  #-- [(isint, isfloat, is_attn, isalnum), is_msn]
        def traverse(node, acc, args):
            acc = [*acc, '%s%d'%(node.name, len(node.args))]
            path = '_'.join(acc[1:])
            #-- non-select node: execute
            if len(node.args) == 0:
                execute(node, acc, [*args, []])
                return
            #-- select-node: generate arguments using list functions
            flat = [e for sl in args for e in sl]
            arg = list(selectnode_defs[path].list(*flat))

            #-- at each arg posittion there are n possible arguments
            #-- create big list of possible combinations
            combinations = []
            #-- try different types of values that may satisfy the constraint function
            for i,a in enumerate(arg):
                if not callable(a): continue
                for v in DUMMYVALUES:
                    if a(v): break  #-- constraint satisfied
                else: raise Exception('Not satisfied constraint={:} path={:} argumentnr={:}'.format(a, path, node.args[i]))
                arg[i] = [v]
            #-- execute using all different combinations of arguments
            for option in itertools.product(*arg):
                #-- expand each value n times
                nn = selectnode_defs[path].count(*flat, *option)
                option = [','.join([o]*nn[i]) if nn[i] is not None else o for i,o in enumerate(option)]
                execute(node, acc, [*args, option])

        def execute(node, acc, args):
            #-- non-leaf node: delegate execution
            if node.children:
                for c in node.children.values():
                    traverse(c, acc, args)
                return

            #-- leaf-node: execute
            m = Match(acc[1:], args[1:], None)
            arg = self.__make_args(m)
            arg.update(constants)
            path = '_'.join(acc[1:])
            #print(path, args, m)
            try:
                arg = leafnode_defs[path].prepare(arg)
            except DocparseException as e:
                print(path, e)
                return

            #-- translate comma-separated values to space-separated lists
            arg = {k: ' '.join(v.split(',')) for k,v in arg.items()}

            #-- execute command
            try: cmd = leafnode_defs[path].exec.format(**arg)
            except Exception as e: print(path, e)
            else: print(path, cmd)
            return
        return traverse(self, [], [])

    def verify_defs(self, selectnode_defs, leafnode_defs):
        def traverse(node, acc):
            acc = [*acc, '%s%d'%(node.name, len(node.args))]
            path = '_'.join(acc[1:])
            #-- select-node
            if len(node.args) > 0:
                if not path in selectnode_defs:
                    print('WARNING: missing select-node definition', path)
            #-- leaf-node
            if not node.children:
                if not path in leafnode_defs:
                    print('WARNING: missing leaf-node definition', path)
                return
            for c in node.children.values():
                traverse(c, acc)
        return traverse(self, [])

    #-- match input command against tree
    #-- return command ID and dict of matched arguments
    def __walk_for_matches(self, cmd, selectnode_defs, leafnode_defs):
        assert isinstance(cmd, list)
        assert isinstance(selectnode_defs, dict)
        def verify_arguments(options, args):
            for i in range(min(len(options), len(args))):
                opts, arg = options[i], args[i]
                #-- unpack comma-separated lists
                for a in arg.split(','):
                    if isinstance(opts, list):
                        if not a in opts:
                            return UnmatchedArgument(a), i
                    else:
                        #-- evaluate validation function on argument
                        if not opts(a):
                            return UnmatchedArgument(a), i
            return None, 0
        def consume(node, tail, head=[], args=[], matches=[]):
            #-- tail exhausted
            if not tail:
                #-- last decendant?
                if node.children:
                    matches.append(Match(head, args, MissingCommand()))
                    return matches

                if DEBUG: print('match complete:', head)

                #-- catch failure compiling target exec keywords
                #-- This implies that provided values are not acceptable for the matched target
                cmd = '_'.join(head)
                func = leafnode_defs[cmd].prepare
                arg = self.__make_args(Match(head, args, None))
                try:
                    arg = func(arg)
                #-- TODO: only catch intended exceptions
                except DocparseException as e:
                    matches.append(Match(head, args, IncompatibleValues(e)))
                    return matches

                matches.append(Match(head, args, None))
                return matches

            name = tail.pop(0)
            #if DEBUG: print(head, name, tail)

            #-- try consuming n args for this node
            result = None
            for i in range(N_ARGS_MAX+1):
                k = "%s%d" % (name, i)
                try:
                    n = node.children[k]
                except KeyError as e:
                    if DEBUG: print(repr(e))
                    continue

                path = '_'.join([*head, k])
                #-- verify arguments
                if i > 0:
                    flat = [e for sl in args for e in sl]
                    options = selectnode_defs[path].list(*flat)
                    err, j = verify_arguments(options, tail[:i])
                    if err is not None:
                        matches.append(Match([*head, k], [*args, tail[:j]], err))
                        result = matches[-1]    # signal at least one successful match
                        continue


                if DEBUG: print("head:{:}  args:{:}  node:'{:}'  tail:{:}".format(head, args, k, tail))
                if i > len(tail):
                    #matches.append(Match([*head, k], [*args, {n.args[i]: a for i,a in enumerate(tail)}], MissingArgument(n.args[i-1])))
                    a = n.args[len(tail)]
                    matches.append(Match([*head, k], [*args, tail], MissingArgument(a)))
                    result = matches[-1]    # signal at least one successful match
                    continue

                #result = consume(n, tail[i:], [*head, k], [*args, {n.args[i]: a for i,a in enumerate(tail[:i])}], matches)
                result = consume(n, tail[i:], [*head, k], [*args, tail[:i]], matches)

            #-- dead end, save
            if result is None:
                #-- last decendant?
                if node.children:
                    matches.append(Match(head, args, UnmatchedCommand(name)))
                else:
                    matches.append(Match(head, args, TooManyKeywords(name)))
            return matches
        return consume(self, cmd.copy())

    def __make_args(self, m):
        n = self
        arg = {}
        for i,c in enumerate(m.cmds):
            n = n.children[c]
            arg.update(dict(zip(n.args, m.args[i])))
        return arg

    def __interpret_match_error(self, m, selectnode_defs):
        assert isinstance(m, Match)
        assert isinstance(selectnode_defs, dict)
        def make_highlight(w1):
            doc = [repr(m.err)]
            w0 = ' '.join(template)
            doc.append('# {} {}'.format(w0, w1))
            doc.append('  {} {}'.format(' '*len(w0), '^'*len(w1)))
            return doc

        n = self
        rootname = self.name
        cmd = [rootname]
        template = [rootname]
        for i,c in enumerate(m.cmds):
            n = n.children[c]
            cmd.append(n.name)
            cmd.extend(n.args)
            template.append(n.name)
            template.extend(m.args[i])

        usage, sugg = [], []

        #-- insert errors only once
        #errhash = '%s_%d_%s'%(n.name, len(m.args[-1]), m.err.args[0])

        #backendctl mark6 ID modules init SLOT MSN
        #backendctl mark6 recorder1 modules init 1 msn test
        #                                              ^^^^
        if isinstance(m.err, TooManyKeywords):
            usage = [' '.join(cmd)]
            sugg = make_highlight(m.err.args[0])


        #backendctl mark6 recorder1 modules init 1
        #                                          ^^^
        elif isinstance(m.err, MissingArgument):
            usage = [' '.join(cmd)]
            missing_args = n.args[len(m.args[-1]):]
            sugg = make_highlight(missing_args[0])
            sugg[-2] += ' ' + ' '.join(missing_args[1:])

            flat = [e for sl in m.args for e in sl]
            options = selectnode_defs['_'.join(m.cmds)].list(*flat)
            #a = m.err.args[0]
            #i = n.args.find(a)
            for i,a in enumerate(n.args):
                o = options[i]
                sugg.append('Options for {:}:'.format(a))
                if isinstance(o, list):
                    sugg.extend(['\t%s'%e for e in o])
                else:
                    sugg.append('\t%s'%o.__doc__)


        #backendctl mark6 recorder1 modules
        #                                   ^^^
        # Usage
        elif isinstance(m.err, MissingCommand):
            usage = n.usage()
            sugg = make_highlight('   ')


        #backendctl mark6 recorder1 modules test
        #                                   ^^^^
        # Usage
        elif isinstance(m.err, UnmatchedCommand):
            usage = n.usage()
            sugg = make_highlight(m.err.args[0])


        #backendctl mark6 ID modules init SLOT MSN
        #                                 ^^^^
        elif isinstance(m.err, UnmatchedArgument):
            usage = [' '.join(cmd)]
            sugg = make_highlight(m.err.args[0])

            flat = [e for sl in m.args for e in sl]
            options = selectnode_defs['_'.join(m.cmds)].list(*flat)
            #a = m.err.args[0]
            #i = n.args.find(a)
            for i,a in enumerate(n.args):
                o = options[i]
                sugg.append('Options for {:}:'.format(a))
                if isinstance(o, list):
                    sugg.extend(['\t%s'%e for e in o])
                else:
                    sugg.append('\t%s'%o.__doc__)

        elif isinstance(m.err, IncompatibleValues):
            usage = n.usage()
            sugg = [repr(m.err)]

        #-- err
        else:
            raise DocparseException('Match error not implemented {!r}', m.err)

        return usage, sugg

    def resolve(self, cmd, selectnode_defs, leafnode_defs, constants):
        assert isinstance(selectnode_defs, dict)
        assert isinstance(leafnode_defs, dict)
        assert isinstance(constants, dict)
        '''Match input command line against tree'''
        #-- possible resolutions are returned as Match objects
        matches = self.__walk_for_matches(cmd, selectnode_defs, leafnode_defs)
        if DEBUG: pprint.pprint(matches, sort_dicts=False)

        #-- select successful matches
        #-- collect usage and suggestion info for printing in case no singular match
        resolutions, usage, sugg = [], [], []
        for m in matches:
            if m.err is not None:
                use, sug = self.__interpret_match_error(m, selectnode_defs)
                usage.extend(use)
                sugg.append(sug)
                continue

            #-- apply target requirements
            resolutions.append(m)

        for i,s in enumerate(sugg):
            msg = 'Suggestion %d:'%(i+1) if len(sugg) > 1 else 'Suggestion:'
            s.insert(0, '='*len(msg))
            s.insert(0, msg)

        #-- continue with successful matches
        #-- too many resolutions
        if len(resolutions) > 1:
            return 'ERROR: Multiple interpretations of this command possible:\n' + pprint.pformat(resolutions, sort_dicts=False), ''

        #-- not enough resolutions
        elif len(resolutions) < 1:
            msg = ['Usage:'] + usage
            #print('\nNote:', 'Comma-separated list PARAMETERS are supported.', sep='\n')
            for i,s in enumerate(sugg):
                msg.append('')
                msg.extend(s)
            return '\n'.join(msg), ''

        #-- one resolution, GO!
        else:
            m = resolutions[0]
            path = '_'.join(m.cmds)

            #-- collect output arguments as key:value pairs
            #-- input arguments
            arg = self.__make_args(m)
            arg.update(constants)
            #-- calculate output arguments using custom function defined for this target
            arg = leafnode_defs[path].prepare(arg)

            #-- translate comma-separated values to space-separated lists
            arg = {k: ' '.join(v.split(',')) for k,v in arg.items()}

            #-- execute command
            cmd = leafnode_defs[path].exec.format(**arg)
            return None, cmd
